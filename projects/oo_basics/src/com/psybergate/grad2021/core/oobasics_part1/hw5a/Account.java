package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public abstract class   Account {

  public abstract int getBalance();

  public abstract int getAccountNum();

}
