package com.psybergate.grad2021.core.oobasics_part1.ce3b;

public class Car extends Vehicle {

  private final String carBrand;

  private final String carModel;

  private int distanceTravelled;

  public Car(String colour, String carBrand, String carModel, int distanceTravelled) {
    super(colour);
    this.carBrand = carBrand;
    this.carModel = carModel;
    this.distanceTravelled = distanceTravelled;
  }

  public Car(String colour, String carBrand, String carModel) {
    this(colour, carBrand, carModel, 0);
  }

  @Override
  public void doSomething(int number) {
    super.doSomething(number);
  }

  @Override
  public void increaseDistanceTravelled(int distanceTravelled) {
    this.distanceTravelled += distanceTravelled;
  }

  public void callObjectToString() {
    //In Java, it is only possible to go one level up:
    super.toString();

    //Therefore, we cannot call the Object's toString() method:
//    super.super.toString();
  }
}
