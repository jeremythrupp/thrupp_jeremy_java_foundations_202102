package com.psybergate.grad2021.core.oobasics_part1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class CustomerUtils {

  public static void main(String[] args) {
    Customer customer =
        new Customer("a0001", "John Doe", 35, "14 Short Street, Honeydew, Johannesburg. 2180.", "Person");

    addAccounts(customer);

    printBalances(customer);
  }

  private static void addAccounts(Customer customer) {
    Account account1 = new CurrentAccount(1, 13000, 2000);
    Account account2 = new CurrentAccount(2, 9000, 1000);
    Account account3 = new SavingsAccount(1, 20000, 15000, 5000);
    Account account4 = new SavingsAccount(2, 14000, 8000, 3000);

    customer.addAccount(account1);
    customer.addAccount(account2);
    customer.addAccount(account3);
    customer.addAccount(account4);
  }

  private static void printBalances(Customer customer) {
    List<Account> accountList = new ArrayList();

    accountList = customer.getAccountList();

    for (Account account : accountList) {
      String output = "CustomerNum: " + customer.getCustomerNumber() + ", Account Num: " + account
          .getAccountNum() + ", Balance: " + account.getBalance();
      System.out.println(output);
    }
  }
}
