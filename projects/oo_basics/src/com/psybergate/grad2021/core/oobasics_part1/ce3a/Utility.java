package com.psybergate.grad2021.core.oobasics_part1.ce3a;

import java.util.ArrayList;
import java.util.List;

public class Utility {
  public static void main(String[] args) {

    List l1 = new ArrayList();

    Account account1 = new CurrentAccount(1, "John", "Smit", 2000, 1.4, 4000);

    //The below code displays that the compiler works with the Object Reference and not the Object
    //System.out.println(account1.isOverDrawn());

    System.out.println(account1.needsToBeReviewed());

    CurrentAccount currentAccount1 = new CurrentAccount(2, "Jane", "Doe", 5000, 0.3, 2000);

    List<CurrentAccount> currentAccountList = new ArrayList();
    currentAccountList.add(currentAccount1);

    System.out.println(currentAccountList.get(0).toString());

    CurrentAccount currentAccount2 = currentAccountList.get(0);
    currentAccountList.remove(0);
    currentAccount2.setInterestRate(1.5);
    currentAccountList.add(currentAccount2);

    System.out.println(currentAccountList.get(0).toString());

  }

}
