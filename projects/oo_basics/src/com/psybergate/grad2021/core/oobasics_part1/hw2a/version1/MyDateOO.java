package com.psybergate.grad2021.core.oobasics_part1.hw2a.version1;

import java.sql.Date;

public class MyDateOO {
  public static void main(String[] args) {

    Date date1 = new Date(0);
    Date date2 = new Date(7);

    System.out.println("isValidDate(date) = " + isValidDate(date1));
    System.out.print("\n");
    System.out.println("addDays(date, 150) = " + addDays(date1, 150));
    System.out.print("\n");
    //The method setGate() is deprecated and is used only for demonstration purposes.
    System.out.println("dateComparison() = " + dateComparison(date1.getDate(), date2.getDate()));

  }

  private static String addDays(Date date, int numberOfDays) {
    Date newDate = date;

    newDate.setDate(numberOfDays);

    return "The new date is: " + newDate;
  }

  private static boolean isValidDate(Date date) {

    return date instanceof Date;
  }

  public static int dateComparison(int date1, int date2) {

    if (date1 < date2) {
      return 0;
    } else if (date1 > date2) {
      return 1;
    } else {
      return -1;
    }

  }

}
