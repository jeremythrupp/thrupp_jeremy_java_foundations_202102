package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public class CurrentAccount extends Account {

  private  int accountNum;

  private  int balance;

  private  double overDraft;

  public CurrentAccount(int accountNum, int balance, double overDraft) {
    this.accountNum = accountNum;
    this.balance = balance;
    this.overDraft = overDraft;
  }

  @Override
  public int getAccountNum() {
    return accountNum;
  }

  @Override
  public int getBalance() {
    return balance;
  }
}
