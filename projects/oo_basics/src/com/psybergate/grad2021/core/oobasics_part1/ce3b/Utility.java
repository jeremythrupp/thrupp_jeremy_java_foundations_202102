package com.psybergate.grad2021.core.oobasics_part1.ce3b;

public class Utility {
  public static void main(String[] args) {
    Vehicle car1 = new Car("Green", "Ford", "Fiesta", 2300);
    Vehicle car2 = new Car("Blue", "Toyota", "Corolla");

    car1.increaseDistanceTravelled(3000);

  }
}

