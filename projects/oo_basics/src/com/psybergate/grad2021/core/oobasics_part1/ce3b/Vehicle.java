package com.psybergate.grad2021.core.oobasics_part1.ce3b;

public abstract class Vehicle {

  private final String colour;

  public Vehicle(String colour) {
    this.colour = colour;
  }

  public void doSomething(int number) {
    System.out.println("We are in the Vehicle class.");
    System.out.println("You passes in the number: " + number + ".");
  }

  public String getColour() {
    return colour;
  }

  public abstract void increaseDistanceTravelled(int distanceTravelled);

  @Override
  public String toString() {
    return "Vehicle{" +
        "colour='" + colour + '\'' +
        '}';
  }
}

