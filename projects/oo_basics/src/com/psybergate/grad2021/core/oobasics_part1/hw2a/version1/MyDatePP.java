package com.psybergate.grad2021.core.oobasics_part1.hw2a.version1;

import java.sql.Date;

public class MyDatePP {

  private final int numberOfDaysToAdd = 9;

  private final Date date1 = new Date(0);

  private final Date date2 = new Date(7);

  public static void main(String[] args) {

    MyDatePP myDatePP = new MyDatePP();

    System.out.println("validDate(myDatePP) = " + validDate(myDatePP));
    System.out.println("\n");
    System.out.println("addDays(myDatePP) = " + addDays(myDatePP));
    System.out.println("\n");
    System.out.println("dateComparison(myDatePP) = " + dateComparison(myDatePP));
  }

  public static boolean validDate(MyDatePP myDatePP) {

    return myDatePP.getDate1() instanceof Date;

  }

  private static String addDays(MyDatePP myDatePP) {
    Date date = myDatePP.getDate1();
    date.setDate(myDatePP.numberOfDaysToAdd);

    return "The new date is: " + date;
  }

  public static int dateComparison(MyDatePP myDatePP) {

    int date1 = myDatePP.getDate1().getDate();
    int date2 = myDatePP.getDate2().getDate();

    if (date1 < date2) {
      return 0;
    } else if (date1 > date2) {
      return 1;
    } else {
      return -1;
    }

  }

  public Date getDate1() {
    return date1;
  }

  public Date getDate2() {
    return date2;
  }
}
