package com.psybergate.grad2021.core.oobasics_part1.ce1a;

public class CurrentAccount {
  private final int accountNum;

  private final int balance;

  public CurrentAccount(int accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  public int getAccountNum() {
    return accountNum;
  }

  public int getBalance() {
    return balance;
  }

}
