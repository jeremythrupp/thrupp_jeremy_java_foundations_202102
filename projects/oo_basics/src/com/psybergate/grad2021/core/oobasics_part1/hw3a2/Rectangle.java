package com.psybergate.grad2021.core.oobasics_part1.hw3a2;

public class Rectangle {
  public static final int MAXIMUM_LENGTH = 250;

  public static final int MAXIMUM_WIDTH = 150;

  public static final int MAXIMUM_AREA = 15000;

  private  int length;

  private  int width;


  public Rectangle(int length, int width) {
    if (!isValidRectangle(length, width)) {
      System.out.println("You have entered invalid dimensions.");
    }
    this.length = length;
    this.width = width;

  }

  public static int getMaximumLength() {
    return MAXIMUM_LENGTH;
  }

  public static int getMaximumWidth() {
    return MAXIMUM_WIDTH;
  }

  public static int getMaximumArea() {
    return MAXIMUM_AREA;
  }

  public String getDimensions() {
    if (isValidRectangle(length, width)) {
      return "Length: " + length + ", Width: " + width;
    } else {
      throw new RuntimeException("Invalid Rectangle");
    }
  }

  public int getArea() {
    if (isValidRectangle(length, width)) {
      return length * width;
    } else {
      throw new RuntimeException("I");
    }

  }

  public boolean isValidRectangle(int length, int width) {
    return length > width;
  }
}
