package com.psybergate.grad2021.core.oobasics_part1.hw4a1;

public class Customer {
  public static final int MINIMUM_AGE = 18;

  private String customerNum;

  private String customerName;

  private int customerAge;

  private String customerAddress;

  private String customerType;


  public Customer(String customerNum, String customerName, int customerAge, String customerAddress, String customerType) {
    this.customerNum = customerNum;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerAddress = customerAddress;
    this.customerType = customerType;
  }

  public void increaseCustomerAge(int yearsToIncreaseBy){
    customerAge += yearsToIncreaseBy;
  }

  public void decreaseCustomerAge(int yearsToDecreaseBy){
    customerAge -= yearsToDecreaseBy;
  }

  public static String getFullCustomerType(){
    return "Type: Customer";
  }



}
