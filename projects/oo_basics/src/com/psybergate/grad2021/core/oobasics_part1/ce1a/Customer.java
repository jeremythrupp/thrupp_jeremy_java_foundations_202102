package com.psybergate.grad2021.core.oobasics_part1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  private final int customerNum;

  private final String name;

  private final String address;

  private final List<CurrentAccount> currentAccountList = new ArrayList<CurrentAccount>();

  public Customer(int customerNum, String name, String address) {
    this.customerNum = customerNum;
    this.name = name;
    this.address = address;
  }

  public void addCurrentAccount(int accountNum, int balance) {
    CurrentAccount currentAccount = new CurrentAccount(accountNum, balance);
    currentAccountList.add(currentAccount);
  }

  public void printCurrentAccounts() {
    for (CurrentAccount currentAccount : currentAccountList
    ) {
      System.out
          .println("Account Number: " + currentAccount.getAccountNum() + " , Balance: " + currentAccount.getBalance());
    }

  }
}
