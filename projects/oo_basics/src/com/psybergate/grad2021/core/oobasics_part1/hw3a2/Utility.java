package com.psybergate.grad2021.core.oobasics_part1.hw3a2;

public class Utility {
  public static void main(String[] args) {
    Rectangle rectangle1 = new Rectangle(6, 4);
    Rectangle rectangle2 = new Rectangle(8, 5);
    Rectangle rectangle3 = new Rectangle(5, 2);

    //What is the key difference between each object?
    //The key difference between each object is the state of each object
    //This can be illustrated by calling the getDimensions() method on each Rectangle object

    System.out.println("rectangle1.getDimensions() = " + rectangle1.getDimensions());
    System.out.println("rectangle2.getDimensions() = " + rectangle2.getDimensions());
    System.out.println("rectangle3.getDimensions() = " + rectangle3.getDimensions());
    System.out.print("\n");


    //What is the same for each object?
    //Each Rectangle object has the same behaviour
    //Therefore, each Rectangle object also has the following methods available that you can call on
    //the rectangle1 reference.
    System.out.println("rectangle1.getDimensions() = " + rectangle1.getDimensions());
    System.out.println("rectangle1.getArea() = " + rectangle1.getArea());
    System.out.print("\n");


//    Make the following changes to the following constraints and note
//    your observations for each of the 3 Rectangles you created
//    previously:
//    Maximum length = 250
//    Maximum width = 150

//    As seen by the output below, these changes effect all 3 Rectangle objects.
//    This is because each rectangle reference is an instance of the Rectangle object which
//    contains the variable MAXIMUM_LENGTH and MAXIMUM_WIDTH.


    System.out.println("rectangle1.MAXIMUM_LENGTH = " + rectangle1.MAXIMUM_LENGTH);
    System.out.println("rectangle1.MAXIMUM_WIDTH = " + rectangle1.MAXIMUM_WIDTH);

    System.out.println("rectangle2.MAXIMUM_LENGTH = " + rectangle2.MAXIMUM_LENGTH);
    System.out.println("rectangle2.MAXIMUM_WIDTH = " + rectangle2.MAXIMUM_WIDTH);

    System.out.println("rectangle3.MAXIMUM_LENGTH = " + rectangle3.MAXIMUM_LENGTH);
    System.out.println("rectangle3.MAXIMUM_WIDTH = " + rectangle3.MAXIMUM_WIDTH);

  }
}
