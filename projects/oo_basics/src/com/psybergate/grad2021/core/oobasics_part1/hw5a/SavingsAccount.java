package com.psybergate.grad2021.core.oobasics_part1.hw5a;

public class SavingsAccount extends Account {

  private int accountNum;

  private int balance;

  private double overDraft;

  private int minBalance;

  public SavingsAccount(int accountNum, int balance, double overDraft, int minBalance) {
    this.accountNum = accountNum;
    this.balance = balance;
    this.overDraft = overDraft;
    this.minBalance = minBalance;
  }

  @Override
  public int getAccountNum() {
    return accountNum;
  }

  @Override
  public int getBalance() {
    return balance;
  }
}
