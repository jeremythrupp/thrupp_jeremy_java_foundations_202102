package com.psybergate.grad2021.core.oobasics_part1.ce4a2;

public class Utility {
  public static void main(String[] args) {

    createAccounts();

  }

  private static void createAccounts() {
    Account account1 = new CurrentAccount(1, "John", "Smit", -60000, 1.4, 4000);
    Account account2 = new CurrentAccount(2, "Fiona", "Jones", 3000, 0.2, 1000);

    Account account3 = new SavingsAccount(1, "Eric", "Donald", 1000);
    Account account4 = new SavingsAccount(2, "Jane", "Doe", 6000);

    printReviewStatus(account1, account2, account3, account4);
  }

  private static void printReviewStatus(Account account1, Account account2, Account account3, Account account4) {
    System.out.println("account1.needsToBeReviewed() = " + account1.needsToBeReviewed());
    System.out.println("account2.needsToBeReviewed() = " + account2.needsToBeReviewed());
    System.out.println("account3.needsToBeReviewed() = " + account3.needsToBeReviewed());
    System.out.println("account4.needsToBeReviewed() = " + account4.needsToBeReviewed());
  }

}
