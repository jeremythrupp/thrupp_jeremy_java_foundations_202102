package com.psybergate.grad2021.core.oobasics_part1.ce1a;

public class Utility {

  public static void main(String[] args) {
    Customer customer = new Customer(1, "John", "245 Pascal Avenue, 2194. Johannesburg");

    customer.addCurrentAccount(1, 3000);
    customer.addCurrentAccount(2, 5000);
    customer.addCurrentAccount(3, 6000);

    customer.printCurrentAccounts();

  }

}
