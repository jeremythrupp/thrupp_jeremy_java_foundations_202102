package com.psybergate.grad2021.core.oobasics_part1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  public static final int MINIMUM_AGE = 18;

  private  String customerNumber;

  private  String customerName;

  private  int customerAge;

  private  String customerAddress;

  private  String customerType;

  private final List<Account> accounts= new ArrayList();

  public Customer(String customerNumber, String customerName, int customerAge, String customerAddress, String customerType) {
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerAddress = customerAddress;
    this.customerType = customerType;
  }

  public void addAccount(Account account) {
    accounts.add(account);
  }

  public List<Account> getAccountList() {
    return accounts;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }
}
