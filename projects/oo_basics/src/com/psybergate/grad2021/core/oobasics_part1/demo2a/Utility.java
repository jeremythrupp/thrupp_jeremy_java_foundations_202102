package com.psybergate.grad2021.core.oobasics_part1.demo2a;

import java.util.ArrayList;
import java.util.List;

public class Utility {
  public static void main(String[] args) {

    List<Shape> shapeList = getShapes();

    printShapes(getShapes());
  }

  private static void printShapes(List<Shape> shapes) {
    for (Shape shape : shapes) {
      shape.print();
    }
  }

  private static List<Shape> getShapes() {
    List<Shape> shapeList = new ArrayList();
    shapeList.add(new Rectangle(1, 6));
    shapeList.add(new Rectangle(4, 3));
    shapeList.add(new Circle(3));
    shapeList.add(new Circle(7));
    shapeList.add(new Triangle(3, 5));
    shapeList.add(new Triangle(2, 6));
    return shapeList;
  }


//  private static void printShapes(List<Shape> shapes) {
//    for (Shape shape : shapes) {
//      //Please note: using 'instanceof' is a code smell
//      if (shape instanceof Rectangle) {
//        System.out.println("Rectangle");
//        System.out.println("Dimensions: Height = " + ((Rectangle) shape).getHeight() + ", Width =" + ((Rectangle) shape).getWidth());
//        System.out.println("Area: " + ((Rectangle) shape).getHeight() * ((Rectangle) shape).getWidth());
//        System.out.print("/n");
//      } else if (shape instanceof Circle) {
//        System.out.println("Circle");
//        System.out.println("Dimensions: Radius = " + ((Circle) shape).getRadius());
//        System.out.println("Area: " + (Math.PI * ((Circle) shape).getRadius() * ((Circle) shape).getRadius()));
//        System.out.println("\n");
//      }
//    }


}

