package com.psybergate.grad2021.core.oobasics_part1.demo1;

public abstract class Vehicle {
  public abstract void printMileage();
}
