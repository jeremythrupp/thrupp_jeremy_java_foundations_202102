package com.psybergate.grad2021.core.oobasics_part1.ce4a2;

public class SavingsAccount extends Account {

  public static final int MIN_BALANCE = 5000;


  public SavingsAccount(int accountNum, String name, String surname, int balance) {
    super(accountNum, name, surname, balance);
  }

  @Override
  public boolean needsToBeReviewed() {
    return getBalance() < 2000;
  }

  @Override
  public String getAccountType() {
    return "Savings Account";
  }

  public boolean isOverdrawn() {
    return getBalance() < MIN_BALANCE;
  }
}
