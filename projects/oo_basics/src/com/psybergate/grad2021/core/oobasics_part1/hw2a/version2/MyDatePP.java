package com.psybergate.grad2021.core.oobasics_part1.hw2a.version2;

//In version two, no code implementation is required and the java.sql.Date object is not included

public class MyDatePP {

  private final int date1 = 19950212;

  private final int date2 = 20131003;

  private final int numberOfDaysToAdd = 9;

  public static boolean validDate(MyDatePP myDatePP) {

    return true;

  }

  private static String addDays(MyDatePP myDatePP) {

    return "";
  }

  public static int dateComparison(MyDatePP myDatePP) {

    return 0;

  }

  public int getDate1() {
    return date1;
  }

  public int getDate2() {
    return date2;
  }
}
