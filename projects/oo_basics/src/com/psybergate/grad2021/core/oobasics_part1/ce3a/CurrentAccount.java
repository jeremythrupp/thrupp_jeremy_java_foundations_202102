package com.psybergate.grad2021.core.oobasics_part1.ce3a;

public class CurrentAccount extends Account {

  public static final int MAX_OVERDRAFT = 100000;

  private double interestRate;

  private final double overdraft;

  public CurrentAccount(int accountNum, String name, String surname, int balance, double interestRate, double overdraft) {
    super(accountNum, name, surname, balance);
    this.interestRate = interestRate;
    this.overdraft = overdraft;
  }

  @Override
  public boolean needsToBeReviewed() {
    return isOverdrawn() && ((getBalance() > ((0.2) * overdraft)) || getBalance() <= -50000);
  }

  @Override
  public String getAccountType() {
    return "Current Account";
  }

  public boolean isOverdrawn() {
    return getBalance() < 0;
  }

  public void setInterestRate(double interestRate) {
    this.interestRate = interestRate;
  }
//
//  @Override
//  public String toString() {
//    return "CurrentAccount{" +
//        "interestRate=" + interestRate +
//        ", overdraft=" + overdraft +
//        '}';
//  }
}
