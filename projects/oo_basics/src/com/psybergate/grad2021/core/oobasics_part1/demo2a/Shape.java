package com.psybergate.grad2021.core.oobasics_part1.demo2a;

public abstract class Shape {

  private int height;

  private int base;

  public void print() {
    System.out.println("Dimensions: Height = " + height + ", Base = " + base);
    System.out.println("Area: " + (height * base) / 2);
  }


}
