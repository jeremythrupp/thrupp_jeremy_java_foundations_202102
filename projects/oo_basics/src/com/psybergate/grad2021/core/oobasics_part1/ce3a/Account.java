package com.psybergate.grad2021.core.oobasics_part1.ce3a;

public abstract class Account {

  private final int accountNum;

  private final String name;

  private final String surname;

  private final int balance;

  public abstract boolean needsToBeReviewed();

  public abstract String getAccountType();

  public Account(int accountNum, String name, String surname, int balance) {
    this.accountNum = accountNum;
    this.name = name;
    this.surname = surname;
    this.balance = balance;
  }

  public int getBalance() {
    return balance;
  }
}
