package com.psybergate.grad2021.core.oobasics_part1.hw4a2;

import java.util.ArrayList;
import java.util.List;

public class Utility {
  public static void main(String[] args) {

    List<Customer> customerList = new ArrayList();
    customerList = getCustomerList();
    printCustomers(customerList);

  }

  private static List<Customer> getCustomerList() {
    List<Customer> customerList = new ArrayList();
    customerList.add(new Customer("a0001", "Outsurance", 13, "213 Main Avenue, Bryanston, Johannesburg. 2190.", "Company"));
    customerList.add(new Customer("a0002", "Momentum", 5, "12 Long Street, Randburg, Johannesburg. 2190.", "Company"));
    customerList.add(new Customer("a0003", "Discovery", 22, "14 Grosvenor Street, Bryanston, Johannebsurg. 2190.", "Company"));

    return customerList;
  }

  private static void printCustomers(List<Customer> customerList) {
    for (Customer customer : customerList) {
      System.out.println("customer.toString() = " + customer.toString());
    }

  }
}
