package com.psybergate.grad2021.core.oobasics_part1.hw3a1;

public class Utility {
  public static void main(String[] args) {
    Rectangle rectangle1 = new Rectangle(3,4);
    System.out.println("rectangle1.getArea() = " + rectangle1.getArea());
    System.out.println("rectangle1.getDimensions() = " + rectangle1.getDimensions());

  }
}
