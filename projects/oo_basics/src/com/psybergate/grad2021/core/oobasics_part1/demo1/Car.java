package com.psybergate.grad2021.core.oobasics_part1.demo1;

public class Car extends Vehicle {

  private String carBrand;

  private String carModel;

  private int mileage;

  public Car(String carBrand, String carModel, int mileage) {
    this.carBrand = carBrand;
    this.carModel = carModel;
    this.mileage = mileage;
  }

  @Override
  public void printMileage() {
    System.out.println("The mileage is: " + mileage);
  }

  public void increaseMileage(int distanceTravelled){
    mileage += distanceTravelled;
  }



}
