package com.psybergate.grad2021.core.oobasics_part1.ce4a2;

public abstract class Account {


  private  int accountNum;

  private  String name;

  private  String surname;

  private  int balance;

  public Account(int accountNum, String name, String surname, int balance) {
    this.accountNum = accountNum;
    this.name = name;
    this.surname = surname;
    this.balance = balance;
  }

  public abstract boolean needsToBeReviewed();

  public abstract String getAccountType();

  public int getBalance() {
    return balance;
  }
}
