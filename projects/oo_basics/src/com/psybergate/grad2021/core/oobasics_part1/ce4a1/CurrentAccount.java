package com.psybergate.grad2021.core.oobasics_part1.ce4a1;

public class CurrentAccount extends Account{
  public static final int MAX_OVERDRAFT = 100000;

  private double interestRate;

  private double overdraft;

  public CurrentAccount(int accountNum, String name, String surname, int balance, double interestRate, double overdraft) {
    super(accountNum, name, surname, balance);
    this.interestRate = interestRate;
    this.overdraft = overdraft;
  }

  @Override
  public boolean needsToBeReviewed() {
    if (isOverdrawn() && ((getBalance() > ((0.2) * overdraft)) || getBalance() <= -50000)) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public String getAccountType() {
    return "Current Account";
  }

  public boolean isOverdrawn() {
    if (getBalance() < 0) {
      return true;
    }
    return false;
  }

  public void setInterestRate(double interestRate) {
    this.interestRate = interestRate;
  }
}
