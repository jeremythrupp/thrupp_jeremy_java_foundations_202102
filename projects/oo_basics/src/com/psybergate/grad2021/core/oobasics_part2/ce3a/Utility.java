package com.psybergate.grad2021.core.oobasics_part2.ce3a;

public class Utility {
  public static void main(String[] args) {
    Money money1 = new Money(2);
    Money money2 = new Money(3.5);

    System.out.println(money1.addAmount(money2).getAmount());
  }

}
