package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a1;

public class OrderItem {

  /**
   * product is a single Product of which OrderItem consists of
   * */
  private Product product;

  /**
   * quantity is the amount of products ordered
   * */
  private int quantity;

  /**
   * productStatus is the status of whether a product is ready to be delivered
   * */
  private String processingStatus;


  public OrderItem(Product product, int quantity, String processingStatus) {
    this.product = product;
    this.quantity = quantity;
    this.processingStatus = processingStatus;
  }
  public double getOrderItemTotal() {
    return product.getProductPrice() * getQuantity();
  }

  public int getQuantity() {
    return this.quantity;
  }

  public String getProcessingStatus() {
    return this.processingStatus;
  }
}
