package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a3;

import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order {

  public final String ORDER_TYPE = "Local";

  private int orderNumber;

  private double discount;

  private String discountPolicyChoice;

  private List<OrderItem> orderItems = new ArrayList();

  private int customerDuration;

  private double totalOrderValue;

  private double localDiscountPolicy;

  private double internationalDiscountPolicy;

  public LocalOrder(int orderNumber, double discount, String discountPolicyChoice) {
    this.orderNumber = orderNumber;
    this.discount = discount;
    this.discountPolicyChoice = discountPolicyChoice;
  }

  public double calculateLocalDiscountPolicy(int customerDuration) {



    if ((customerDuration >= 0) && (customerDuration <= 2)) {
      return 0;
    } else if ((customerDuration > 2) && (customerDuration <= 5)) {
      return 0.075;
    } else if (customerDuration > 5) {
      return 0.125;
    }
    return 0;
  }

  public double calculateInternationalDiscountPolicy(double totalOrderValue) {

    if (this.totalOrderValue != 0) {
      totalOrderValue = this.totalOrderValue;
    }

    if (internationalDiscountPolicy != 0) {
      return internationalDiscountPolicy;
    }

    if ((totalOrderValue >= 0) && (totalOrderValue <= 500000)) {
      return 0;
    } else if ((totalOrderValue > 500000) && (totalOrderValue <= 1000000)) {
      return 0.05;
    } else if (totalOrderValue > 1000000) {
      return 0.10;
    }

    return 0;
  }


  public double getOrderPrice() {

    double orderPrice = 0;

    for (OrderItem orderItem : orderItems) {
      orderPrice += (orderItem.getPrice() - (orderItem.getPrice() * discount));
    }

    return orderPrice;
  }


  @Override
  public String getDiscountPolicyChoice() {
    return discountPolicyChoice;
  }

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}
