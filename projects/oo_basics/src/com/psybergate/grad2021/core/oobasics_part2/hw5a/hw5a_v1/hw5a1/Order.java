package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//don't put interface for domain objects
public abstract class Order {

  //NB!
  private int orderNumber;

  private LocalDate orderDate;

  private List<OrderItem> orderItems = new ArrayList();

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public double getOrderTotal() {
    double orderTotal = 0;

    for (OrderItem orderItem : orderItems) {
      orderTotal += (orderItem.getOrderItemTotal());
    }
    return orderTotal;
  }

  public boolean isInternational() {
    return false;
  }

  public boolean isLocal() {
    return false;
  }

  public abstract String getOrderType();

}
