package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {

  public static void main(String[] args) {
    List<Order> orders = generateLocalAndInternationalOrders(4);
    printOrders(orders);
  }

  private static void printOrders(List<Order> orders) {
    for (Order order : orders) {
      System.out.println("Order Number: " + order.getOrderNumber());
      System.out.println("Order Total: " + order.getOrderTotal());
      System.out.println("Discounted Order Total: " + getDiscountedValue(order));
      System.out.print("\n");
    }
  }

  private static double getDiscountedValue(Order order) {
    if (order.isLocal()) {
      Discount discount = new LocalDiscount(order.getOrderTotal(), order.getOrderDate());
      return discount.getDiscountedValue();
    }
    Discount discount = new InternationalDiscount(order.getOrderTotal());
    return discount.getDiscountedValue();
  }

  public static List<Order> generateLocalAndInternationalOrders(int amount) {
    List<Order> orders = new ArrayList();
    DataUtils dataUtils = new DataUtils();
    for (int i = 0; i <= amount; i++) {
      orders.add(dataUtils.generateLocalOrder());
      orders.add(dataUtils.generateInternationalOrder());
    }
    return orders;
  }

}
