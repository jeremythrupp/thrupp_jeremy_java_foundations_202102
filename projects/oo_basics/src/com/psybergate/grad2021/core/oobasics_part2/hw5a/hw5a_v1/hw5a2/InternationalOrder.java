package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class InternationalOrder extends Order {

  public static final String ORDER_TYPE = "International";

  private int orderNumber;

  private double importDuty;

  private List<OrderItem> orderItems = new ArrayList();

  public InternationalOrder(int orderNumber, double importDuty) {
    this.orderNumber = orderNumber;
    this.importDuty = importDuty;
  }

  public double getOrderPrice() {

    double orderPrice = 0;

    for (OrderItem orderItem : orderItems) {
      orderPrice += (orderItem.getPrice() + (orderItem.getPrice() * importDuty));
    }

    return orderPrice;
  }

  public static double calculateDiscountPolicy(double totalOrderValue) {

    if (totalOrderValue <= 500000) {
      return 0;
    } else if (totalOrderValue <= 1000000) {
      return 0.05;
    } else if (totalOrderValue > 1000000) {
      return 0.10;
    }

    return 0;
  }

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}
