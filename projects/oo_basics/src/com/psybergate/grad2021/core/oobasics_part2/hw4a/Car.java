package com.psybergate.grad2021.core.oobasics_part2.hw4a;

public class Car extends Vehicle {

  private final String carBrand;

  private final String carModel;

  private int distanceTravelled;

  public Car(String carBrand, String carModel, int distanceTravelled) {
    this.carBrand = carBrand;
    this.carModel = carModel;
    this.distanceTravelled = distanceTravelled;
  }

//  @Override
  public static void printClass(){
    System.out.println("We are in the Car class.");
  }

}
