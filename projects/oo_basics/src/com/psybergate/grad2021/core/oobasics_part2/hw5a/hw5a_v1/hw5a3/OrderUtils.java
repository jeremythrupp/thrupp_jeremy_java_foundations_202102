package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a3;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
  public static void main(String[] args) {
    List<Customer> customers = createCustomers();

    printCustomers(customers);
  }

  private static void printCustomers(List<Customer> customers) {
    for (Customer customer : customers) {
      customer.print();
    }

  }

  private static List<Customer> createCustomers() {
    List<Customer> customers = new ArrayList();

    Customer
        customer1 = new Customer(1, "John", 25, "Local", 1);
    Customer
        customer2 = new Customer(2, "Alex", 30, "Local", 3);

    Customer
        customer3 = new Customer(1, "Ben", 26, "International", 4);
    Customer customer4 = new Customer(2, "Quinton", 28, "International", 6);

    Order order1 = new LocalOrder(1, 0.17, "International");
    Order order2 = new LocalOrder(2, 0.13, "Local");
    Order order3 = new InternationalOrder(1, 0.23, "Local");
    Order order4 = new InternationalOrder(2, 0.16, "International");

    Product product1 = new Product(1, "Nike Shoes", 499);
    Product product2 = new Product(2, "New Balance Hoodie", 599);
    Product product3 = new Product(3, "Soviet Jeans", 399);
    Product product4 = new Product(4, "HP Probook 234Y", 25999);

    OrderItem orderItem1 = new OrderItem(product1, 3, "Processing");
    OrderItem orderItem2 = new OrderItem(product2, 7, "Completed");
    OrderItem orderItem3 = new OrderItem(product3, 5, "Completed");
    OrderItem orderItem4 = new OrderItem(product4, 4, "Processing");

    order1.addOrderItem(orderItem1);
    order1.addOrderItem(orderItem2);

    order2.addOrderItem(orderItem4);

    order3.addOrderItem(orderItem1);
    order3.addOrderItem(orderItem2);
    order3.addOrderItem(orderItem3);
    order3.addOrderItem(orderItem4);

    order4.addOrderItem(orderItem2);
    order4.addOrderItem(orderItem3);
//
//    order1.setTotalOrderValue(9900000);
//    order2.setCustomerDuration(7);
//    order3.setLocalDiscountPolicy(0.15);
//    order4.setInternationalDiscountPolicy(0.23);

    customer1.addOrder(order1);
    customer1.addOrder(order2);
    customer1.addOrder(order4);

    customer2.addOrder(order1);
    customer2.addOrder(order2);
    customer2.addOrder(order3);
    customer2.addOrder(order4);

    customer3.addOrder(order1);
    customer3.addOrder(order2);
    customer3.addOrder(order3);

    customer4.addOrder(order1);
    customer4.addOrder(order2);
    customer4.addOrder(order3);
    customer4.addOrder(order4);

    customers.add(customer1);
    customers.add(customer2);
    customers.add(customer3);
    customers.add(customer4);

    return customers;
  }
}
