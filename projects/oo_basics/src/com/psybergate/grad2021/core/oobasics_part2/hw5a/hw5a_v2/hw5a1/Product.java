package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a1;

public class Product {

  /**
   * productNumber is the unique identifier for a Product
   */
  private int productNumber;

  /**
   * productName is the name a Product
   */
  private String productName;

  /**
   * price is the price per individual Product
   */
  private double price;

  public Product(int productNumber, String productName, double price) {
    this.productNumber = productNumber;
    this.productName = productName;
    this.price = price;
  }

  public int getProductNumber() {
    return productNumber;
  }

  public String getProductName() {
    return productName;
  }

  public double getProductPrice() {
    return price;
  }

}


