package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class Order {

  /**
   * orderNumber is the unique identifier for an Order
   */
  private int orderNumber;

  /**
   * orderDate is the date on which the order was created
   */
  private LocalDate orderDate;

  //TODO 1-03-2021 : The customer should be second (after orderNumber) --> press ctrl+f6 on constructor to change the order

  /**
   * customer is the customer who placed the order
   */
  private Customer customer;

  /**
   * orderItems is a collection of Orders placed by the customer
   */
  private List<OrderItem> orderItems = new ArrayList();

  /**
   * discountPolicyChoice is the customers choice of the discount policy to be applied
   * to the order
   */
  private String discountPolicyChoice;

  //TODO 1-03-2021 : The orderDate should be stored here --> this is the initial date the customer placed the order
  public Order(int orderNumber, String discountPolicyChoice) {
    this.orderNumber = orderNumber;
    this.discountPolicyChoice = discountPolicyChoice;
    this.orderDate = LocalDate.now();
  }

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public double getOrderTotal() {
    double orderTotal = 0;

    for (OrderItem orderItem : orderItems) {
      orderTotal += (orderItem.getOrderItemTotal());
    }
    return orderTotal;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    if (isValidCustomer(customer)) {
      this.customer = customer;
    } else {
      throw new RuntimeException("The Customer Type does not match the Order Type.");
    }
  }

  public String getDiscountPolicyChoice() {
    return discountPolicyChoice;
  }

  public abstract boolean isValidCustomer(Customer customer);

  public abstract String getOrderType();

  public abstract boolean isLocal();

  public LocalDate getOrderDate() {
    return orderDate;
  }

}
