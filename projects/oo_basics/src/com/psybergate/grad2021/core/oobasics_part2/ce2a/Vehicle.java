package com.psybergate.grad2021.core.oobasics_part2.ce2a;

public class Vehicle {
  private int vehicleIdentityNumber;

  private String vehicleName;

  public Vehicle(int vehicleIdentityNumber, String vehicleName) {
    this.vehicleIdentityNumber = vehicleIdentityNumber;
    this.vehicleName = vehicleName;
  }

 public void print(){
   System.out.println("Vehicle ID Number: " + vehicleIdentityNumber + ", Vehicle Name: " + vehicleName);
 }
}
