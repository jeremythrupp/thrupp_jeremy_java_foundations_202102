package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private int customerNumber;

  private String customerName;

  private int customerAge;

  private String customerType;

  public Customer(int customerNumber, String customerName, int customerAge, String customerType, int customerDuration) {
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerType = customerType;
    this.customerDuration = customerDuration;
  }

  private int customerDuration;

  private List<Order> orders = new ArrayList();



  public void addOrder(Order order) {

    if (order.getOrderType().equals("Local")) {
      if (customerType.equals("Local")) {
        orders.add(order);
      } else {
        System.out.println("Invalid order type. \n");
      }
    }

    if (order.getOrderType().equals("International")) {
      if (customerType.equals("International")) {
        orders.add(order);
      } else {
        System.out.println("Invalid order type. \n");
      }
    }

  }

  public void print() {
    System.out
        .println("Customer Number: " + customerNumber + ", Name: " + customerName + ", Age: " + customerAge + ", Type: " + customerType);

    double totalPrice = 0;

    for (Order order : orders) {
      System.out.println("Order Number: " + order.getOrderNumber() + ", Order Price: " + order.getOrderPrice());
      totalPrice += order.getOrderPrice() ;
    }

    if (customerType.equals("Local")) {
      totalPrice  = totalPrice - (totalPrice * LocalOrder.calculateDiscountPolicy(customerDuration));
      System.out.println("LocalOrder.calculateDiscountPolicy(customerDuration) = " + LocalOrder.calculateDiscountPolicy(customerDuration));
    }else if(customerType.equals("International")){
      totalPrice  = totalPrice - (totalPrice * InternationalOrder.calculateDiscountPolicy(totalPrice));

      System.out.println("totalPrice = " + totalPrice);
      System.out.println("InternationalOrder.calculateDiscountPolicy(totalPrice) = " + InternationalOrder.calculateDiscountPolicy(totalPrice));
    }

    System.out.println("Total Order Price: " + totalPrice);
    System.out.print("\n");
  }
}
