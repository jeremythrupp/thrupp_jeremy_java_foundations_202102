package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class Order {


  /**
   * orderNumber is the unique identifier for an Order
   * */
  private int orderNumber;

  /**
   * orderDate is the date on which the order was created
   * */
  private LocalDate orderDate;

  /**
   * customer is the customer who placed the order
   * */
  private Customer customer;

  /**
   * orderItems is a collection of Orders placed by the customer
   * */
  private List<OrderItem> orderItems = new ArrayList();

  public Order(int orderNumber, LocalDate orderDate) {
    this.orderNumber = orderNumber;
    this.orderDate = orderDate;
  }

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  public double getOrderTotal() {
    double orderTotal = 0;

    for (OrderItem orderItem : orderItems) {
      orderTotal += (orderItem.getOrderItemTotal());
    }
    return orderTotal;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    if (isValidCustomer(customer)) {
      this.customer = customer;
    } else {
      throw new RuntimeException("The Customer Type does not match the Order Type.");
    }
  }

  public abstract boolean isValidCustomer(Customer customer);

  public abstract String getOrderType();

}
