package com.psybergate.grad2021.core.oobasics_part2.ce3a;

import java.math.BigDecimal;

public class Money {
  private BigDecimal amount;

  public Money(int amount) {
    this.amount = new BigDecimal(amount);
  }

  public Money(double amount) {
    this.amount = new BigDecimal(amount);
  }

  public Money(BigDecimal amount) {
    this.amount = amount;
  }

  public Money addAmount(Money money) {
    return new Money(amount.add(money.getAmount()));
  }

  public BigDecimal getAmount() {
    return amount;
  }
}
