package com.psybergate.grad2021.core.oobasics_part2.ce2a;

public class Utility {
  public static void main(String[] args) {
    Vehicle vehicle1 = new Vehicle(1, "Boeing, 787");
    Vehicle vehicle2 = new Vehicle(2, "Ferrari, Enzo");

    print(vehicle1, vehicle2);

    swap(vehicle1, vehicle2);

    print(vehicle1, vehicle2);
  }

  private static void swap(Vehicle vehicle1, Vehicle vehicle2) {
    Vehicle vehicle3 = vehicle1;
    vehicle1 = vehicle2;
    vehicle2 = vehicle3;
  }

  private static void print(Vehicle vehicle1, Vehicle vehicle2) {
    vehicle1.print();
    vehicle2.print();
  }
}
