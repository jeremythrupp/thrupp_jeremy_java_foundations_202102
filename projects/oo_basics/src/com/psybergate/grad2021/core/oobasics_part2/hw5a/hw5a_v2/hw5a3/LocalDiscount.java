package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

import java.time.LocalDate;

public class LocalDiscount extends Discount {

  private LocalDate orderDate;

  public LocalDiscount(double totalOrderValue, LocalDate orderDate) {
    super(totalOrderValue);
    this.orderDate = orderDate;
  }

  @Override
  public double getDiscountedValue() {

    double discount;
    if (orderDate.getYear() <= CUSTOMER_MINIMUM_YEARS) {
      discount = 0;
    } else if (orderDate.getYear() <= CUSTOMER_MAXIMUM_YEARS) {
      discount = 0.075;
    }
    discount = 0.125;

    return getTotalOrderValue() * (1 - discount);
  }

}
