package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class Customer {

  private int customerNumber;

  private String customerName;

  private int customerAge;

  private String customerType;

  private List<Order> orders = new ArrayList();

  public Customer(int customerNumber, String customerName, int customerAge, String customerType) {
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerType = customerType;
  }

  public void addOrder(Order order) {

    //seperate addOrder - this method is too bulky
    //--> isValidOrder()

    //using too many strings --> brittle code; doesn't read well

    if (order.isLocal()) {
      //do the same for customerType
      //make Customer the focus --> invert this
      if (customerType.equals("Local")) {
        orders.add(order);
      } else {
//        System.out.println("Invalid order type. \n");
        throw new RuntimeException("Local Customer can only have a Local Order.");
      }
    }

    if (order.isInternational()) {
      if (customerType.equals("International")) {
        orders.add(order);
      } else {
        System.out.println("Invalid order type. \n");
      }
    }

  }

  public void print() {
    System.out
        .println("Customer Number: " + customerNumber + ", Name: " + customerName + ", Age: " + customerAge + ", Type: " + customerType);

    double totalPrice = 0;

    for (Order order : orders) {
      System.out.println("Order Number: " + order.getOrderNumber() + ", Order Price: " + order.getOrderTotal());
      totalPrice += order.getOrderTotal();
    }

    System.out.println("Total Order Price: " + totalPrice);
    System.out.print("\n");
  }
}
