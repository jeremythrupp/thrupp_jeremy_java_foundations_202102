package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InternationalOrder extends Order {

  /**
   * ORDER_TYPE is the type of order.
   */
  public static final String ORDER_TYPE = "International";

  /**
   * importDuty is the importDuty for that InternationalOrder in decimal form - it is a percentage.
   */
  private double importDuty;


  public InternationalOrder(int orderNumber, LocalDate orderDate, double importDuty) {
    super(orderNumber, orderDate);
    this.importDuty = importDuty;
  }


  private double applyImportDuty(double orderTotal) {
    return orderTotal * (1 - importDuty);
  }

  @Override
  public double getOrderTotal() {
    return applyImportDuty(super.getOrderTotal());
  }

  @Override
  public boolean isValidCustomer(Customer customer) {
    if (customer.isInternational()) {
      return true;
    }
    return false;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}