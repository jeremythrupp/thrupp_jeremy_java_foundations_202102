package com.psybergate.grad2021.core.oobasics_part2.hw3a;

public class Customer {
  public static final int MINIMUM_AGE = 18;

  private String customerNumber;

  private String customerName;

  private int customerAge;

  private String customerAddress;

  private String customerType;

  public Customer(String customerNum, String customerName, int customerAge, String customerAddress, String customerType) {
    this.customerNumber = customerNum;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerAddress = customerAddress;
    this.customerType = customerType;
  }

  public static String getFullCustomerType() {
    return "Type: Customer";
  }

  public void increaseCustomerAge(int yearsToIncreaseBy) {
    customerAge += yearsToIncreaseBy;
  }

  public void decreaseCustomerAge(int yearsToDecreaseBy) {
    customerAge -= yearsToDecreaseBy;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "customerNumber='" + customerNumber + '\'' +
        ", customerName='" + customerName + '\'' +
        ", customerAge=" + customerAge +
        ", customerAddress='" + customerAddress + '\'' +
        ", customerType='" + customerType + '\'' +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }

    Customer customer = (Customer) o;

    //use .equals() for Strings
    if (customerNumber.equals(((Customer) o).getCustomerNumber())){
      return true;
    }

    return false;
  }

  public String getCustomerNumber() {
    return this.customerNumber;
  }

}





