package com.psybergate.grad2021.core.oobasics_part2.ce1a.code2;

public class CurrentAccount extends Account {

  private int accountNumber;

  private double overDraft;

  public CurrentAccount(int balance, int accountNumber, double overDraft) {
    super(balance);
    this.accountNumber = accountNumber;
    this.overDraft = overDraft;
    System.out.println("In Current Account.");
    System.out.println("Balance: = " + balance + ", Account Number = " + accountNumber + ", Overdraft: " + overDraft);
  }
}
