package com.psybergate.grad2021.core.oobasics_part2.demo2a;

public class CurrentAccount {
  private int accountNumber;

  private int balance;

  public CurrentAccount(int accountNumber, int balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }

    CurrentAccount currentAccount = (CurrentAccount) o;

    if (accountNumber == ((CurrentAccount) o).getAccountNumber()) {
      return true;
    }

    return false;
  }

  public int getAccountNumber() {
    return accountNumber;
  }
}
