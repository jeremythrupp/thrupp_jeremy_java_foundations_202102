package com.psybergate.grad2021.core.oobasics_part2.ce1a.code3;

public class CurrentAccount extends Account {

  private int accountNumber;

  private double overDraft;

  public CurrentAccount(int balance, int accountNumber, double overDraft) {
    super(balance);
    this.accountNumber = accountNumber;
    this.overDraft = overDraft;
 }

// public Account(){
//
// }
}
