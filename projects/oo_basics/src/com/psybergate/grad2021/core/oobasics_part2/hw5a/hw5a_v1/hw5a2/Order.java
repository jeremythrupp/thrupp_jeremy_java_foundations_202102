package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a2;

public abstract class Order {

  public abstract void addOrderItem(OrderItem orderItem);

  public abstract String getOrderType();

  public abstract int getOrderNumber();

  public abstract double getOrderPrice();
}
