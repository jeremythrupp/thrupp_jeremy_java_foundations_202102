package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

public abstract class Discount  {

  public static final int CUSTOMER_MINIMUM_YEARS = 2;

  public static final int CUSTOMER_MAXIMUM_YEARS = 5;

  public static final int ORDER_MINIMUM_VALUE = 500000;

  public static final int ORDER_MAXIMUM_VALUE = 1000000;

  private double totalOrderValue;

  public Discount(double totalOrderValue) {
    this.totalOrderValue = totalOrderValue;
  }

  public abstract double getDiscountedValue();

  public double getTotalOrderValue() {
    return totalOrderValue;
  }
}
