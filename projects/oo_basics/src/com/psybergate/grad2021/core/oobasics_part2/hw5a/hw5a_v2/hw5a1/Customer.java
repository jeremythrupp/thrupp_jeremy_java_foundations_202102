package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a1;

public class Customer {

  /**
   * customerNumber is the unique identifier for the customer
   */
  private int customerNumber;

  /**
   * customerName is the name for the customer
   */
  private String customerName;

  /**
   * customerAge is the age for the customer
   */
  private int customerAge;

  /**
   * customerType is the type of customer
   */
  private String customerType;

  public Customer(int customerNumber, String customerName, int customerAge, String customerType) {
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerType = customerType;
  }

  public boolean isLocal() {
    if (customerType.equals("Local")) {
      return true;
    }
    return false;
  }

  public boolean isInternational() {
    if (customerType.equals("International")) {
      return true;
    }
    return false;
  }

  public int getCustomerNumber() {
    return customerNumber;
  }

  public String getCustomerName() {
    return customerName;
  }

  public int getCustomerAge() {
    return customerAge;
  }

  public String getCustomerType() {
    return customerType;
  }
}
