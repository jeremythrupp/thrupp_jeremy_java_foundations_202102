package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

public class Customer {

  /**
   * customerNumber is the unique identifier for the customer
   */
  private double customerNumber;

  /**
   * customerName is the name for the customer
   */
  private String customerName;

  /**
   * customerType is the type of customer
   */
  private String customerType;



  public Customer(int customerNumber, String customerName, String customerType) {
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.customerType = customerType;
  }

  public boolean isLocal() {
    if (customerType.equals("Local")) {
      return true;
    }
    return false;
  }

  public boolean isInternational() {
    if (customerType.equals("International")) {
      return true;
    }
    return false;
  }

  public double getCustomerNumber() {
    return customerNumber;
  }

  public String getCustomerName() {
    return customerName;
  }

  public String getCustomerType() {
    return customerType;
  }

}
