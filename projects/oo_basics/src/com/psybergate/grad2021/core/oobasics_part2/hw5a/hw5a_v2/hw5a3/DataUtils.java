package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

import java.time.LocalDate;

public class DataUtils {

  public static final int ORDER_AMOUNT = 4;

  public static Order generateLocalOrder() {
    Order order = new LocalOrder(getRandomNumber(), LocalDate.now(), "Local");
    assignOrderItems(order, ORDER_AMOUNT);
    order.setCustomer(generateLocalCustomer());
    return order;
  }

  public static Order generateInternationalOrder() {
    Order order = new InternationalOrder(getRandomNumber(), LocalDate.now(), "International");
    assignOrderItems(order, ORDER_AMOUNT);
    order.setCustomer(generateInternationalCustomer());
    return order;
  }

  public static Customer generateLocalCustomer() {
    Customer customer = new Customer(getRandomNumber(), "John", "Local");
    return customer;
  }

  public static Customer generateInternationalCustomer() {
    Customer customer = new Customer(getRandomNumber(), "John", "International");
    return customer;
  }

  public static int getRandomNumber() {
    return (int) (100 * Math.random());
  }

  public static void assignOrderItems(Order order, int amount) {
    for (int i = 0; i <= amount; i++) {
      Product product = generateProduct();
      OrderItem orderItem = new OrderItem(product, getRandomNumber());
      order.addOrderItem(orderItem);
    }
  }

  public static Product generateProduct() {
    return new Product(getRandomNumber(), "Eraser", 20);
  }

}
