package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order {

  /**
   * ORDER_TYPE is the type of order.
   * */
  public static final String ORDER_TYPE = "Local";

  /**
   * discount is the initial local order discount in decimal form - it is a percentage.
   */
  private double discount;



  public LocalOrder(int orderNumber, LocalDate orderDate, double discount) {
    super(orderNumber, orderDate);
    this.discount = discount;
  }

  private double applyDiscount(double orderTotal) {
    return orderTotal * (1 - discount);
  }

  @Override
  public double getOrderTotal() {
    return applyDiscount(super.getOrderTotal());
  }

  @Override
  public boolean isValidCustomer(Customer customer) {
    if (customer.isLocal()) {
      return true;
    }
    return false;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}
