package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

public class InternationalDiscount extends Discount {

  public InternationalDiscount(double totalOrderValue) {
    super(totalOrderValue);
  }

  @Override
  public double getDiscountedValue() {
    double discount;

    if (getTotalOrderValue() <= ORDER_MINIMUM_VALUE) {
      discount = 0;
    } else if (getTotalOrderValue() <= ORDER_MAXIMUM_VALUE) {
      discount = 0.05;
    }

    discount = 0.10;

    return getTotalOrderValue() * (1 - discount);
  }

}
