package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order {

  public final String ORDER_TYPE = "Local";

  private int orderNumber;

  private double discount;

  private List<OrderItem> orderItems = new ArrayList();

  public LocalOrder(int orderNumber, double discount) {
    this.orderNumber = orderNumber;
    this.discount = discount;
  }

  public double getOrderPrice() {

    double orderPrice = 0;

    for (OrderItem orderItem : orderItems) {
      orderPrice += (orderItem.getPrice() - (orderItem.getPrice() * discount));
    }

    return orderPrice;
  }

  public static double calculateDiscountPolicy(int customerDuration) {
    if (customerDuration <= 2) {
      return 0;
    }else if(customerDuration <= 5){
      return 0.075;
    }else if(customerDuration > 5){
      return 0.125;
    }
    return 0;
  }

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}
