package com.psybergate.grad2021.core.oobasics_part2.hw2a;

public class Rectangle {
  public static final int MAXIMUM_LENGTH = 200;

  public static final int MAXIMUM_WIDTH = 100;

  public static final int MAXIMUM_AREA = 15000;

  private int length;

  private int width;

  public Rectangle(int length, int width) {
    if (!isValidRectangle(length, width)) {
      System.out.println("You have entered invalid dimensions.");
    }
    this.length = length;
    this.width = width;

  }

  public static String getShapeDescription() {
    return "A rectangle is a shape with two sets of sides, one of which is longer than the other.";
  }

  public static double getCalculatedMaximumArea() {
    return 0.8 * MAXIMUM_LENGTH * MAXIMUM_WIDTH;
  }

  public String getDimensions() {
    return "Length: " + length + ", Width: " + width;
  }

  public int getArea() {
    return length * width;
  }

  public boolean isValidRectangle(int length, int width) {
    return length > width;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Rectangle rectangle = (Rectangle) o;

    return length == rectangle.length && width == rectangle.width;
  }

}
