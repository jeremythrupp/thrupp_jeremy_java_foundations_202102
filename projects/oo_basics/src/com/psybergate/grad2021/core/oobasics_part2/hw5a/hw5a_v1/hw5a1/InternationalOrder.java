package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class InternationalOrder extends Order {

  public static final String ORDER_TYPE = "International";

  /**
   * importDuty is a percentage
   */
  private double importDuty;

  private List<OrderItem> orderItems = new ArrayList();

  public InternationalOrder(int orderNumber, double importDuty) {
    //this.orderNumber = super.getOrderNumber();
    this.importDuty = importDuty;
  }


  @Override
  public double getOrderTotal() {
    return applyImportDuty(super.getOrderTotal());


    }

    public boolean isInternational () {
      return true;
    }

    @Override
    public String getOrderType () {
      return ORDER_TYPE;
    }

    private double applyImportDuty ( double orderTotal){
      return orderTotal * (1 - orderTotal);
    }
  }