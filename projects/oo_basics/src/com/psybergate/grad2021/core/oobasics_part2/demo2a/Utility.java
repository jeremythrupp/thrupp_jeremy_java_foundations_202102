package com.psybergate.grad2021.core.oobasics_part2.demo2a;

import java.util.ArrayList;
import java.util.List;

public class Utility {
  public static void main(String[] args) {

    identity();

    equality();

  }

  public static void identity() {

    int a = 1;
    int b = 1;
    int c = a;

    System.out.println("(a==b) = " + (a == b));
    System.out.println("(a==c) = " + (a == c));

    String word1 = "Hello";
    String word2 = "Hello";
    String word3 = "hello";
    String word4 = String.valueOf("Hello");
    String word5 = new String("Hello");

    System.out.println("(word1==word2) = " + (word1 == word2));
    System.out.println("(word1==word3) = " + (word1 == word3));
    System.out.println("(word1==word4) = " + (word1 == word4));
    System.out.println("(word1==word5) = " + (word1 == word5));
  }

  private static void equality() {
    CurrentAccount currentAccount1 = new CurrentAccount(1, 3000);
    CurrentAccount currentAccount2 = new CurrentAccount(1, 700);
    CurrentAccount currentAccount3 = new CurrentAccount(2, 3000);

    List<CurrentAccount> currentAccounts = new ArrayList();
    currentAccounts.add(currentAccount1);

    if (currentAccounts.contains(currentAccount2)) {
      System.out.println("Object currentAccounts2 is in the Array List.");
    } else {
      System.out.println("Object currentAccounts2 is not in the Array List.");
    }

    if (currentAccount1.equals(currentAccount2)) {
      System.out.println("Object currentAccount1 is equivalent to currentAccount2.");
    } else {
      System.out.println("Object currentAccount1 is not equivalent to currentAccount2.");
    }

    if (currentAccount1.equals(currentAccount3)) {
      System.out.println("Object currentAccount1 is equivalent to currentAccount3");
    } else{
      System.out.println("Object currentAccount1 is not equivalent to currentAccount3");
    }
  }
}
