package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a3;

public class Product {

  private int productNumber;

  private String productName;

  private double price;

  public Product(int productNumber, String productName, double price) {
    this.productNumber = productNumber;
    this.productName = productName;
    this.price = price;
  }

  public int getProductNumber() {
    return productNumber;
  }

  public String getProductName() {
    return productName;
  }

  public double getProductPrice() {
    return price;
  }

}


