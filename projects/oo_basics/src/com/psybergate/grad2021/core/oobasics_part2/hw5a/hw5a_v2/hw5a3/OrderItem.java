package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

public class OrderItem {

  /**
   * product is a single Product of which OrderItem consists of
   */
  private Product product;

  /**
   * quantity is the amount of products ordered
   */
  private int quantity;

  public OrderItem(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public double getOrderItemTotal() {
    return product.getProductPrice() * getQuantity();
  }

  public int getQuantity() {
    return this.quantity;
  }

}
