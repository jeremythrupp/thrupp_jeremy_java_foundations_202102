package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a3;

import java.time.LocalDate;

public class LocalOrder extends Order {

  /**
   * ORDER_TYPE is the type of order.
   */
  public static final String ORDER_TYPE = "Local";

  public LocalOrder(int orderNumber, LocalDate orderDate, String discountPolicyChoice) {
    super(orderNumber, discountPolicyChoice);
  }

  @Override
  public boolean isValidCustomer(Customer customer) {
    if (customer.isLocal()) {
      return true;
    }
    return false;
  }

  @Override
  public boolean isLocal() {
    if (ORDER_TYPE.equals("Local")) {
      return true;
    }
    return false;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}
