package com.psybergate.grad2021.core.oobasics_part2.ce1a.code2;

public class Account {

  private int balance;

  public Account(int balance) {
    this.balance = balance;
    System.out.println("In Account.");
    System.out.println("Balance: " + balance);
  }
}
