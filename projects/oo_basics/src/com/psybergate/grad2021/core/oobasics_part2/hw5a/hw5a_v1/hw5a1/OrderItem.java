package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a1;

public class OrderItem {

  private Product product;

  private int quantity;

  private String processingStatus;

  //

  public OrderItem(Product product, int quantity, String processingStatus) {
    this.product = product;
    this.quantity = quantity;
    this.processingStatus = processingStatus;
  }

  //seperate this
  public double getOrderItemTotal() {
    return product.getProductPrice() * getQuantity();
  }

  public int getQuantity() {
    return this.quantity;
  }

  public String getProcessingStatus() {
    return this.processingStatus;
  }
}
