package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a3;

public abstract class Order {

  public double calculateLocalDiscountPolicy(int customerDuration) {

    if ((customerDuration >= 0) && (customerDuration <= 2)) {
      return 0;
    } else if ((customerDuration > 2) && (customerDuration <= 5)) {
      return 0.075;
    } else if (customerDuration > 5) {
      return 0.125;
    }
    return 0;
  }

  public double calculateInternationalDiscountPolicy(double totalOrderValue) {

    if ((totalOrderValue >= 0) && (totalOrderValue <= 500000)) {
      return 0;
    } else if ((totalOrderValue > 500000) && (totalOrderValue <= 1000000)) {
      return 0.05;
    } else if (totalOrderValue > 1000000) {
      return 0.10;
    }

    return 0;
  }

  public abstract void addOrderItem(OrderItem orderItem);

  public abstract String getDiscountPolicyChoice();

  public abstract String getOrderType();

  public abstract int getOrderNumber();

  public abstract double getOrderPrice();

//  public abstract void setCustomerDuration(int customerDuration);
//
//  public abstract void setTotalOrderValue(double totalOrderValue);
//
//  public abstract void setLocalDiscountPolicy(double localDiscountPolicy);
//
//  public abstract void setInternationalDiscountPolicy(double internationalDiscountPolicy);
}
