package com.psybergate.grad2021.core.oobasics_part2.hw3a;

public class Utility {
  public static void main(String[] args) {

    identity();

    eqaulity();
  }

  private static void identity() {
    Customer customer1 =
        new Customer("a0001", "Outsurance", 13, "213 Main Avenue, Bryanston, Johannesburg. 2190.", "Company");
    Customer customer2 =
        new Customer("a0001", "Momentum", 5, "12 Long Street, Randburg, Johannesburg. 2190.", "Company");
    Customer customer3 =
        new Customer("a0003", "Discovery", 22, "14 Grosvenor Street, Bryanston, Johannebsurg. 2190.", "Company");
    Customer customer4 =
        new Customer("a0001", "Outsurance", 13, "213 Main Avenue, Bryanston, Johannesburg. 2190.", "Company");

    if (customer1 == customer2) {
      System.out.println("customer1 is identical to customer2.");
    } else {
      System.out.println("customer1 is not identical to customer2.");
    }

    if (customer1 == customer3) {
      System.out.println("customer1 is identical to customer3.");
    } else {
      System.out.println("customer1 is not identical to customer3.");
    }

    if (customer1 == customer4) {
      System.out.println("customer1 is identical to customer4.");
    } else {
      System.out.println("customer1 is not identical to customer4.");
    }
  }

  private static void eqaulity() {
    Customer customer1 =
        new Customer("a0001", "Outsurance", 13, "213 Main Avenue, Bryanston, Johannesburg. 2190.", "Company");
    Customer customer2 =
        new Customer("a0001", "Momentum", 5, "12 Long Street, Randburg, Johannesburg. 2190.", "Company");
    Customer customer3 =
        new Customer("a0003", "Discovery", 22, "14 Grosvenor Street, Bryanston, Johannebsurg. 2190.", "Company");
    Customer customer4 =
        new Customer("a0001", "Outsurance", 13, "213 Main Avenue, Bryanston, Johannesburg. 2190.", "Company");

    if (customer1.equals(customer2)) {
      System.out.println("customer1 is equivalent to customer2.");
    } else {
      System.out.println("customer1 is not equivalent to customer2.");
    }

    if (customer1.equals(customer3)) {
      System.out.println("customer1 is equivalent to customer3.");
    } else {
      System.out.println("customer1 is not equivalent to customer3.");
    }

    if (customer1.equals(customer4)) {
      System.out.println("customer1 is equivalent to customer4.");
    } else {
      System.out.println("customer1 is not equivalent to customer4.");
    }

  }
}
