package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v1.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class LocalOrder extends Order {

  public
  final String ORDER_TYPE = "Local";

  private int orderNumber;

  /**
   * discount is a percentage (decimalised)
   */
  private double discount;

  //MAXIMUM discount --> introduce

  private List<OrderItem> orderItems = new ArrayList();

  //does client give or generate orderNumber
  public LocalOrder(int orderNumber, double discount) {
    this.orderNumber = orderNumber;
    this.discount = discount;
  }

  //add isInternational() method aswell!
  public boolean isLocal() {
    return true;
  }

  @Override
  public double getOrderTotal() {
    return applyDiscount(super.getOrderTotal());
  }

  private double applyDiscount(double orderTotal) {
    //use shortcuts! --> refactoring: extracting, inlining etc.
    return orderTotal * (1 - discount);
  }

  public void addOrderItem(OrderItem orderItem) {
    orderItems.add(orderItem);
  }

  public int getOrderNumber() {
    return orderNumber;
  }

  @Override
  public String getOrderType() {
    return ORDER_TYPE;
  }
}
