package com.psybergate.grad2021.core.oobasics_part2.hw5a.hw5a_v2.hw5a1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderUtils {

  public static void main(String[] args) {

    List<Order> orders = createOrders();

    List<Product> products = createProducts();

    List<OrderItem> orderItems = createOrderItems(products);

    List<Customer> customers = createCustomers();

    assignOrderItems(orders, orderItems);

    assignCustomers(orders, customers);

    printOrders(orders);

    Map<Customer, Double> orderTotals = getOrderTotals(orders, customers);

    printOrderTotals(orderTotals, customers);
  }

  private static void printOrderTotals(Map<Customer, Double> orderTotals, List<Customer> customers) {
    for (Customer customer : customers) {

      System.out.println("Value of all orders for Customer: " + customer.getCustomerNumber() + ", " + customer
          .getCustomerType());
      System.out.println("Total Order Value: " + orderTotals.get(customer));
    }
  }

  public static void printOrders(List<Order> orders) {

    for (Order order : orders) {
      System.out.println(order.getOrderType() + " Order Number: " + order.getOrderNumber() + ", Order Price: " + order
          .getOrderTotal());
    }
  }

  private static Map<Customer, Double> getOrderTotals(List<Order> orders, List<Customer> customers) {

    Map<Customer, Double> customerOrderTotals = new HashMap();

    for (Order order : orders) {
      Customer customer = order.getCustomer();
      Double currentValue = customerOrderTotals.get(customer);
      double orderTotal = order.getOrderTotal();

      if (customerOrderTotals.containsKey(customer)) {
        customerOrderTotals.replace(customer, currentValue + orderTotal);
      } else {
        customerOrderTotals.put(customer, orderTotal);
      }
    }

    return customerOrderTotals;
  }

  private static void assignCustomers(List<Order> orders, List<Customer> customers) {

    orders.get(0).setCustomer(customers.get(0));
    orders.get(1).setCustomer(customers.get(1));
    orders.get(2).setCustomer(customers.get(2));
    orders.get(3).setCustomer(customers.get(3));
    orders.get(4).setCustomer(customers.get(4));
    orders.get(5).setCustomer(customers.get(5));

  }

  private static void assignOrderItems(List<Order> orders, List<OrderItem> orderItems) {

    orders.get(0).addOrderItem(orderItems.get(0));
    orders.get(0).addOrderItem(orderItems.get(2));
    orders.get(0).addOrderItem(orderItems.get(3));

    orders.get(1).addOrderItem(orderItems.get(1));
    orders.get(1).addOrderItem(orderItems.get(2));
    orders.get(1).addOrderItem(orderItems.get(3));

    orders.get(2).addOrderItem(orderItems.get(0));
    orders.get(2).addOrderItem(orderItems.get(1));
    orders.get(2).addOrderItem(orderItems.get(2));

    orders.get(3).addOrderItem(orderItems.get(1));
    orders.get(3).addOrderItem(orderItems.get(2));
    orders.get(3).addOrderItem(orderItems.get(3));

    orders.get(4).addOrderItem(orderItems.get(0));
    orders.get(4).addOrderItem(orderItems.get(2));
    orders.get(4).addOrderItem(orderItems.get(3));

    orders.get(5).addOrderItem(orderItems.get(0));
    orders.get(5).addOrderItem(orderItems.get(1));
    orders.get(5).addOrderItem(orderItems.get(2));

  }

  private static List<Product> createProducts() {

    List<Product> products = new ArrayList();

    products.add(new Product(1, "Nike Shoes", 499));
    products.add(new Product(2, "New Balance Hoodie", 599));
    products.add(new Product(3, "Soviet Jeans", 399));
    products.add(new Product(4, "HP Probook 234Y", 25999));

    return products;
  }

  private static List<OrderItem> createOrderItems(List<Product> products) {

    List<OrderItem> orderItems = new ArrayList();

    orderItems.add(new OrderItem(products.get(1), 3, "Processing"));
    orderItems.add(new OrderItem(products.get(3), 7, "Completed"));
    orderItems.add(new OrderItem(products.get(0), 5, "Completed"));
    orderItems.add(new OrderItem(products.get(2), 4, "Processing"));
    orderItems.add(new OrderItem(products.get(2), 6432534, "Completed"));

    return orderItems;
  }

  private static List<Order> createOrders() {

    List<Order> orders = new ArrayList();

    orders.add(new LocalOrder(1, LocalDate.now(), 0.13));
    orders.add(new LocalOrder(2, LocalDate.now(), 0.17));
    orders.add(new LocalOrder(3, LocalDate.now(), 0.18));
    orders.add(new InternationalOrder(1, LocalDate.now(), 0.05));
    orders.add(new InternationalOrder(2, LocalDate.now(), 0.12));
    orders.add(new InternationalOrder(3, LocalDate.now(), 0.22));

    return orders;
  }

  private static List<Customer> createCustomers() {
    List<Customer> customers = new ArrayList();

    customers.add(new Customer(1, "John", 25, "Local"));
    customers.add(new Customer(2, "Alex", 30, "Local"));
    customers.add(new Customer(3, "Steven", 32, "Local"));
    customers.add(new Customer(1, "Ben", 26, "International"));
    customers.add(new Customer(2, "Quinton", 28, "International"));
    customers.add(new Customer(3, "Thamaga", 35, "International"));

    return customers;
  }
}
