package com.psybergate.grad2021.core.oobasics_part2.hw2a;

public class Utility {
  public static void main(String[] args) {
    Rectangle rectangle1 = new Rectangle(7,3);
    Rectangle rectangle2 = new Rectangle(7,3);

    testIdentity(rectangle1, rectangle2);

    testEquality(rectangle1,rectangle2);
  }

  private static void testIdentity(Rectangle rectangle1, Rectangle rectangle2) {
    if (rectangle1 == rectangle2) {
      System.out.println("Object rectangle1 is identical to rectangle2.");
    } else {
      System.out.println("Object rectangle1 is not identical to rectangle2.");
    }
  }

  private static void testEquality(Rectangle rectangle1, Rectangle rectangle2) {
    if (rectangle1.equals(rectangle2)) {
      System.out.println("Object rectangle1 is equivalent to rectangle2.");
    } else {
      System.out.println("Object rectangle1 is not equivalent to rectangle2.");
    }

  }
}
