package com.psybergate.grad2021.core.binary.ce2a;

public class ce2a {


  public static void main(String[] args) {
    byte a = 101;
    byte b = 57;
    byte c = (byte)(a + b);

    //This occurs because of the way Java performs addition.
    //The problem lies in the fact that Java uses 2's complement to perform addition.
    //View Wikipedia link for further information on two's complement:
    //https://en.wikipedia.org/wiki/Two%27s_complement

    System.out.println("The two's complement is calculated by inverting the bits and adding one.");
    System.out.println("a is " + a + ", which is " + Integer.toBinaryString(a) + " in binary.");
    System.out.println("b is " + b + ", which is " + Integer.toBinaryString(b) + " in binary.");
    System.out.println("The answer is: " + c);

  }
}
