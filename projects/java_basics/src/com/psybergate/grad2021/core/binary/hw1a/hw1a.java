package com.psybergate.grad2021.core.binary.hw1a;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class hw1a {
  public static void main(String[] args) throws IOException {

    Path path = Paths.get("src/com/psybergate/grad2021/core/binary/hw1a/CompiledClass.class");
    byte[] raw = java.nio.file.Files.readAllBytes(path);


      int printCount = 0;

    for (int i = 0; i < raw.length - 1; i++) {
      System.out.print(Integer.toHexString(raw[i]) + "|");


      if (printCount == 7) {
        System.out.print("\n");
        printCount = 0;
      }
      printCount++;
    }

  }
}
