package com.psybergate.grad2021.core.binary.ce3a;

public class ce3a {
  public static void main(String[] args) {
    byte b1 = 127;
    byte b2 = -128;

    short s1 = 32767;
    short s2 = -32768;

    int i1 = 2147483647;
    int i2 = -2147483648;

    //Note the 'l' at the end so the compiler will treat this as
    //a long and not as an int.
    long l1 = 9223372036854775807l;
    long l2 = -9223372036854775808l;

    System.out.println("The Maximum and Minimum values of all Java integer types:");
    System.out.println("=========================================================\n");

    System.out.println("byte ");
    System.out.println("Max Value: " + b1);
    System.out.println("Min Value: " + b2);
    System.out.print("\n");

    System.out.println("short ");
    System.out.println("Max Value: " + s1);
    System.out.println("Min Value: " + s2);
    System.out.print("\n");

    System.out.println("int ");
    System.out.println("Max Value: " + i1);
    System.out.println("Min Value: " + i2);
    System.out.print("\n");

    System.out.println("long ");
    System.out.println("Max Value: " + l1);
    System.out.println("Min Value: " + l2);
    System.out.print("\n");
  }

}
