package com.psybergate.grad2021.core.binary.ce1a;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ce1a {
  public static void main(String[] args) throws IOException {
    writeToFile();
    String words = readFile();
    writeBitsDecimal(words);
    writeBitsHexadecimal(words);

  }

  private static void writeBitsDecimal(String words) {

    System.out.println("DECIMAL:");

    char[] lettersArray = words.toCharArray();

    int count = 0;

    for (char letter : lettersArray
    ) {
      System.out.print(Integer.valueOf(letter));
      count++;
      if (count != lettersArray.length) {
        System.out.print("|");
      }

    }

    System.out.println("\n");

  }

  private static void writeBitsHexadecimal(String words) {
    System.out.println("HEXADECIMAL:");

    char[] lettersArray = words.toCharArray();

    int count = 0;

    for (char letter : lettersArray
    ) {
      System.out.print(Integer.toHexString(letter));
      count++;
      if (count != lettersArray.length) {
        System.out.print("|");
      }

    }

    System.out.println("\n");
  }

  private static void writeToFile() throws FileNotFoundException {
    File file = new File("src/com/psybergate/grad2021/core/binary/ce1a/text01.txt");
    PrintWriter printWriter = new PrintWriter(file);

    printWriter.write("The cat sat on the hat.");

    printWriter.flush();
    printWriter.close();


  }

  private static String readFile() throws FileNotFoundException {

    File file = new File("src/com/psybergate/grad2021/core/binary/ce1a/text01.txt");
    Scanner scanner = new Scanner(file);

    String extractedWords = "";

    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      extractedWords += extractedWords.concat(line);
    }

    scanner.close();

    return extractedWords;
  }
}
