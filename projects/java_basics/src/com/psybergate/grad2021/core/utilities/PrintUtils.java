package com.psybergate.grad2021.core.utilities;

import java.util.List;

public class PrintUtils {

  /**
   * Prints the list containing the months.
   *
   * @param list is the list to be printed.
   */
  public static void printList(List<String> list) {
    int i = 1;
    for (String month : list) {
      System.out.println(i + ".) " + month);
      i++;
    }
  }

  /**
   * Prints a statement followed by a new line.
   *
   * @param input the statement to be printed.
   */
  public static void print(Object input) {
    System.out.println(input.toString() + "\n");
  }

  /**
   * Prints a new line.
   */
  public static void printLine() {
    System.out.println("\n");
  }
}
