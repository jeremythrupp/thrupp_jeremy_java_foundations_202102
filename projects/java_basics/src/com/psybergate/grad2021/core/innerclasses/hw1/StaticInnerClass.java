package com.psybergate.grad2021.core.innerclasses.hw1;

import java.time.LocalDate;

public class StaticInnerClass {

  /**
   * Prints the current date by calling printDate() in the Inner class.
   */
  public static void printDate() {
    Inner.printDate();
  }

  /**
   * The Static Inner class.
   */
  static class Inner {

    /**
     * Prints the current date.
     */
    public static void printDate() {
      System.out.println(LocalDate.now());
    }
  }

}
