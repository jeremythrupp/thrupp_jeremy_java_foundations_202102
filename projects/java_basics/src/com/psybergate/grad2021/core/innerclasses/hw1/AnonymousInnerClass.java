package com.psybergate.grad2021.core.innerclasses.hw1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * AnonymouseInnerClass is used to showcase the use of an Anonymouse Inner Class.
 */
public class AnonymousInnerClass {
  public static void main(String[] args) {
    List<String> words = generateWords();
    sort(words);
    print(words);
  }

  /**
   * Sorts a list of words alphabetically using the method within the Anonymous Inner Class.
   * The Anonymous Inner class implements the Comparator interface.
   * @param words is the list of words to be sorted alphabetically.
   */
  public static void sort(List<String> words) {
    Comparator sorter = new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        return ((String) o1).compareTo(((String) o2));
      }
    };

    words.sort(sorter);
  }

  /**
   * Generates a list of words.
   * @return a list of generated words.
   */
  private static List<String> generateWords() {
    List<String> words = new ArrayList<>();
    words.add("Car");
    words.add("Bicycle");
    words.add("Aeroplane");
    words.add("Boat");
    return words;
  }

  /**
   * Prints out the the list of words on separate lines.
   *
   * @param words the list of words to be printed out.
   */
  private static void print(List<String> words) {
    for (String word : words) {
      System.out.println(word);
    }
  }
}
