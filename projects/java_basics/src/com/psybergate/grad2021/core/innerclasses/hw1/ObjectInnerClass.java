package com.psybergate.grad2021.core.innerclasses.hw1;

import java.time.LocalDate;

public class ObjectInnerClass {

  /**
   * Prints the current date by calling printDate() on the Inner class instance.
   */
  public void printDate() {
    ObjectInnerClass.Inner inner = new ObjectInnerClass().new Inner();
    inner.printDate();
  }

  /**
   * The Object Inner class.
   */
  class Inner {

    /**
     * Prints the current date.
     */
    public void printDate() {
      System.out.println(LocalDate.now());
    }
  }
}
