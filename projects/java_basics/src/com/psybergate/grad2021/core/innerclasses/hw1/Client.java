package com.psybergate.grad2021.core.innerclasses.hw1;

public class Client {
  public static void main(String[] args) {
    ObjectInnerClass objectInner1 = new ObjectInnerClass();
    objectInner1.printDate();

    ObjectInnerClass.Inner objectInner2 = new ObjectInnerClass().new Inner();
    objectInner2.printDate();

    StaticInnerClass.printDate();

    StaticInnerClass.Inner.printDate();

    StaticInnerClass.Inner staticInner1 = new StaticInnerClass.Inner();
    staticInner1.printDate();

  }
}
