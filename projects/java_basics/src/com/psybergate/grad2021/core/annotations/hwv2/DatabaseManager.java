package com.psybergate.grad2021.core.annotations.hwv2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseManager {

  public static void generateDatabase() {
    String sql = getCreateSQL();
    passQueryToDatabase(sql);

  }

  public static void passQueryToDatabase(String sql){
    Connection databaseConnection = null;
    Statement executor = null;
    try {
      Class.forName("org.postgresql.Driver");

      databaseConnection = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/customerdb",
              "postgres", "admin");

      executor = databaseConnection.createStatement();
      executor.executeUpdate(sql);
      executor.close();
      databaseConnection.close();
    } catch (Exception e) {
      throw new RuntimeException("Failed to create table", e);
    }
  }

  //TODO - rather use annotation names property instead of field.getName
  public static String getCreateSQL() {

    Field[] fields = Customer.class.getDeclaredFields();

    String sqlQuery = "CREATE TABLE CUSTOMER (";
    int count = 0;

    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sqlQuery = generateSQLQuery(sqlQuery, field, (DomainProperty) annotation);
          break;
        }
      }
      count++;
      if (count < fields.length - 1) {
        sqlQuery = sqlQuery + ", ";
      }
    }
    sqlQuery = sqlQuery + ")";
    return sqlQuery;
  }

  private static String generateSQLQuery(String sqlStatement, Field field, DomainProperty annotation) {
    sqlStatement =
        sqlStatement + annotation.name() + " " + getColumnType(field.getType().getSimpleName());
    if (annotation.primaryKey()) {
      sqlStatement += " PRIMARY KEY";
    }
    if (!annotation.nullable()) {
      sqlStatement = sqlStatement + " NOT NULL";
    }
    return sqlStatement;
  }

  private static String getColumnType(String simpleName) {
    String columnType = "";
    switch (simpleName) {
      case "String":
        columnType = "TEXT";
        break;
      case "Integer":
        columnType = "INT";
        break;
      case "int":
        columnType = "INT";
        break;
    }
    return columnType;
  }
}
