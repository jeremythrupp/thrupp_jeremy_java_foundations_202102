package com.psybergate.grad2021.core.annotations.hwv2;

@DomainClass
public class Customer {

  /**
   * customerNum is a unique identifier for each Customer object
   */
  @DomainProperty(name = "customerNum", primaryKey = true)
  private String customerNum;

  /**
   * name is the first name of the customer
   */
  @DomainProperty(name = "name")
  private String name;


  /**
   * surname is the last name of the customer
   */
  @DomainProperty(name = "surname")
  private String surname;

  /**
   * dateOfBirth is the birthdate of the customer
   */
  @DomainProperty(name = "dateOfBirth")
  private Integer dateOfBirth;

  /**
   * age is the age in years of the customer
   */
  @DomainTransient
  private int age;

  public Customer() {
  }

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth, int age) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = age;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  @Override
  public String toString() {
    String output = "Customer Number: " + customerNum + "\n"
        + "Name: " + name + "\n"
        + "Surname: " + surname + "\n"
        + "Date of Birth: " + dateOfBirth + "\n";

    return output;
  }
}
