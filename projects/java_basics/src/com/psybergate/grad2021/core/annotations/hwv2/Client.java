package com.psybergate.grad2021.core.annotations.hwv2;

public class Client {
  public static void main(String[] args) {
    //ensure customerdb exists and is empty

    DatabaseManager.generateDatabase();

    CustomerService.saveCustomer();

    Customer customer = CustomerService.getCustomer("abc123");

    print(customer);
  }

  private static void print(Customer customer) {
    System.out.println(customer.toString());
  }
}
