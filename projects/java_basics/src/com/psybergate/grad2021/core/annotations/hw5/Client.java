package com.psybergate.grad2021.core.annotations.hw5;

public class Client {
  public static void main(String[] args) {

//required library: http://static.springsource.org/spring/docs/2.5.x/api/org/springframework/context/annotation/ClassPathScanningCandidateComponentProvider.html

    //A component provider that scans the classpath from a base package.
    // It then applies exclude and include filters to the resulting classes to find candidates.

//    ClassPathScanningCandidateComponentProvider scanner =
//        new ClassPathScanningCandidateComponentProvider(<DO_YOU_WANT_TO_USE_DEFALT_FILTER>);
//
//    scanner.addIncludeFilter(new AnnotationTypeFilter(WebListener.class));
//
//    for (BeanDefinition bd : scanner.findCandidateComponents(com.psybergate.grad2021.core.annotations.hw5)){
//      System.out.println(bd.getBeanClassName());
//    }
  }
}
