package com.psybergate.grad2021.core.annotations.hwv1;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CustomerService {

  public static void saveCustomer() {
    DatabaseManager.passQueryToDatabase(getInsertSQL());
  }

  public static void main(String[] args) {
    System.out.println(getInsertSQL());
  }

  private static List<String[]> getFields() {
    List<String[]> databaseRows = new ArrayList<>();

    Field[] customerFields = Customer.class.getDeclaredFields();

    for (Field field : customerFields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          addDatabaseRow(databaseRows, field);
          break;
        }
      }
    }
    return databaseRows;
  }

  private static void addDatabaseRow(List<String[]> databaseRows, Field field) {
    String[] fieldAndType = new String[2];
    fieldAndType[0] = field.getName();
    fieldAndType[1] = field.getType().getSimpleName();
    databaseRows.add(fieldAndType);
  }

  public static Customer getCustomer(String customerNum) {

    List<Object> data = new ArrayList();
    try {
      Class.forName("org.postgresql.Driver");
      Connection databaseConnection = DriverManager
          .getConnection("jdbc:postgresql://localhost:5432/customerdb",
              "postgres", "admin");
      databaseConnection.setAutoCommit(false);

      Statement executor = databaseConnection.createStatement();
      ResultSet output = executor.executeQuery("SELECT * FROM CUSTOMER;");

      addData(customerNum, data, output);
      output.close();
      executor.close();
      databaseConnection.close();
    } catch (Exception e) {
      throw new RuntimeException("Failed to perform queries", e);
    }

    return createCustomerObject(data);
  }

  private static void addData(String customerNum, List<Object> data, ResultSet output) throws SQLException {
    while (output.next()) {
      if (customerNum.equals(output.getString("customerNum"))) {
        for (String[] field : getFields()) {
          switch (field[1]) {
            case "String":
              data.add(output.getString(field[0]));
              break;
            case "Integer":
              data.add(output.getInt(field[0]));
              break;
          }

        }
      }
    }
  }

  public static Customer createCustomerObject(List<Object> data) {
    Customer customer =
        new Customer((String) data.get(0), (String) data.get(1), (String) data.get(2), (Integer) data.get(3));
    return customer;
  }

  private static String getInsertSQL() {
    List<String> fieldNames = new ArrayList<>();

    String sqlQuery = "INSERT INTO CUSTOMER (";

    Customer customer = CustomerUtils.generateCustomer();
    Field[] fields = customer.getClass().getDeclaredFields();

    for (int i = 0; i < fields.length - 1; i++) {
      Field field = fields[i];
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          sqlQuery = sqlQuery + field.getName();
          fieldNames.add(field.getName());
          break;
        }
      }
      if (i <= fields.length - 3) {
        sqlQuery = addComma(sqlQuery);
      }
    }
    sqlQuery = addParenthesis(sqlQuery);

    return appendSQL(sqlQuery, customer, fieldNames);
  }

  private static String appendSQL(String sqlQueryBeginning, Customer customer, List<String> fieldNames) {
    String sql = sqlQueryBeginning + " VALUES (";
    for (Iterator iterator = fieldNames.iterator(); iterator.hasNext();) {
      String field = (String) iterator.next();
      sql += getValue(customer, field);
      if (iterator.hasNext()) {
        sql = addComma(sql);
      }
    }
    sql = addParenthesis(sql);
    return sql;
  }

  private static String addParenthesis(String sql) {
    sql += ")";
    return sql;
  }

  private static String addComma(String sql) {
    sql += ", ";
    return sql;
  }

  private static String getValue(Customer customer, String field) {
    String value = "";
    switch (field) {
      case "customerNum":
        value = "'" + customer.getCustomerNum() + "'";
        break;
      case "name":
        value = "'" + customer.getName() + "'";
        break;
      case "surname":
        value = "'" + customer.getSurname() + "'";
        break;
      case "dateOfBirth":
        value = String.valueOf(customer.getDateOfBirth());
        break;
    }
    return value;
  }

}
