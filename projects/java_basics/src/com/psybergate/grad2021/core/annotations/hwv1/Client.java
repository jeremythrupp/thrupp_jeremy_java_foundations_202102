package com.psybergate.grad2021.core.annotations.hwv1;

public class Client {
  public static void main(String[] args) {
    DatabaseManager.generateDatabase();

    CustomerService.saveCustomer();

    Customer customer = CustomerService.getCustomer("abc123");

    print(customer);
  }

  private static void print(Customer customer) {
    System.out.println(customer.toString());
  }
}
