package com.psybergate.grad2021.core.exceptions.ce3;

public class ExceptionUtility {
  public static void main(String[] args) {
    Student student = new Student(1,"John Doe");

    try {
      student.printDate();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }

  }
}
