package com.psybergate.grad2021.core.exceptions.ce3;

import java.time.LocalDate;

public class Student {
  private int studentNumber;

  private String studentName;

  public Student(int studentNumber, String studentName) {
    this.studentNumber = studentNumber;
    this.studentName = studentName;
  }

  public void printDate() throws Throwable {
    System.out.println("Date: " + LocalDate.now());
    try {
      printStudentCode();
    } catch (Throwable throwable) {
      throw new Throwable(throwable);
    }
  }

  public void printStudentCode() throws Throwable {
    String studentCode = studentNumber + studentName;
    System.out.println("Unique Student Code: " + studentCode);
    throw new Throwable();
  }
}

