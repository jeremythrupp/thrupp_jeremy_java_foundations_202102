package com.psybergate.grad2021.core.exceptions.hw1;

import java.time.LocalDate;
import java.time.LocalTime;

public class CheckedExceptionUtility {
  public static void main(String[] args) {
    try {
      printDate();
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Could not print the date.");
      //squashing the exception - poor practice
    }
  }

  private static void printDate() throws Exception {
    System.out.println(LocalDate.now());
    try {
      printYear();
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception();
    }
  }

  private static void printYear() throws Exception {
    System.out.println(LocalDate.now().getYear());
    try {
      printMonth();
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception();
    }
  }

  private static void printMonth() throws Exception {
    System.out.println(LocalDate.now().getMonth());
    try {
      printDay();
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception();
    }
  }

  private static void printDay() throws Exception {
    System.out.println(LocalDate.now().getDayOfMonth());
    try {
      printTime();
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception();
    }
  }

  private static void printTime() throws Exception{
    System.out.println(LocalTime.now());
    throw new Exception();
  }

}
