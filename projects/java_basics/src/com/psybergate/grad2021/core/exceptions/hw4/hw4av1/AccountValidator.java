package com.psybergate.grad2021.core.exceptions.hw4.hw4av1;

import java.time.LocalDate;

public class AccountValidator {

  //custom exceptions
  public static boolean validateAccountNumber(Integer accountNumber) throws Exception {

    if (accountNumber > 40) {
      throw new Exception("Invalid Account Number.");
    }
    return true;
  }

  public static boolean validateAccountBalance(double accountBalance) throws Exception {
    if (accountBalance > 2000) {
      throw new Exception("Invalid Account Balance.");
    }
    return true;
  }

  public static boolean validateAccountDate(LocalDate accountDate) throws Exception {
    if (accountDate.isBefore(LocalDate.now())) {
      throw new Exception("Invalid Account Date.");
    }
    return true;
  }

  public static void validateAccount(Account account) throws Exception {
    validateAccountNumber(account.getAccountNumber());
    validateAccountBalance(account.getBalance());
    validateAccountDate(account.getAccountDate());
  }
}
