package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DataUtils {
  public static List<Account> generateAccounts(int amount) {
    List<Account> accounts = new ArrayList<>();
    for (int i = 0; i < amount; i++) {
      accounts.add(generateAccount());
    }
    return accounts;
  }

  public static Account generateAccount() {
    return new Account(generateAccountNumber(), generateBalance(), generateDate());
  }

  private static LocalDate generateDate() {
    return LocalDate.now();
  }

  private static double generateBalance() {
    return Math.random() * 1000;
  }

  private static int generateAccountNumber() {
    return (int) (Math.random() * 100);
  }
}
