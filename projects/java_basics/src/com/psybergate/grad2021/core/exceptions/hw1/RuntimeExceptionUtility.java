package com.psybergate.grad2021.core.exceptions.hw1;

import java.time.LocalDate;
import java.time.LocalTime;

public class RuntimeExceptionUtility {
  public static void main(String[] args) {
    printDate();
  }

  private static void printDate() {
    System.out.println(LocalDate.now());
    printYear();
  }

  private static void printYear() {
    System.out.println(LocalDate.now().getYear());
    printMonth();
  }

  private static void printMonth() {
    System.out.println(LocalDate.now().getMonth());
    printDay();
  }

  private static void printDay() {
    System.out.println(LocalDate.now().getDayOfMonth());
    printTime();
  }

  private static void printTime() throws RuntimeException{
    System.out.println(LocalTime.now());
    throw new RuntimeException();
  }

}
