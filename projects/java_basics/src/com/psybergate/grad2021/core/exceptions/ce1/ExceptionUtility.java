package com.psybergate.grad2021.core.exceptions.ce1;

import java.time.LocalDate;

public class ExceptionUtility {
  public static void main(String[] args) {

    try {
      printDate();
    } catch (Exception e) {
      e.printStackTrace();
    }

    printRandomNumber();

  }

  private static void printDate() throws Exception {
    System.out.println("Today's date is: " + LocalDate.now());
    throw new Exception();
  }

  private static void printRandomNumber() {
    System.out.println("A random number is: " + Math.random());
  }

}
