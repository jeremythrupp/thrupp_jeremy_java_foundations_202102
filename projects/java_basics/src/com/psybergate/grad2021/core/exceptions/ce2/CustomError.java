package com.psybergate.grad2021.core.exceptions.ce2;

public class CustomError extends Error{
  public CustomError() {
  }

  public CustomError(String message) {
    super(message);
  }
}
