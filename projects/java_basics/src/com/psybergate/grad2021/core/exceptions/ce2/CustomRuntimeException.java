package com.psybergate.grad2021.core.exceptions.ce2;

public class CustomRuntimeException extends RuntimeException {
  public CustomRuntimeException() {
  }

  public CustomRuntimeException(String message) {
    super(message);
  }
}
