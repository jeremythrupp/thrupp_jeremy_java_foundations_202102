package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.util.HashMap;
import java.util.Map;

public class AccountDB {
  private static Map<Integer, Account> accounts = new HashMap();

  //in proper DB you don't communicate with DB directly
  //the service communicates with the DB directly

  public static void addAccount(Account account) {
    accounts.put(account.getAccountNumber(), account);
  }

  public static Account getAccount(Integer accountNumber){
    return accounts.get(accountNumber);
  }

  public static Map<Integer, Account> getAccounts() {
    return accounts;
  }
}
