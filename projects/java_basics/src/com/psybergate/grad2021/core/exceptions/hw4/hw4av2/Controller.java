package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.util.List;

public class Controller {

  public static void withdraw(Integer accountNumber, double amount) {
    Service.withdraw(accountNumber, amount);
  }

  public static void deposit(Integer accountNumber, double amount) {
    Service.deposit(accountNumber, amount);
  }

  public static void addAccounts(List<Account> accounts) {
    Service.addAccounts(accounts);
  }

  public static void addAccount(Account account) {
    Service.addAccount(account);
  }

  public static void printAccounts() {
    Service.printAccounts();
  }
}
