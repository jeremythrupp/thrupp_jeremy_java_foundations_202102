package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.time.LocalDate;

public class AccountValidator {

  //custom exceptions
  public static boolean validateAccountNumber(Integer accountNumber) throws RuntimeException {

    if (accountNumber > 40) {
      throw new RuntimeException("Invalid Account Number.");
    }
    return true;
  }

  public static boolean validateAccountBalance(double accountBalance) throws RuntimeException {
    if (accountBalance > 2000) {
      throw new RuntimeException("Invalid Account Balance.");
    }
    return true;
  }

  public static boolean validateAccountDate(LocalDate accountDate) throws RuntimeException {
    if (accountDate.isBefore(LocalDate.now())) {
      throw new RuntimeException("Invalid Account Date.");
    }
    return true;
  }

  public static void validateAccount(Account account){
    validateAccountNumber(account.getAccountNumber());
    validateAccountBalance(account.getBalance());
    validateAccountDate(account.getAccountDate());
  }
}
