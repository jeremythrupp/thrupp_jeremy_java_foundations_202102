package com.psybergate.grad2021.core.exceptions.hw2;

import java.sql.SQLException;

public class Car{

  private String vinNumber;

  private String carLicensePlate;

  private String carManufacturer;

  private String carModel;

  public Car(String vinNumber, String carLicensePlate, String carManufacturer, String carModel) {
    this.vinNumber = vinNumber;
    this.carLicensePlate = carLicensePlate;
    this.carManufacturer = carManufacturer;
    this.carModel = carModel;
  }

  public void printVinNumber() throws ApplicationException {
    System.out.println("VIN Number: " + vinNumber);
    try {
      printCarLicensePlate();
    } catch (SQLException throwables) {
      throw new ApplicationException(throwables.getMessage(), throwables);
    }
  }

  public void printCarLicensePlate() throws SQLException {
    System.out.println("License Plate: " + carLicensePlate);
    throw new SQLException("In printCarLicensePlate()");
  }

}
