package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.util.List;

public class ClientRunner {

  public static void main(String[] args){

    List<Account> accounts = DataUtils.generateAccounts(23);

    Controller.addAccounts(accounts);

    Integer accountNumber = accounts.get(1).getAccountNumber();

    Controller.deposit(accountNumber, 5000);
    Controller.withdraw(accountNumber, 2000);

    Controller.addAccount(DataUtils.generateAccount());

    Controller.printAccounts();


  }













}
