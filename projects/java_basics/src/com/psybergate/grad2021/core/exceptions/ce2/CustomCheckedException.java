package com.psybergate.grad2021.core.exceptions.ce2;

public class CustomCheckedException extends Exception{
  public CustomCheckedException() {
  }

  public CustomCheckedException(String message) {
    super(message);
  }
}
