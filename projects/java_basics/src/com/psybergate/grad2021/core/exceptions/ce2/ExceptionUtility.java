package com.psybergate.grad2021.core.exceptions.ce2;

public class ExceptionUtility {
  public static void main(String[] args) {
    Car car = new Car("13A3B3CDJK22HKFH3KHFKK23", "TGH574", "Ford", "Fiesta");
    testCheckedExceptions(car);
    testRuntimeExceptions(car);

  }

  private static void testRuntimeExceptions(Car car) {
    car.printVimNumber();
    car.printCarLicensePlate();
  }

  private static void testCheckedExceptions(Car car) {
    try {
      car.printCarManufacturer();
    } catch (CustomCheckedException customCheckedException) {
      customCheckedException.printStackTrace(); //squashing the exception - poor practice
    }
    try {
      car.printCarModel();
    } catch (CustomThrowable customThrowable) {
      customThrowable.printStackTrace(); //squashing the exception - poor practice
    }
  }
}
