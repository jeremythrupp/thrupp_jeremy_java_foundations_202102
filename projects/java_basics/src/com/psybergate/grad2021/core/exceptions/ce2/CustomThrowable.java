package com.psybergate.grad2021.core.exceptions.ce2;

public class CustomThrowable extends Throwable{
  public CustomThrowable() {
  }

  public CustomThrowable(String message) {
    super(message);
  }
}
