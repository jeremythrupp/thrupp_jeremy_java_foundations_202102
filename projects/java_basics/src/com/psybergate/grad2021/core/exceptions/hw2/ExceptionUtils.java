package com.psybergate.grad2021.core.exceptions.hw2;

public class ExceptionUtils {
  public static void main(String[] args) {
    Car car = new Car("14BDJDKK4562U2344", "VFH789", "Toyota", "Corolla");
    try {
      car.printVinNumber();
    } catch (ApplicationException e) {
      System.out.println(e.getCause());
      e.printStackTrace();
    }
  }
}
