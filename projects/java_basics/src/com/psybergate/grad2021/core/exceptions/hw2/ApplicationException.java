package com.psybergate.grad2021.core.exceptions.hw2;

import java.io.IOException;
import java.sql.SQLException;

public class ApplicationException extends IOException {

  public ApplicationException(String message, SQLException throwables) {
    super(message,throwables);
  }
}
