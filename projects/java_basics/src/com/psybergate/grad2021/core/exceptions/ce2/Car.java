package com.psybergate.grad2021.core.exceptions.ce2;

public class Car {

  private String vimNumber;

  private String carLicensePlate;

  private String carManufacturer;

  private String carModel;

  public Car(String vimNumber, String carLicensePlate, String carManufacturer, String carModel) {
    this.vimNumber = vimNumber;
    this.carLicensePlate = carLicensePlate;
    this.carManufacturer = carManufacturer;
    this.carModel = carModel;
  }

  public void printVimNumber(){
    System.out.println("VIM Number: " + vimNumber);
    throw new CustomRuntimeException();
  }

  public void printCarLicensePlate(){
    System.out.println("License Plate: " + carLicensePlate);
    throw new CustomError();
  }

  public void printCarManufacturer() throws CustomCheckedException {
    System.out.println("Manufacturer: " + carManufacturer);
    throw new CustomCheckedException();
  }

  public void printCarModel() throws CustomThrowable {
    System.out.println("Model: " + carModel);
    throw new CustomThrowable();
  }

}
