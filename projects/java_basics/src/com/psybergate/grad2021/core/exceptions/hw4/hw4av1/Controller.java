package com.psybergate.grad2021.core.exceptions.hw4.hw4av1;

import java.util.List;

public class Controller {

  public static void withdraw(Integer accountNumber, double amount) throws Exception {
    Service.withdraw(accountNumber,amount);
  }

  public static void deposit(Integer accountNumber, double amount) throws Exception {
    Service.deposit(accountNumber,amount);
  }

  public static void addAccounts(List<Account> accounts) throws Exception {
    Service.addAccounts(accounts);
  }

  public static void addAccount(Account account) throws Exception {
    Service.addAccount(account);
  }

  public static void printAccounts() {
    Service.printAccounts();
  }
}
