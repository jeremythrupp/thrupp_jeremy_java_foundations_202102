package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.time.LocalDate;

public class Account {
  private Integer accountNumber;

  private double balance;

  private LocalDate accountDate;

  public Account(Integer accountNumber, double balance, LocalDate accountDate) {
    this.accountNumber = accountNumber;
    this.balance = balance;
    this.accountDate = accountDate;
  }


  public Integer getAccountNumber() {
    return accountNumber;
  }

  public double getBalance() {
    return balance;
  }

  public LocalDate getAccountDate() {
    return accountDate;
  }

  public void withdraw(double amount) {
    balance = balance - amount;
  }

  public void deposit(double amount) {
    balance = balance + amount;
  }
}
