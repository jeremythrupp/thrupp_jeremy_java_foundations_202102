package com.psybergate.grad2021.core.exceptions.hw4.hw4av2;

import java.util.List;
import java.util.Map;

public class Service {

  public static void addAccounts(List<Account> accounts) {
    for (Account account : accounts) {
      AccountDB.addAccount(account);
      AccountValidator.validateAccount(account);
    }
  }

  public static void withdraw(Integer accountNumber, double amount) {
    AccountDB.getAccount(accountNumber).withdraw(amount);
    AccountValidator.validateAccountBalance(accountNumber);

  }

  public static void deposit(Integer accountNumber, double amount) {
    AccountDB.getAccount(accountNumber).deposit(amount);
    AccountValidator.validateAccountBalance(accountNumber);
  }

  public static void addAccount(Account account) {
    AccountValidator.validateAccount(account);
    AccountDB.addAccount(account);
  }

  public static void printAccounts() {
    Map<Integer, Account> accounts = AccountDB.getAccounts();

    for (int i = 0; i < accounts.size(); i++) {
      accounts.toString();
    }

  }
}
