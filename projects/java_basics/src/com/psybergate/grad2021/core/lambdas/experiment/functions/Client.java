package com.psybergate.grad2021.core.lambdas.experiment.functions;

import java.util.function.IntSupplier;
import java.util.function.Predicate;

public class Client {

  public static void main(String[] args) {
    IntSupplier intSupplier = () -> 5;
    System.out.println(intSupplier.getAsInt());

    Predicate isEven = (num) -> (int) num % 2 == 0;
    System.out.println(isEven.test(4));
    System.out.println(isEven.test(5));
  }
}
