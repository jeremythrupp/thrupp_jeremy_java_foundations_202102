package com.psybergate.grad2021.core.lambdas.experiment.printer;

public interface Printer {
  public void print(String word);
}
