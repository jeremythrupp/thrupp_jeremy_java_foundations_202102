package com.psybergate.grad2021.core.lambdas.experiment.printer;

public class Client {
  public static void main(String[] args) {
    Printer printNormal = (word) -> {
      System.out.println(word);
    };

    Printer printerCapital = (word) -> {
      System.out.println(word.toUpperCase());
    };

    printNormal.print("hello");
    printerCapital.print("hello");
  }
}
