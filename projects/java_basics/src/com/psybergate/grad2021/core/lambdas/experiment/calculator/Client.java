package com.psybergate.grad2021.core.lambdas.experiment.calculator;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@FunctionalInterface
interface Calculator {
  public int calculate(int num1, int num2);

  default void print(int result) {
    System.out.println(result);
  }

}

public class Client {
  public static void main(String[] args) {

  }

  public void usePredicate() {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);

    Predicate isEven = new Predicate() {
      @Override
      public boolean test(Object o) {
        int num = (int) o;
        return num % 2 == 0;
      }
    };

    Object[] objs = numbers.stream().filter(isEven).toArray();

    for (Object obj : objs) {
      System.out.println(obj);
    }

  }

  public void calulations() {
    Calculator summer = (num1, num2) -> num1 + num2;

    Calculator multiplier = (num1, num2) -> {
      return num1 * num2;
    };

    int result = multiplier.calculate(4, 5);

    multiplier.print(result);

  }
}
