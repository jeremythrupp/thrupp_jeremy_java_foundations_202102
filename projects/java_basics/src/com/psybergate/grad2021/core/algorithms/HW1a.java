package com.psybergate.grad2021.core.algorithms;

public class HW1a {

    public static void main(String[] args) {
        int theArray[] = new int[1000];
        // int is initialised to 0

        // 0 = closed
        // 1 = open

        for (int i = 1; i <= theArray.length; i++) {
            for (int j = i; j <= theArray.length; j += i) {
                if (theArray[j - 1] == 1) {
                    theArray[j - 1] = 0;
                } else {
                    theArray[j - 1] = 1;
                }
            }
            printTheArray(theArray);
        }

        System.out.print("\n");
        System.out.println("Total Open Doors: " + countTheOpenDoors(theArray));

    }

    private static int countTheOpenDoors(int[] theArray) {
        int result = 0;

        for (int i = 0; i < theArray.length; i++) {
            if (theArray[i] == 1) {
                result = result + 1;
            }
        }

        return result;
    }

    private static void printTheArray(int[] theArray) {
        for (int i = 0; i < theArray.length; i++) {
            System.out.print(theArray[i]);
        }
        System.out.print("\n");

    }

}
