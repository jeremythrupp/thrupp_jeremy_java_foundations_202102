package com.psybergate.grad2021.core.algorithms;

public class HW2a {
    public static void main(String[] args) {
        int totalNumbers = 2000000;


        int compositeCount = 0;

        //1 is a prime number and here we are only
        //counting the composite numbers.

        for (int i = 2; i <= totalNumbers; i++){
            if(i%2==0){
                compositeCount++;
                System.out.println( i + " is a composite number.");

            }else{
                for(int j = 2; j <= totalNumbers; j++){
                    if((i%j == 0)&&(i!=j)){
                        compositeCount++;
                        System.out.println( i + " is a composite number.");
                        break;
                    }

                }
            }


        }
        int primeTotal = totalNumbers - compositeCount;
        System.out.println("\n" + "Total prime numbers: " + primeTotal);
    }
}
