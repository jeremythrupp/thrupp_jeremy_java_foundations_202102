package com.psybergate.grad2021.core.strings.ce2;

public class StringsUtils {
  public static void main(String[] args) {
    String word = "John";

    concat(word);

    join(word);
  }

  private static void join(String word) {
    word.join(word, " World");
    System.out.println(word);
    String result = word.join(word, "World");
    System.out.print(result);
  }

  private static void concat(String word) {
    word.concat(" Doe");
    System.out.println(word);
    System.out.println(word.concat(" Doe"));

    String hello = "Hello";
    String helloWorld = "HelloWorld";
    System.out.println(hello.concat("World") == "HelloWorld");

  }
}
