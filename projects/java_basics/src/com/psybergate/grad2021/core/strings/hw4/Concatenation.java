package com.psybergate.grad2021.core.strings.hw4;

public class Concatenation {
  public static void main(String[] args) {
    System.out.println("Hello" + " World");
    System.out.println("Hello " + 1);
    System.out.println("Hello " + true);
    System.out.println("Hello " + 'a');
    System.out.println(1 + 1);
    System.out.println(1 + 'a');

    Concatenation concatenation = new Concatenation();

//    System.out.println(1 + concatenation);

    //calls toString method
    System.out.println("1" + concatenation);

  }
}
