package com.psybergate.grad2021.core.strings.hw1;

public class Account {
  private int accountNumber;

  private float accountBalance;

  private String accountDescription;

  public Account(int accountNumber, float accountBalance, String accountDescription) {
    this.accountNumber = accountNumber;
    this.accountBalance = accountBalance;
    this.accountDescription = accountDescription;
  }

  public void printAccountNumber() {
    String accountNumber = String.format("%x", this.accountNumber);
    System.out.println("Account Number: " + accountNumber);
  }

  public void printAccountBalance() {
    String accountBalance = String.format("%f", this.accountBalance);
    System.out.println("Account Balance: " + accountBalance);
  }

  public void printAccountNumberHexadecimal() {
    String accountNumberHexadecimal = String.format("%H", this.accountNumber);
    System.out.println("Account Number Hexadecimal: " + accountNumberHexadecimal);
  }
}
