package com.psybergate.grad2021.core.strings.hw2;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Tokenizing {
  public static void main(String[] args) {

    String sentence = "Hello this is a sentence.";

    List<String> words = getWords(sentence);

    printWords(words);
  }

  private static List<String> getWords(String sentence) {
    List<String> words = new ArrayList<>();
    StringTokenizer tokenizer1 = new StringTokenizer(sentence, " ");
    while (tokenizer1.hasMoreElements()) {
      words.add(tokenizer1.nextToken());
    }

    //another way
    StringTokenizer tokenizer2 = new StringTokenizer(sentence, " ");
    int count = tokenizer2.countTokens();
    while (count > 1) {
      count = tokenizer2.countTokens();
      System.out.println(tokenizer2.nextToken());
    }

    return words;

  }

  private static void printWords(List<String> words) {
    for (int i = 0; i < words.size(); i++) {
      System.out.println("Word (" + i + "): " + words.get(i));
    }
  }
}
