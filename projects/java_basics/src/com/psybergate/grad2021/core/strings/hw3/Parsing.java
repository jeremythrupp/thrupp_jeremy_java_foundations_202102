package com.psybergate.grad2021.core.strings.hw3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parsing {
  public static void main(String[] args) {
    System.out.println(validator("0132231"));
  }

  public static final Pattern PHONE_NUMBER_PATTERN =
      Pattern.compile("^[0]+[0-9]+[0-9]$", Pattern.CASE_INSENSITIVE);

  public static boolean validator(String emailStr) {
    Matcher matcher = PHONE_NUMBER_PATTERN.matcher(emailStr);
    return matcher.find();
  }

}
