package com.psybergate.grad2021.core.strings.ce1;

public class StringsUtils {
  public static void main(String[] args) {
    String word = new String("Hello");

    concat(word);

    indexOf(word);

    join(word);

    replace(word, 'e', 'a');

    split(" ");

    trim();



  }

  private static void split(String regex) {
    String sentence = "Hello, this is a sentence.";
    String[] words = sentence.split(regex,4);

    int i = 0;
    for (String word:words) {
      System.out.println(i + ".) " + word);
      i+=1;
    }
  }

  private static void trim() {
    String word = "       Hello   ";
    System.out.println(word.trim());
  }

  private static void replace(String word, char letterToReplace, char letter) {
    System.out.println(word.replace(letterToReplace,letter));
  }

  private static void join(String word) {
    word.join(" ", word, "World");
    System.out.println(word);
    String result = word.join(" " ,word, "World");
    System.out.println(result);
  }

  private static void indexOf(String word) {
    System.out.println(word.indexOf('H'));
    System.out.println(word.indexOf('o'));
  }

  private static void concat(String word) {
    word.concat(" World");
    System.out.println(word);
    System.out.println(word.concat(" World"));
  }
}
