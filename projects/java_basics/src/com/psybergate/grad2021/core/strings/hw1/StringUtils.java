package com.psybergate.grad2021.core.strings.hw1;

public class StringUtils {
  public static void main(String[] args) {

    Account account = new Account(323, 3500, "Account");
    account.printAccountNumber();
    account.printAccountBalance();
    account.printAccountNumberHexadecimal();
  }
}
