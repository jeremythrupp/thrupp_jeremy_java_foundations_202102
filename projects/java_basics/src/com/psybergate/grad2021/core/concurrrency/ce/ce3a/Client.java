package com.psybergate.grad2021.core.concurrrency.ce.ce3a;

public class Client {

  /**
   * Range must be even.
   *
   * @param args
   * @throws InterruptedException
   */
  public static void main(String[] args) throws InterruptedException {
    long startTime = System.currentTimeMillis();
    ThreadUtils.getTotal(1, 1_000_000_00L, 10);
    long endTime = System.currentTimeMillis();
    double timeElapsed = (endTime - startTime) / 1000d;
    System.out.println("Total Time: " + timeElapsed);
  }

}
