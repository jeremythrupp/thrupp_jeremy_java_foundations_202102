package com.psybergate.grad2021.core.concurrrency.hw.hw1a.mycodev2;

public class ReaderRunnable implements Runnable {

  /**
   * A MessageReader instance which will be used to read the messages.
   */
  private MessageReader messageReader = new MessageReader();

  /**
   * A continuous loop that will read the messages.
   * This method runs when its thread is running.
   */
  @Override
  public void run() {
    while (true) {
      read();
    }
  }

  /**
   * Reads the messages.
   */
  public void read() {
    messageReader.read();
  }

}
