package com.psybergate.grad2021.core.concurrrency.hw.hw3c;

public class ReaderRunnable implements Runnable {

  MessageReader messageReader = new MessageReader();

  @Override
  public void run() {
    while (true) {
      read();
    }
  }

  public void read() {
    messageReader.read();
  }

}
