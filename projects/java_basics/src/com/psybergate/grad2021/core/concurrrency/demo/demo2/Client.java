package com.psybergate.grad2021.core.concurrrency.demo.demo2;

public class Client {
  public static void main(String[] args) throws InterruptedException {

    Adder a1 = new Adder(10);

    Worker worker1 = new Worker(a1);
    worker1.start();

    Worker worker2 = new Worker(a1);
    worker2.start();

    Worker worker3 = new Worker(a1);
    worker3.start();

    worker1.join();
    worker2.join();
    worker3.join();

    System.out.println(a1.getValue());

  }
}
