package com.psybergate.grad2021.core.concurrrency.hw.hw1a.polling.v1;

import java.io.FileNotFoundException;

public class ReaderRunnable implements Runnable {

  /**
   * MessageReader instance which has the read() method.
   */
  private MessageReader messageReader;

  public ReaderRunnable(MessageReader messageReader) {
    this.messageReader = messageReader;
  }

  /**
   * Gets the text from the text file when the thread is running.
   */
  @Override
  public void run() {
    getText();
  }

  /**
   * Will read text from the text file.
   *
   * @return the text that is read from the text file.
   */
  public String getText() {
    String text = "";
    try {
      text = messageReader.readText();
      System.out.println(text);
    } catch (FileNotFoundException | InterruptedException e) {
      throw new RuntimeException(e);
    }
    return text;
  }

  /**
   * Reads and removes the text in the text file.
   *
   * @return the text from the text file.
   */
  public String poll() {
    String text = "";
    try {
      text = messageReader.poll();
    } catch (FileNotFoundException e) {
      throw new RuntimeException();
    }
    return text;
  }
}
