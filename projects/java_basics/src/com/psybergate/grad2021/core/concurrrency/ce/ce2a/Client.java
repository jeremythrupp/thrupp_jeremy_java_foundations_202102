package com.psybergate.grad2021.core.concurrrency.ce.ce2a;

/**
 * Daemon thread is a low priority thread (in context of JVM) that runs in background
 * to perform tasks such as garbage collection (gc) etc., they do not prevent the JVM
 * from exiting (even if the daemon thread itself is running) when all the user threads
 * (non-daemon threads) finish their execution. JVM terminates itself when all user
 * threads (non-daemon threads) finish their execution, JVM does not care whether Daemon
 * thread is running or not, if JVM finds running daemon thread (upon completion of user
 * threads), it terminates the thread and after that shutdown itself.
 *
 * Source: https://beginnersbook.com/2015/01/daemon-thread-in-java-with-example
 */
public class Client {
  public static void main(String[] args) throws InterruptedException {


    Thread daemonThread = new Thread(new MyRunnable(new Looper(10000000)), "Daemon Thread");
    Thread userThread = new Thread(new MyRunnable(new Looper(100000)), "User Thread");
//    daemonThread.setDaemon(true);
    userThread.start();
    daemonThread.start();

  }
}
