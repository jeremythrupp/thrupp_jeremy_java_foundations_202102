package com.psybergate.grad2021.core.concurrrency.ce.ce2a;

public class Looper {

  public static final int ZERO = 0;

  /**
   * The end number used in the loop method.
   */
  int endNumber;

  public Looper(int endNumber) {
    this.endNumber = endNumber;
  }

  /**
   * Will loop from 0 until endNumber.
   * Purpose: to display if a thread completed the loop.
   * @throws InterruptedException
   */
  public void loop() throws InterruptedException {
    String threadName = Thread.currentThread().getName();
    System.out.println("Started loop on thread: " + threadName);
    int result = 0;
    for (int i = ZERO; i < endNumber; i++) {
       result++;
     }
     System.out.println("Completed the loop on thread: " + threadName + ", Looped until = " + result);
  }
}
