package com.psybergate.grad2021.core.concurrrency.hw.hw1a.polling.v1;

import java.io.File;

public class Messages {

  /**
   * File containing the messages.
   */
  private File textFile;

  public Messages(File textFile) {
    this.textFile = textFile;
  }

  /**
   *
   * @return the text file.
   */
  public File getTextFile() {
    return textFile;
  }

}
