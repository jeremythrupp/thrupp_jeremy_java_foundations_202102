package com.psybergate.grad2021.core.concurrrency.hw.hw1a.mycodev1;

public class Client {

  public static void main(String[] args) throws InterruptedException {
    ReaderRunnable readerRunnable = new ReaderRunnable();
    WriterRunnable writerRunnable = new WriterRunnable();

    Thread readerThread = new Thread(readerRunnable);
    Thread writerThread = new Thread(writerRunnable);

    writerThread.start();
    readerThread.start();
  }
}
