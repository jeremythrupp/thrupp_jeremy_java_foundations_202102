package com.psybergate.grad2021.core.concurrrency.hw.hw3b;

public class MessageReader {

  public void read() {
    System.out.println(getMessages().read());
  }

  public Messages getMessages() {
    return Messages.getInstance();
  }

}
