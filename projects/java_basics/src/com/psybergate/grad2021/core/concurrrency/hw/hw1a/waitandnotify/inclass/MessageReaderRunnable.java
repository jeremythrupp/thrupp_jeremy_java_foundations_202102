package com.psybergate.grad2021.core.concurrrency.hw.hw1a.waitandnotify.inclass;


public class MessageReaderRunnable implements Runnable {
  @Override
  public void run() {
  MessageReader messageReader = new MessageReader();
    while (true) {
      try {
        System.out.println("Starting to Read");
        String message = messageReader.read();
//        Thread.sleep((int) (Math.random() * 4000));
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }


  }
}
