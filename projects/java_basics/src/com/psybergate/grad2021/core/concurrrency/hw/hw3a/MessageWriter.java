package com.psybergate.grad2021.core.concurrrency.hw.hw3a;

public class MessageWriter {
  private MessageCache getMessageCache() {
    return MessageCache.getInstance();
  }

  public void write(String message) {
    System.out.println("Writing message....");
    getMessageCache().write(message);
  }
}
