package com.psybergate.grad2021.core.concurrrency.hw.experiment;

import java.util.Scanner;

public class Processor {

  public void work1() throws InterruptedException {
    synchronized (this) {
      System.out.println("work1 start");
      wait();
      System.out.println("work1 end");
    }
  }

  public void work2() {
    synchronized (this) {
      System.out.println("work2 start");
      Scanner scanner = new Scanner(System.in);
      scanner.next();
      notify();
      System.out.println("work2 end");
    }
  }

}
