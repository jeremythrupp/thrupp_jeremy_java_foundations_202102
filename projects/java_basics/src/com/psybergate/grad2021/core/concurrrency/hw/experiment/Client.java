package com.psybergate.grad2021.core.concurrrency.hw.experiment;

public class Client {
  public static void main(String[] args) {
    Processor processor = new Processor();

    Thread thread1 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          processor.work1();
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    });

    Thread thread2 = new Thread(new Runnable() {
      @Override
      public void run() {
        processor.work2();
      }
    });

    thread1.start();
    thread2.start();

  }

}


