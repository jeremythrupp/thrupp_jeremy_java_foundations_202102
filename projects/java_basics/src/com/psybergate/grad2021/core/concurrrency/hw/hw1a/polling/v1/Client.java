package com.psybergate.grad2021.core.concurrrency.hw.hw1a.polling.v1;

import java.io.File;

/**
 * First version of demonstrating polling.
 * A more accurate method is shown in package 'mycode'.
 */
public class Client {

  public static void main(String[] args) throws InterruptedException {
    Messages messages = new Messages(new File("output.txt"));

    MessageWriter messageWriter = new MessageWriter(messages);
    MessageReader messageReader = new MessageReader(messages);

    WriterRunnable writer = new WriterRunnable(messageWriter, "This is a sentence.");
    ReaderRunnable reader = new ReaderRunnable(messageReader);
    Thread writerThread = new Thread(writer);
    Thread readerThread = new Thread(reader);

    writerThread.start();
    readerThread.start();
    writerThread.join();
    System.out.println(reader.poll());
    readerThread.join();

  }
}
