package com.psybergate.grad2021.core.concurrrency.hw.hw1a.waitandnotify.inclass;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {
  private final List<String> MESSAGES = new ArrayList<>();

  public static final MessageCache INSTANCE = new MessageCache();

  public static MessageCache getInstance() {
    return INSTANCE;
  }

  public synchronized String read() {
    try {
      wait();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    if (MESSAGES.isEmpty()){
      return null;
    }
    return MESSAGES.remove(0);
  }

  public synchronized void write(String message) {
    MESSAGES.add(message);
    notifyAll();
  }
}
