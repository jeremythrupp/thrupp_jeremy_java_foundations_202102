package com.psybergate.grad2021.core.concurrrency.demo.demo1;

public class MyWorker implements Runnable{

  private Adder adder;

  public MyWorker(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    adder.add(1000_000);
    adder.printValue();
  }


}
