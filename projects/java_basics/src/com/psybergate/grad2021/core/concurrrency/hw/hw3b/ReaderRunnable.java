package com.psybergate.grad2021.core.concurrrency.hw.hw3b;

import java.util.concurrent.*;

public class ReaderRunnable implements Runnable {

  MessageReader messageReader = new MessageReader();

  @Override
  public void run() {
    while (true) {
      try {
        read();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * Reads text from the Messages class.
   * Uses a CountDownLatch to pause for 500 milliseconds before running.
   * The loop will be repeated 100 times.
   * @throws InterruptedException
   */
  public void read() throws InterruptedException {
    CountDownLatch lock = new CountDownLatch(3);

    ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
    ScheduledFuture<?> future = executor.scheduleAtFixedRate(() -> {
      messageReader.read();
      lock.countDown();
    }, 500, 100, TimeUnit.MILLISECONDS);

    lock.await(1000, TimeUnit.MILLISECONDS);
    future.cancel(true);
  }

}
