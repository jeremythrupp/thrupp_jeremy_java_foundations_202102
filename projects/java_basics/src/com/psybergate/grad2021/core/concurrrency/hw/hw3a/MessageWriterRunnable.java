package com.psybergate.grad2021.core.concurrrency.hw.hw3a;

import com.psybergate.grad2021.core.utilities.NumberUtils;

public class MessageWriterRunnable implements Runnable {
  private MessageWriter messageWriter = new MessageWriter();

  @Override
  public void run() {
    write();
  }

  public void write() {
    while (true) {
      String message = "Random String: " + NumberUtils.generateRandomNumber();
      messageWriter.write(message);
    }
  }

}
