package com.psybergate.grad2021.core.concurrrency.hw.hw1a.mycodev1;

/**
 * Reads message from Messages class.
 */
public class MessageReader {

  public void read() {
    System.out.println(getMessages().read());
  }

  /**
   * @return the messages from the Messages class.
   */
  public MessageCache getMessages() {
    return MessageCache.getInstance();
  }

}
