package com.psybergate.grad2021.core.concurrrency.hw.hw3a;

public class Client {
  public static void main(String[] args) throws InterruptedException {
    MessageReaderRunnable readerRunnable = new MessageReaderRunnable();
    MessageWriterRunnable
        writerRunnable = new MessageWriterRunnable();

    Thread readerThread = new Thread(readerRunnable);
    Thread writerThread = new Thread(writerRunnable);

    readerThread.start();
    writerThread.start();

    readerThread.join();
    writerThread.join();

  }

}
