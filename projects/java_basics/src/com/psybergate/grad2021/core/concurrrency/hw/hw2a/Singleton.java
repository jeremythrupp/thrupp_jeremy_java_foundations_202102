package com.psybergate.grad2021.core.concurrrency.hw.hw2a;

/**
 * In this example Singleton Instance is declared as volatile variable to ensure
 * every thread will see updated value for instance.
 *
 * @author Javin Paul
 */
public class Singleton {

  /**
   * A Singleton instance which has will show its updated value.
   */
  private static volatile Singleton instance;

  /**
   * If an instance does not exist, a new instance will be created and returned.
   * If an instance does exist, this instance is returned.
   * @return the instance of this class.
   */
  public static Singleton getInstance() {

    if (instance == null) {
      synchronized (Singleton.class) {
        if (instance == null)
          instance = new Singleton();
      }

    }
    return instance;

  }
}
