package com.psybergate.grad2021.core.concurrrency.ce.ce3a;

import java.util.HashMap;
import java.util.Map;

public class ThreadUtils {
  /**
   * Calculates the sum of the numbers between a specific starting and ending number.
   *
   * @param startingPoint
   * @param endPoint
   * @param range         Will impact the number of threads.
   * @throws InterruptedException
   */
  public static void getTotal(long startingPoint, long endPoint, int range) throws InterruptedException {
    long interval = endPoint / range;
    long end = interval;

    Map<Thread, MyRunnable> threads = new HashMap<>();

    for (long i = startingPoint; i <= range; i++) {
      Adder adder = new Adder(startingPoint, end);
      MyRunnable runnable = new MyRunnable(adder);
      Thread thread = new Thread(runnable);
      threads.put(thread, runnable);
      thread.start();
      end += interval;
      startingPoint += interval;
    }

    long total = 0;

    for (Thread thread : threads.keySet()) {
      thread.join();
      total += threads.get(thread).getSum();
    }

    System.out.println("total = " + total);

  }
}
