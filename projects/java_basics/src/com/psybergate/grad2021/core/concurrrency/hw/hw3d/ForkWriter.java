package com.psybergate.grad2021.core.concurrrency.hw.hw3d;

import java.util.concurrent.RecursiveAction;

public class ForkWriter extends RecursiveAction {

  private MessageWriter messageWriter = new MessageWriter();

  public void write() {
    messageWriter.write();
  }

  /**
   * Will run when this class is executed.
   */
  @Override
  protected void compute() {
    while (true) {
      write();
    }
  }
}
