package com.psybergate.grad2021.core.concurrrency.hw.hw3d;

import java.util.concurrent.ForkJoinPool;

public class Client {
  /**
   * Uses ForkJoinPool to run read from and write to the Messages class.
   * @param args
   * @throws InterruptedException
   */
  public static void main(String[] args) throws InterruptedException {
    ForkReader forkReader = new ForkReader();
    ForkWriter forkWriter = new ForkWriter();

    ForkJoinPool pool = new ForkJoinPool();
    pool.execute(forkWriter);
    pool.execute(forkReader);

    pool.invoke(forkWriter);
    pool.invoke(forkReader);

  }
}
