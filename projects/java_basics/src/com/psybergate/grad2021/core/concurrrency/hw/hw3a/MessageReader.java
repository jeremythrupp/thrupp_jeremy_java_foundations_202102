package com.psybergate.grad2021.core.concurrrency.hw.hw3a;

public class MessageReader {
  public void read() {
    String message;
    while (true) {
      message = getMessageCache().read();
      if (message == null) {
        break;
      }
      System.out.println(message);
    }
  }

  private MessageCache getMessageCache() {
    return MessageCache.getInstance();
  }

}
