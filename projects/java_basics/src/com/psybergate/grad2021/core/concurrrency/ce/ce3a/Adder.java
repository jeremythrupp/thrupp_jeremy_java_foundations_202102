package com.psybergate.grad2021.core.concurrrency.ce.ce3a;

public class Adder {

  private long startNum;

  private long endNum;

  public Adder(long startNum, long endNum) {
    this.startNum = startNum;
    this.endNum = endNum;
  }

  public long getSum() {
    long result = 0;
    for (long i = startNum; i <= endNum; i++) {
      result += i;
    }
    System.out.println(Thread.currentThread().getName());
    return result;
  }
}
