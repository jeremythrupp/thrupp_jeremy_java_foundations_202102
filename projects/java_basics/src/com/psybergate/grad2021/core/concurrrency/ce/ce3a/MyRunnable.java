package com.psybergate.grad2021.core.concurrrency.ce.ce3a;

public class MyRunnable implements Runnable {

  private Adder adder;

  private long sum;

  public MyRunnable(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    sum = adder.getSum();
  }

  public long getSum() {
    return sum;
  }
}
