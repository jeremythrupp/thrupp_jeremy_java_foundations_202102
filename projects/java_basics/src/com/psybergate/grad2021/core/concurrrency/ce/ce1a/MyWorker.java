package com.psybergate.grad2021.core.concurrrency.ce.ce1a;

public class MyWorker implements Runnable {
  @Override
  public void run() {
    doWork1();
  }

  private void doWork1() {
    doWork2();
  }

  private void doWork2() {
    throw new RuntimeException("RuntimeException in doWork2() method.");
  }
}
