package com.psybergate.grad2021.core.concurrrency.demo.demo1;

import static com.psybergate.grad2021.core.utilities.PrintUtils.print;

public class Adder {
  private int value;

  public Adder(int value) {
    this.value = value;
  }

  public void add(int num) {
    print(Thread.currentThread().getName() + ", Value = " + value);
    int temp = 0;
    for (int i = 0; i < num; i++) {
      temp++;
    }
    value += temp;
  }

  public int getValue() {
    return value;
  }

  public void printValue() {
    print(Thread.currentThread().getName() + ", Value = " + value);
  }
}
