package com.psybergate.grad2021.core.concurrrency.hw.hw1a.waitandnotify.inclass;

public class MessageReader {
  public String read() {
    String message;
    while (true) {
      message = getMessageCache().read();
      if (message == null) {
        break;
      }
      System.out.println(message);
    }
    return message;
  }

  private MessageCache getMessageCache() {
    return MessageCache.getInstance();
  }

}
