package com.psybergate.grad2021.core.concurrrency.hw.hw3a;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class MessageCache {
  public static final MessageCache INSTANCE = new MessageCache();

  private final List<String> MESSAGES = new ArrayList<>();

  /**
   * A Semaphore used to prevent race conditions when multiple threads try to access MESSAGES.
   */
  private Semaphore semaphore = new Semaphore(1, true);

  public static MessageCache getInstance() {
    return INSTANCE;
  }


  public synchronized String read() {
    synchronized (this){
    try {
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    if (MESSAGES.isEmpty()) {
      return null;
    }
    }
    String word = MESSAGES.remove(0);
    return word;
  }

  /**
   * Writes a message to the list.
   * This method uses a Semaphore to ensure synchronization.
   * @param message is the message to be written to the list.
   */
  public synchronized void write(String message) {
    try {
      semaphore.acquire();
      MESSAGES.add(message);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    semaphore.release();
  }
}
