package com.psybergate.grad2021.core.concurrrency.hw.hw1a.polling.v1;

public class WriterRunnable implements Runnable {
  /**
   * MessageWriter that has the write() method.
   */
  private MessageWriter messageWriter;

  /**
   * The sentence to write to the text file.
   */
  private String sentence;

  public WriterRunnable(MessageWriter messageWriter, String sentence) {
    this.messageWriter = messageWriter;
    this.sentence = sentence;
  }

  /**
   * Writes the sentence to the text file when the thread is running.
   */
  @Override
  public void run() {
    try {
      messageWriter.write(sentence);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
