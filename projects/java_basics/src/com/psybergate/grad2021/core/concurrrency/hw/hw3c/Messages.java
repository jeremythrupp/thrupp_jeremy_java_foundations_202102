package com.psybergate.grad2021.core.concurrrency.hw.hw3c;

import com.psybergate.grad2021.core.utilities.NumberUtils;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Messages {

  public static final Messages INSTANCE = new Messages();

  /**
   * A concurrent collection used to store the messages.
   * Will prevent a race condition when this field is accessed.
   */
  private Map<Integer, String> concurrentMap = new ConcurrentHashMap<>();

  public static Messages getInstance() {
    return INSTANCE;
  }

  /**
   * Reads messages from the map.
   * @return the messages form the map.
   */
  public String read() {
    System.out.println("Reading...");
    if (concurrentMap.isEmpty()) {
      return null;
    }
    String values = getValues();
    return values;
  }

  /**
   * Gets the messages in the map.
   * @return the messages in the map.
   */
  private String getValues() {
    String temp = "";
    for (Integer key : concurrentMap.keySet()) {
      temp += concurrentMap.get(key) + "\n";
    }
    concurrentMap.clear();
    return temp;
  }

  public void write(String message) {
    System.out.println("Writing...");
    concurrentMap.put(NumberUtils.generateRandomNumber() * 1000, message);
  }

}
