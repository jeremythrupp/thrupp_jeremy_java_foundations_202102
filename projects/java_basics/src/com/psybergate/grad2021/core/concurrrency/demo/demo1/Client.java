package com.psybergate.grad2021.core.concurrrency.demo.demo1;

public class Client {
  public static void main(String[] args) throws InterruptedException {

    Adder a1 = new Adder(10);

    Thread thread = new Thread(new MyWorker(a1));
    thread.start();

    Worker worker = new Worker(a1);
    worker.start();

    worker.join();
    thread.join();

    System.out.println(a1.getValue());

  }
}
