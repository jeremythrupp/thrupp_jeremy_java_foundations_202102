package com.psybergate.grad2021.core.concurrrency.ce.ce4a;

public class Singleton {

  private static Singleton singleton;

  private Singleton() {

  }

  public static Singleton getInstance() {

    if (singleton == null) {
     singleton = new Singleton();
   }
   return singleton;
  }
}
