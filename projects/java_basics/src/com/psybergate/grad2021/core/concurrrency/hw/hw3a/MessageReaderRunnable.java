package com.psybergate.grad2021.core.concurrrency.hw.hw3a;

public class MessageReaderRunnable implements Runnable {
  private MessageReader messageReader = new MessageReader();

  @Override
  public void run() {
    read();
  }

  public void read() {
    while (true) {
      try {
        System.out.println("Starting to Read");
        messageReader.read();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

}

