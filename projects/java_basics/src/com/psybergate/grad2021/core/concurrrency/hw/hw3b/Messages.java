package com.psybergate.grad2021.core.concurrrency.hw.hw3b;

import java.util.ArrayList;
import java.util.List;

public class Messages {

  public static final Messages INSTANCE = new Messages();

  private List<String> messages = new ArrayList<>();

  public static Messages getInstance() {
    return INSTANCE;
  }

  public synchronized String read() {
    if (messages.isEmpty()) {
      return null;
    }
    return messages.remove(0);
  }

  public synchronized void write(String message) {
    messages.add(message);
  }

}
