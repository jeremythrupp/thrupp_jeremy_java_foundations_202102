package com.psybergate.grad2021.core.concurrrency.hw.hw3b;

public class WriterRunnable implements Runnable {

  MessageWriter messageWriter = new MessageWriter();

  @Override
  public void run() {
    while (true) {
      write();
    }
  }
// ----> READ and code
//  volatile keyword
//
//  CountDownLatch;
//  Semaphore;
//  CyclicBarrier;
//  Exchanger;
//  Phaser;
//  ThreadPoolExecutor;
//  ScheduledThreadPoolExecutor;
//  ForkJoinPool;
//  Callable;
//  Future;
//  TimeUnit;
//  ConcurrentMap..;
//  Lock..;
//  Atomic..;

  public void write() {
    messageWriter.write();
  }

}
