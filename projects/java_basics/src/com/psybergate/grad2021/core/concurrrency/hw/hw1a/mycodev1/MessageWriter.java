package com.psybergate.grad2021.core.concurrrency.hw.hw1a.mycodev1;

/**
 * Writes messages to the Messages class.
 */
public class MessageWriter {

  /**
   * Writes a random string to the Messages class.
   */
  public void write() {
    getMessages().write("Random String: " + ((int) (Math.random() * 100)));
  }

  /**
   * @return the messages from the Messages class.
   */
  public MessageCache getMessages() {
    return MessageCache.getInstance();
  }

}
