package com.psybergate.grad2021.core.concurrrency.hw.hw1a.mycodev2;

public class WriterRunnable implements Runnable {

  /**
   * An MessageWriter instance which is used to write messages.
   */
  private MessageWriter messageWriter = new MessageWriter();

  /**
   * A continuous loop that will write messages to the Messages class.
   * This method runs when its thread is running.
   */
  @Override
  public void run() {
    while (true) {
      write();
    }
  }

  /**
   * Writes messages to the Messages class.
   */
  public void write() {
    messageWriter.write();
  }

}
