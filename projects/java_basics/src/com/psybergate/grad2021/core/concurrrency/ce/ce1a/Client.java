package com.psybergate.grad2021.core.concurrrency.ce.ce1a;

public class Client {
  public static void main(String[] args) {
    Thread thread = new Thread(new MyWorker());
    thread.start();

    doWork1();
  }

  private static void doWork1() {
    doWork2();
  }

  private static void doWork2() {
    throw new RuntimeException("RuntimeException in doWork2() method.");
  }
}
