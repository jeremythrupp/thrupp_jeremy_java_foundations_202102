package com.psybergate.grad2021.core.concurrrency.ce.ce2a;

public class MyRunnable implements Runnable{

  private Looper printer;

  public MyRunnable(Looper printer) {
    this.printer = printer;
  }

  @Override
  public void run() {
    try {
      printer.loop();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}
