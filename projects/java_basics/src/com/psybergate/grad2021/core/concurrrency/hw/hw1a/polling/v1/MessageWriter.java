package com.psybergate.grad2021.core.concurrrency.hw.hw1a.polling.v1;

import java.io.FileWriter;
import java.io.IOException;

public class MessageWriter {
  private Messages messages;

  public MessageWriter(Messages messages) {
    this.messages = messages;
  }

  /**
   * Writes text to the text file.
   * @param sentence the line of text to be written to the text file.
   * @throws InterruptedException
   */
  public void write(String sentence) throws InterruptedException {
    Thread.sleep(1_000_0);
    try {
      FileWriter writer = new FileWriter(messages.getTextFile());
      writer.write(sentence);
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

  }
}
