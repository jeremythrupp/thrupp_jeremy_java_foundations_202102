package com.psybergate.grad2021.core.concurrrency.hw.hw1a.waitandnotify.inclass;

/**
 * Code given in class.
 */
public class Client {

  public static void main(String[] args) {
    MessageReaderRunnable readerRunnable = new MessageReaderRunnable();
    MessageWriterRunnable writerRunnable = new MessageWriterRunnable();

    Thread readerThread = new Thread(readerRunnable);
    Thread writerThread = new Thread(writerRunnable);

    readerThread.start();
    writerThread.start();
  }

}
