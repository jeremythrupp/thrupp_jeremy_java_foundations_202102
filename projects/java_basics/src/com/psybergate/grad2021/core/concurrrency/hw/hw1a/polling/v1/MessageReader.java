package com.psybergate.grad2021.core.concurrrency.hw.hw1a.polling.v1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class MessageReader {

  /**
   * The Messages instance which contains the text file.
   */
  private Messages messages;

  public MessageReader(Messages messages) {
    this.messages = messages;
  }

  /**
   * Reads text from the text file.
   * @return the text form the text file.
   * @throws FileNotFoundException
   * @throws InterruptedException
   */
  public String readText() throws FileNotFoundException, InterruptedException {
    Scanner reader = new Scanner(messages.getTextFile());
    String sentence = "";
    while (reader.hasNextLine()) {
      sentence += reader.nextLine();
    }
    reader.close();
    return sentence;
  }

  /**
   * Reads and removes text from the text file.
   * @return the extracted text.
   * @throws FileNotFoundException
   */
  public String poll() throws FileNotFoundException {
    Scanner reader = new Scanner(messages.getTextFile());
    String sentence = "";
    boolean textFound = false;
    while (!textFound) {
      if (reader.hasNextLine()) {
        textFound = true;
      }
    }
    sentence += extractText(reader);
    reader.close();
    return sentence;
  }

  /**
   * Extracts the contents of a file and deletes the contents of the file.
   *
   * @param reader
   * @return The extracted text
   */
  private String extractText(Scanner reader) {
    String text = "";
    while (reader.hasNextLine()) {
      text += reader.nextLine();
    }
    clearFile();
    return text;
  }

  /**
   * Deletes the contents of the file.
   */
  private void clearFile() {
    try {
      PrintWriter writer = new PrintWriter(messages.getTextFile());
      writer.print("");
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
