package com.psybergate.grad2021.core.concurrrency.hw.hw1a.mycodev2;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {

  /**
   * Singleton instance of this class.
   */
  public static final MessageCache
      INSTANCE = new MessageCache();

  /**
   * Stores the messages.
   */
  private List<String> messages = new ArrayList<>();

  /**
   * @return the singleton instance of this class.
   */
  public static MessageCache getInstance() {
    return INSTANCE;
  }

  /**
   * Reads and removes the first message from the messages list.
   *
   * @return the first message in the messages list.
   */
  public synchronized String read() {
    String message = "";
    try {
      System.out.println("Reading...");
      wait();
      if (messages.isEmpty()) {
        return null;
      }
      message = messages.remove(0);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    return message;
  }

  /**
   * Writes a message to the messages list.
   *
   * @param message is the message to be written to the messages list.
   */
  public synchronized void write(String message) {
    System.out.println("Writing...");
    messages.add(message);
    notify();
  }

}
