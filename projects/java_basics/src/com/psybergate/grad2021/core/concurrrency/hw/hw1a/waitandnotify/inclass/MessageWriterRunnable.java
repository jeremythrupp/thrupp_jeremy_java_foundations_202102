package com.psybergate.grad2021.core.concurrrency.hw.hw1a.waitandnotify.inclass;

import com.psybergate.grad2021.core.utilities.NumberUtils;

public class MessageWriterRunnable implements Runnable {

  @Override
  public void run() {
    MessageWriter messageWriter = new MessageWriter();
    while (true) {
      String message = "Random String: " + NumberUtils.generateRandomNumber();
      messageWriter.write(message);
      try {
        Thread.sleep(((int) (Math.random() * 5000)));
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
