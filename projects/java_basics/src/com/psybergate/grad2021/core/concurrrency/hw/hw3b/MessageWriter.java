package com.psybergate.grad2021.core.concurrrency.hw.hw3b;

public class MessageWriter {

  public void write() {
    getMessages().write("Random String: " + ((int) (Math.random() * 100)));
  }

  public Messages getMessages() {
    return Messages.getInstance();
  }

}
