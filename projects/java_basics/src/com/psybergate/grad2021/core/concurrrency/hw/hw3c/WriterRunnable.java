package com.psybergate.grad2021.core.concurrrency.hw.hw3c;

public class WriterRunnable implements Runnable {

  MessageWriter messageWriter = new MessageWriter();

  @Override
  public void run() {
    while (true) {
      write();
    }
  }

  public void write() {
    messageWriter.write();
  }

}
