package com.psybergate.grad2021.core.concurrrency.hw.hw3d;

import java.util.concurrent.RecursiveAction;

public class ForkReader extends RecursiveAction {

  private MessageReader messageReader = new MessageReader();

  public void read() {
    messageReader.read();

  }

  /**
   * Will run when this class is executed.
   */
  @Override
  protected void compute() {
    while (true) {
      read();
    }
  }

}
