package com.psybergate.grad2021.core.concurrrency.demo.demo2;

public class Worker extends Thread{

  private Adder adder;

  public Worker(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    adder.add(1000_000);
  }
}
