package com.psybergate.grad2021.core.enums.hw1;

public enum AccountEnum {

  DEPOSIT_ACCOUNT(),
  CURRENT_ACCOUNT("Current Account", "A transactional account.", 2000),
  SAVINGS_ACCOUNT("Savings Account", "An account used to generate interest", 1000),
  CREDIT_ACCOUNT("Credit Account", "An account with an overdraft.", 6000);

  private String accountType;

  private String accountDescription;

  private double maximumBalance;

  private AccountEnum() {

  }

  private AccountEnum(String accountType, String accountDescription, double maximumBalance) {
    this.accountType = accountType;
    this.accountDescription = accountDescription;
    this.maximumBalance = maximumBalance;
  }

  public String getAccountType() {
    return accountType;
  }

  public String getAccountDescription() {
    return accountDescription;
  }

  public double getMaximumBalance() {
    return maximumBalance;
  }
}
