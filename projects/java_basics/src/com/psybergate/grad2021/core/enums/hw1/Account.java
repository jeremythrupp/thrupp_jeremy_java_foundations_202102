package com.psybergate.grad2021.core.enums.hw1;

public final class Account {

//  public static final Account DEPOSIT_ACCOUNT() = new Account();

  public static final Account CURRENT_ACCOUNT = new Account("Current Account", "A transactional account.", 2000);

  public static final Account SAVINGS_ACCOUNT =
      new Account("Savings Account", "An account used to generate interest", 1000);

  public static final Account CREDIT_ACCOUNT = new Account("Credit Account", "An account with an overdraft.", 6000);

  private String accountType;

  private String accountDescription;

  private double maximumBalance;

  //
//  private Account() {
//  }

  private Account(String accountType, String accountDescription, double maximumBalance) {
    this.accountType = accountType;
    this.accountDescription = accountDescription;
    this.maximumBalance = maximumBalance;
  }


}
