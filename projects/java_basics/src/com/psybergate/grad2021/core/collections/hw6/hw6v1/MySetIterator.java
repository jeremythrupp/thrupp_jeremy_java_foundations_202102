package com.psybergate.grad2021.core.collections.hw6.hw6v1;

import java.util.Iterator;
import java.util.Set;

public class MySetIterator implements Iterator {

  private int position = 0;

//  private Set set;

  private int size;

  private Object[] objects;

  public MySetIterator(Set set, int size) {
//    this.set = set;
    this.size = size;
    objects = set.toArray();
  }

  @Override
  public boolean hasNext() {
    return position < size;
  }

  @Override
  public Object next() {
    return objects[position++];
  }
}
