package com.psybergate.grad2021.core.collections.hw3;

import java.util.SortedSet;
import java.util.TreeSet;

public class SortUtils {
  public static void main(String[] args) {
    SortedSet cars = new TreeSet(new ReverseComparator());

    cars.addAll(generateCars());

    print(cars);
  }

  private static void print(SortedSet cars) {
    for (Object car : cars) {
      System.out.println(car);
    }
  }

  private static SortedSet generateCars() {
    SortedSet cars = new TreeSet();
    cars.add("Mazda");
    cars.add("Ford");
    cars.add("Renault");
    cars.add("Hyundai");
    cars.add("Tesla");
    return cars;
  }
}
