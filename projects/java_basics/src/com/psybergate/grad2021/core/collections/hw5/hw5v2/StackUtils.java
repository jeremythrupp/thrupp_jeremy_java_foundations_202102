package com.psybergate.grad2021.core.collections.hw5.hw5v2;

public class StackUtils {
  public static void main(String[] args) {
//
    SortedStack sortedStack1 = new TreeStack(new AscendingComparator());
//    System.out.println("Add a: ");
//    sortedStack1.add("a");
//    System.out.println("Add b: ");
//    sortedStack1.add("b");
//    System.out.println("Add c: ");
//    sortedStack1.add("c");
//    System.out.println("Add a: ");
//    sortedStack1.add("a");

    sortedStack1.add("c");
    sortedStack1.add("a");
    sortedStack1.add("b");
    sortedStack1.add("g");
    sortedStack1.add("f");
    sortedStack1.add("e");
    sortedStack1.add("d");

//    System.out.println(sortedStack1.toString());

    for (Object o : sortedStack1) {
      System.out.println(o);
    }
  }
}
