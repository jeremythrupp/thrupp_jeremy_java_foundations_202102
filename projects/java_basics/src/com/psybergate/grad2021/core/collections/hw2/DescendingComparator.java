package com.psybergate.grad2021.core.collections.hw2;

import java.util.Comparator;

public class DescendingComparator implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {

    return ((Integer) o2).compareTo((Integer) o1);
  }
}
