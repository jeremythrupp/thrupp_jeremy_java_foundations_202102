package com.psybergate.grad2021.core.collections.maps;

import java.time.LocalDate;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class Maps {
  public static void main(String[] args) {
    Account account = generateAccount();

    int amount = 5;
    SortedMap<Integer, Account> accounts = populateMap(amount);

    printMaps(accounts);


  }

  private static void printMaps(Map<Integer, Account> accounts) {
    for (Account value : accounts.values()) {
      System.out.println(value);
    }
  }

  private static SortedMap<Integer, Account> populateMap(int amount) {
    SortedMap<Integer, Account> accounts = new TreeMap();

    for (int i = 0; i < amount; i++) {
      accounts.put(generateAccount().getAccountNumber(), generateAccount());
    }

    return accounts;
  }

  private static Account generateAccount() {
    return new Account((int) (100 * Math.random()), (int) (100 * Math.random()), LocalDate.now());
  }

}
