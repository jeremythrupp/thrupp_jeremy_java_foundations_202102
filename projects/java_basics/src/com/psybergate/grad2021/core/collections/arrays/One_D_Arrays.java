package com.psybergate.grad2021.core.collections.arrays;

public class One_D_Arrays {
  public static void main(String[] args) {
    int[] values = {1, 2, 3, 4, 5};

    int[] numbers = new int[5];

    numbers = populateArray(5);

    printArrays(numbers);
  }

  private static int[] populateArray(int amount) {
    int[] numbers = new int[amount];
    for (int i = 0; i < amount; i++) {
     int number = (int) (100 * Math.random());
     numbers[i] = number;
    }
    return numbers;
  }

  private static void printArrays(int[] numbers) {
    for (int number : numbers) {
      System.out.println(number);
    }
  }
}
