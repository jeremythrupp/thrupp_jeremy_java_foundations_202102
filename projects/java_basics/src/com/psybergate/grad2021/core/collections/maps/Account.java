package com.psybergate.grad2021.core.collections.maps;

import java.time.LocalDate;

public class Account {
  private int accountNumber;

  private int accountBalance;

  private LocalDate accountDate;

  public Account(int accountNumber, int accountBalance, LocalDate accountDate) {
    this.accountNumber = accountNumber;
    this.accountBalance = accountBalance;
    this.accountDate = accountDate;
  }

  public int getAccountNumber() {
    return accountNumber;
  }
}
