package com.psybergate.grad2021.core.collections.hw5.hw5v1;

import java.util.Comparator;

public class AscendingComparator implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {

    //TODO: rather cast to Comparable
    String str1 = (String) o1;
    String str2 = (String) o2;

    if (str1 == null) {
      str1 = "";
    }

    return str1.compareTo(str2);

  }

}
