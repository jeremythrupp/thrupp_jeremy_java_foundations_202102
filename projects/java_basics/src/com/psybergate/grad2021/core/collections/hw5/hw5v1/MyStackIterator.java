package com.psybergate.grad2021.core.collections.hw5.hw5v1;

import java.util.Iterator;

public class MyStackIterator implements Iterator {

  private int position = 0;

  private Object[] objects;

  private int size;

  public MyStackIterator(Object[] objects, int finalElementIndex) {
    this.objects = objects;
    size = finalElementIndex;
  }

  @Override
  public boolean hasNext() {
    return position < size;
  }

  @Override
  public Object next() {
    return objects[position++];
  }

}
