package com.psybergate.grad2021.core.collections.hw1;

import java.util.ArrayList;
import java.util.List;

public class CustomerUtils {

  public static List generateLocalCustomers(int amount) {
    List customers = new ArrayList();

    for (int i = 0; i < amount; i++) {
      customers.add(generateLocalCustomer());
    }

    return customers;
  }

  public static List generateInternationalCustomers(int amount) {
    List customers = new ArrayList();

    for (int i = 0; i < amount; i++) {
      customers.add(generateInternationalCustomer());
    }

    return customers;
  }

  public static Customer generateLocalCustomer() {
    Customer customer = new Customer(getRandomNumber(), "John", "Local");
    return customer;
  }

  public static Customer generateInternationalCustomer() {
    Customer customer = new Customer(getRandomNumber(), "John", "International");
    return customer;
  }

  public static int getRandomNumber() {
    return (int) (100 * Math.random());
  }
}


