package com.psybergate.grad2021.core.collections.ce1;

public class StackUtils {
  public static void main(String[] args) {
    MyStack myStack = new ArrayMyStack();


    stackPush(myStack, 3);
    System.out.println("myStack.size() = " + myStack.size());
//    stackPop(myStack, 2);
//    System.out.println("myStack.size() = " + myStack.size());


    stackPrint(myStack);

  }

  private static void stackPrint(MyStack myStack) {
    for (Object o : myStack) {
      System.out.println(o);
    }
  }

  private static void testOneItem(MyStack myStack, String word) {
    myStack.push(word);
    System.out.println(myStack.pop());
  }

  private static void stackPop(MyStack myStack, int amount) {
    for (int i = 0; i < amount; i++) {
      myStack.pop();
    }
  }

  private static void print(String topItem) {
    System.out.println("Item: " + topItem);
  }

  private static void stackPush(MyStack myStack, int amount) {
    for (int i = 0; i < amount; i++) {
      myStack.push("Hello World");
    }
  }
}
