package com.psybergate.grad2021.core.collections.hw4.hw4v2;

public class Node {
  private Object object;

  private Node next;

  private Node lastNode;

  public Node(Object object, Node lastNode) {
    this.object = object;
    if (lastNode != null) {
      lastNode.setNext(this);
    }
  }

  public Node getNext() {
    if (next == null) {
      return null;
    } else {
      return next;
    }
  }

  public Object setObject(Object object) {
    Object temp = this.getObject();

    this.object = object;

    return temp;
  }

  public void setNext(Node next) {
    this.next = next;
  }

  public Object getObject() {
    return object;
  }

//  @Override
//  public String toString() {
//    return this.object.toString();
//  }
}
