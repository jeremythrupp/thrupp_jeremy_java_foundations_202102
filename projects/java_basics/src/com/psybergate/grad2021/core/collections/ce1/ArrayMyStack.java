package com.psybergate.grad2021.core.collections.ce1;

import java.util.Iterator;

public class ArrayMyStack extends MyStack {

  private Object[] objects = new Object[100];

  private int stackSize = 0;

  private int finalElementIndex = -1;



  public ArrayMyStack() {
  }

  private void validateSize() {
    if (stackSize - finalElementIndex < 10) {
      increaseStackSize();
    } else if (stackSize - finalElementIndex > 110) {
      decreaseStackSize();
    }
  }

  public void increaseStackSize() {
    stackSize += 100;
    Object[] tempStack = objects;
    objects = new Object[stackSize];
    for (int i = 0; i < tempStack.length; i++) {
      objects[i] = tempStack[i];
    }
  }

  public void decreaseStackSize() {
    stackSize -= 100;
    Object[] tempStack = objects;
    objects = new Object[stackSize];
    for (int i = 0; i < objects.length; i++) {
      objects[i] = tempStack[i];
    }
  }

  public void push(Object o) {
    validateSize();
    finalElementIndex++;
    objects[finalElementIndex] = o;
  }

  public Object pop() {
    validateSize();
    Object retrievedItem = objects[finalElementIndex];
    objects[finalElementIndex] = null;
    finalElementIndex--;
    return retrievedItem;
  }

  public Object get(int index) {
    return objects[index];
  }

  public int size() {
    //return objects.length --> poor encapsulation --> exposing client to internals
    return finalElementIndex+1;
  }

  public boolean isEmpty() {
    return finalElementIndex == -1;
  }

  public boolean contains(Object o) {
    for (Iterator iterator = iterator(); iterator.hasNext(); ) {
      Object obj = iterator.next();
      if (o.equals(obj)) {
        return true;
      }
    }
    return false;
  }

  public Iterator iterator() {
    return new MyStackIterator(objects, finalElementIndex);
  }


}
