package com.psybergate.grad2021.core.collections.hw5.hw5v3;

import java.util.Comparator;
import java.util.Iterator;

public class TreeStack extends SortedStack {

  Object[] stack = new Object[100];

  /**
   * stackSize is the size of the Stack
   */
  private int stackSize = 0;

  /**
   * comparator is the Comparator defined by the user
   */
  private Comparator comparator;

  public TreeStack(Comparator comparator) {
    this.comparator = comparator;
  }

  public TreeStack() {

  }

  public boolean push(Object o) {
    return add(o);
  }

  @Override
  public boolean add(Object o) {

    validateSize();

    if (stackSize == 0) {
      return addFirstElement(o);
    }

    Comparable comparable = (Comparable) o;

    if (comparator == null) {
      for (int i = 0; i < stackSize; i++) {
        int result = comparable.compareTo(stack[i]);
        if (result > 0) {
          return shiftStackAndAdd(i, o);
        }
      }
    } else {
      for (int i = 0; i < stackSize; i++) {
        int result = comparator.compare(o, stack[i]);
        if (result > 0) {
          return shiftStackAndAdd(i, o);
        }
      }

    }

    addBiggerElement(o);

    return true;
  }

  private void addBiggerElement(Object o) {
    stack[stackSize] = o;
    stackSize++;
  }

  private boolean addFirstElement(Object o) {
    stack[0] = o;
    stackSize++;
    return true;
  }

  private boolean shiftStackAndAdd(int i, Object o) {
    for (int j = stackSize; j > i; j--) {
      stack[j] = stack[j - 1];
    }
    stack[i] = o;
    stackSize++;
    return true;
  }

  /**
   * pop() returns the highest element on the Stack
   */
  public Object pop() {
    validateSize();
    Object retrievedItem = stack[stackSize - 1];
    stack[stackSize - 1] = null;
    stackSize--;
    return retrievedItem;
  }

  public Object get(int index) {
    return stack[index];
  }

  @Override
  public Object peek() {
    if (stackSize == 0) {
      return null;
    }
    return stack[stackSize - 1];
  }

  public int size() {
    return stackSize;
  }

  public boolean isEmpty() {
    return stackSize == -1;
  }

  public Iterator iterator() {
    return new MyStackIterator(stack, stackSize);
  }

  public boolean contains(Object o) {
    for (Iterator iterator = iterator(); iterator.hasNext(); ) {
      Object obj = iterator.next();
      if (o.equals(obj)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String toString() {
    String result = "";
    if (stackSize == 0) {
      return "" + stack[stackSize];
    }
    for (int i = 0; i < stackSize; i++) {
      result = result + "\n" + stack[i];
    }
    return result;
  }

  private void validateSize() {
    if (stack.length - stackSize < 10) {
      increaseStackSize();
    } else if (stack.length - stackSize > 110) {
      decreaseStackSize();
    }
  }

  private void increaseStackSize() {
    Object[] tempStack = stack;
    stack = new Object[tempStack.length + 100];
    for (int i = 0; i < tempStack.length; i++) {
      stack[i] = tempStack[i];
    }
  }

  private void decreaseStackSize() {
    Object[] tempStack = stack;
    stack = new Object[tempStack.length - 100];
    for (int i = 0; i < stack.length; i++) {
      stack[i] = tempStack[i];
    }
  }
}
