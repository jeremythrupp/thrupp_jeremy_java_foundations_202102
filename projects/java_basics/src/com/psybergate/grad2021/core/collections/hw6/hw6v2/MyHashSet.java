package com.psybergate.grad2021.core.collections.hw6.hw6v2;

import java.util.*;

public class MyHashSet extends AbstractSet {

  private Map<Object, Integer> hashCodes = new TreeMap<>();

  private Map<Integer, Object> map = new HashMap<>();

  private int mapSize = 0;

  private int generateHash() {
    int hash = 0;
    if (mapSize == 0) {
      return (int) ((Math.random() * 100) + (Math.random() * 100));
    }
    boolean passed = false;
    while (!passed) {
      hash = (int) ((Math.random() * 100) + (Math.random() * 100));
      if (!hashCodes.containsValue(hash)) {
        passed = true;
      }
    }
    return hash;
  }

  public int hashValue(Object o) {
    return hashCodes.get(o);
  }

  @Override
  public boolean add(Object o) {
    if (!contains(o)) {
      int hash = generateHash();
      map.put(hash, o);
      hashCodes.put(o, hash);
      mapSize++;
      return true;
    }
    return false;
  }

  @Override
  public boolean contains(Object o) {
    if(mapSize == 0){
      return false;
    }
    for (Object o1 : map.values()) {
      if (o1.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new MyMapIterator(map.values().toArray());
  }

  @Override
  public int size() {
    return mapSize;
  }

}
