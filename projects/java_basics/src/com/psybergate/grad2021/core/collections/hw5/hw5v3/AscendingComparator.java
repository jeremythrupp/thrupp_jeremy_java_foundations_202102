package com.psybergate.grad2021.core.collections.hw5.hw5v3;

import java.util.Comparator;

public class AscendingComparator implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {

    if(o1 == null){
      return 1;
    }

    return ((Comparable) o1).compareTo(((Comparable) o2));
  }

}
