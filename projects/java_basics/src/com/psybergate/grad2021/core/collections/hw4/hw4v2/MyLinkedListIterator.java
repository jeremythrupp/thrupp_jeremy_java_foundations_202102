package com.psybergate.grad2021.core.collections.hw4.hw4v2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedListIterator implements Iterator {

  private Node head;

  private Node currentNode;

  public MyLinkedListIterator(Node head) {
    this.head = head;
    currentNode = head;
  }

  @Override
  public boolean hasNext() {
    if (currentNode == null) {
      return false;
    }
    if (currentNode.getNext() == null) {
      return false;
    }
    return true;
  }

  @Override
  public Object next() {
    Object temp = currentNode;
    if (currentNode == null) {
      return null;
    }
    if (currentNode.getNext() == null) {
      throw new NoSuchElementException("Reached end of list.");
    }
    currentNode = currentNode.getNext();
    return currentNode.getObject();

  }
}
