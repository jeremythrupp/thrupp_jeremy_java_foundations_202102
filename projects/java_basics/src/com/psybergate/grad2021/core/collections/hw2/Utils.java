package com.psybergate.grad2021.core.collections.hw2;

import java.util.SortedMap;
import java.util.TreeMap;

public class Utils {
  public static void main(String[] args) {

    SortedMap customers = generateCustomers(5);

    printAscending(customers);

    printDescending(customers);

  }

  private static void printAscending(SortedMap customers) {
    System.out.println("Ascending Order:");
    SortedMap customersAscending = new TreeMap(new AscendingComparator());
    customersAscending.putAll(customers);
    printCustomers(customers);
  }

  private static void printDescending(SortedMap customers) {
    System.out.println("Descending Order:");
    SortedMap customersDescending = new TreeMap(new DescendingComparator());
    customersDescending.putAll(customers);
    printCustomers(customers);
  }

  private static void printCustomers(SortedMap customers) {
    for (Object customerNumber : customers.keySet()) {
      customerNumber = (Integer) customerNumber;
      System.out.println(customers.get(customerNumber).toString());
    }
    System.out.print("\n");
  }

  private static SortedMap generateCustomers(int amount) {
    SortedMap customers = new TreeMap();

    for (int i = 0; i < amount; i++) {
      Customer customer = CustomerUtils.generateLocalCustomer();
      customers.put(customer.getCustomerNumber(), customer);
    }

    return customers;
  }
}
