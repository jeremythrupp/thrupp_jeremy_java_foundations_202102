package com.psybergate.grad2021.core.collections.hw6.hw6v2;

import java.util.Set;

public class Client {
  public static void main(String[] args) {
    Set set = new MyHashSet();

    populateSet(set);
    printSize(set);
    printSet(set);
  }

  private static void populateSet(Set set) {
    set.add("a");
    set.add("b");
    set.add("c");
    set.add("a");
    set.add("a");
  }

  private static void printSize(Set set) {
    System.out.println("Set size: " + set.size() + "\n");
  }

  private static void printSet(Set set) {
    System.out.println("Elements in Set: ");
    for (Object o : set) {
      System.out.println(o);
    }
  }
}
