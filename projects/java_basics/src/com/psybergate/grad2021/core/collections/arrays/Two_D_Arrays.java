package com.psybergate.grad2021.core.collections.arrays;

public class Two_D_Arrays {
  public static void main(String[] args) {

//    int[][] values = new int[][3];

    int[][] numbers = new int[3][];
    numbers = populateArrays(numbers, 2);

    int[][] values = new int[3][2];
    values = populateFixedArrays(values);

    printArrays(numbers);
    printArrays(values);
  }

  private static int[][] populateArrays(int[][] numbers, int amount) {
    for (int i = 0; i < numbers.length; i++) {
      numbers[i] = new int[amount];
      for (int j = 0; j < amount; j++) {
        numbers[i][j] = (int) (100 * Math.random());
      }
    }
    return numbers;
  }

  private static int[][] populateFixedArrays(int[][] values) {
    for (int i = 0; i < values.length; i++) {
      for (int j = 0; j < values[i].length; j++) {
        values[i][j] = (int) (100 * Math.random());
      }
    }
    return values;
  }

  private static void printArrays(int[][] values) {
    for (int[] value : values) {
      for (int number : value) {
        System.out.println(number);
      }
    }
  }

}
