package com.psybergate.grad2021.core.collections.hw1;

import java.util.Objects;

public class Customer implements Comparable{

  /**
   * customerNumber is the unique identifier for the customer
   */
  private int customerNumber;

  /**
   * customerName is the name for the customer
   */
  private String customerName;

  /**
   * customerType is the type of customer
   */
  private String customerType;



  public Customer(int customerNumber, String customerName, String customerType){
    this.customerNumber = customerNumber;
    this.customerName = customerName;
    this.customerType = customerType;
  }

  @Override
  public int compareTo(Object o) {
    Customer customer = (Customer) o;

    return this.customerNumber - (customer.getCustomerNumber());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return customerNumber == customer.customerNumber;
  }

  @Override
  public int hashCode() {
    return Objects.hash(customerNumber);
  }

  public int getCustomerNumber() {
    return customerNumber;
  }

  public String getCustomerName() {
    return customerName;
  }

  public String getCustomerType() {
    return customerType;
  }

  @Override
  public String toString() {
    return
        "Customer Number = " + customerNumber +
            ", Customer Name = " + customerName +
            ", Customer Type = " + customerType;
  }



}
