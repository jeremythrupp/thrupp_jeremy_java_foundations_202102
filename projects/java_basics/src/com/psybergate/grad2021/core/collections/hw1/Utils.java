package com.psybergate.grad2021.core.collections.hw1;

import java.util.SortedSet;
import java.util.TreeSet;

public class Utils {
  public static void main(String[] args) {
    printAscendingOrder();

    printDescendingOrder();

  }

  private static void printAscendingOrder() {
    System.out.println("Ascending Order:");
    SortedSet customers = new TreeSet(new AscendingComparator());
    customers.addAll(generateCustomers());
    printCustomers(customers);
  }

  private static void printDescendingOrder() {
    System.out.println("Descending Order:");
    SortedSet customers = new TreeSet(new DescendingComparator());
    customers.addAll(generateCustomers());
    printCustomers(customers);
  }

  private static void printCustomers(SortedSet customers) {
    for (Object obj : customers) {
      Customer customer = (Customer) obj;
      System.out.println(customer.toString());
    }
    System.out.println("\n");
  }

  private static SortedSet generateCustomers() {
    SortedSet customers = new TreeSet();

    customers.addAll(CustomerUtils.generateLocalCustomers(7));
    customers.addAll(CustomerUtils.generateInternationalCustomers(4));

    return customers;
  }
}
