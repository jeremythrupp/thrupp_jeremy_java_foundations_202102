package com.psybergate.grad2021.core.collections.hw4.hw4v1;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyLinkedList implements List {


  private Node head;
  private int size;



  public MyLinkedList() {
    this.head = new Node(null,null);
    this.size = 0;
  }

  @Override
  public boolean add(Object o) {
    Node node = new Node(head.getNext(),o);
    head.setNext(node);
    size++;
    return true;
  }

  @Override
  public boolean remove(Object o) {
    Node node = head.getNext();
    Node prevNode = node;
    for (int i = 0; i < size; i++) {
      if (node.getObject().equals(o)) {
        prevNode.setNext(node.getNext());
        node = null;
        System.out.println("Here." + prevNode.getNext().getObject());
        size--;
        return true;
      }else{
        prevNode = node;
        node = node.getNext();
      }
    }
    return false;
  }

  public Node getFirst() {
    return head.getNext();
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return head == null;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Iterator iterator() {
    return new MyIterator(head);
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public boolean equals(Object o) {
    return false;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public Object get(int index) {
    return null;
  }

  @Override
  public Object set(int index, Object element) {
    return null;
  }

  @Override
  public void add(int index, Object element) {

  }

  @Override
  public Object remove(int index) {
    return null;
  }

  @Override
  public int indexOf(Object o) {
    return 0;
  }

  @Override
  public int lastIndexOf(Object o) {
    return 0;
  }

  @Override
  public ListIterator listIterator() {
    return null;
  }

  @Override
  public ListIterator listIterator(int index) {
    return null;
  }

  @Override
  public List subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }
}
