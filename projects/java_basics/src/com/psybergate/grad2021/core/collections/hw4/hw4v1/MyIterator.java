package com.psybergate.grad2021.core.collections.hw4.hw4v1;

import java.util.Iterator;
import java.util.function.Consumer;

public class MyIterator implements Iterator {

  private Node currentNode;

  public MyIterator(Node currentNode) {
    this.currentNode = currentNode.getNext();
  }

  @Override
  public boolean hasNext() {

    return currentNode != null;
  }

  @Override
  public Object next() {
    Node temp = currentNode;
    currentNode = currentNode.getNext();
    return temp.getObject();
  }

  @Override
  public void remove() {

  }

  @Override
  public void forEachRemaining(Consumer action) {

  }
}
