package com.psybergate.grad2021.core.collections.hw5.hw5v3;

public abstract class SortedStack extends AbstractStack {

  public abstract boolean push(Object o);

  public abstract Object pop();

  public abstract Object get(int index);

  public abstract Object peek();
}
