package com.psybergate.grad2021.core.collections.hw6.hw6v1;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MyHashSet extends AbstractSet {

  private Map<Object, Integer> hashCodes = new TreeMap<>();

  private Set set = new AbstractSet();

  private int setSize = 0;

  private int generateHash() {
    int hash = 0;
    boolean passed = false;
    while (!passed) {
      hash = (int) ((Math.random() * 100) + (Math.random() * 100));
      for (Object hashCode : hashCodes.keySet()) {
        if ((int) hashCode != hash) {
          passed = true;
        }
      }
    }
    return hash;
  }

  public int hashValue(Object o) {
    return hashCodes.get(o);
  }

  @Override
  public boolean add(Object o) {
    if (!set.contains(o)) {
      set.add(o);
      hashCodes.put(o, generateHash());
      setSize++;
      return true;
    }
    return false;
  }

  @Override
  public boolean contains(Object o) {
    for (Object o1 : set) {
      if (o1.equals(o)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new MySetIterator(set, setSize);
  }

  @Override
  public int size() {
    return setSize;
  }

}
