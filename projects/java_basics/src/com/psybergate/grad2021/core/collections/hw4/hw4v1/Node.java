package com.psybergate.grad2021.core.collections.hw4.hw4v1;

public class Node {


  private Node next;

  private Object object;

  public Node(Node next, Object object) {
    this.next = next;
    this.object = object;
  }

  public Node getNext() {
    return next;
  }

  public void setNext(Node next) {
    this.next = next;
  }

  public Object getObject() {
    return object;
  }

  public void setObject(Object object) {
    this.object = object;
  }

  @Override
  public String toString() {
    return ((String) object);
  }
}
