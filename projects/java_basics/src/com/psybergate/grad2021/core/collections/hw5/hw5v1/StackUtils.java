package com.psybergate.grad2021.core.collections.hw5.hw5v1;

public class StackUtils {
  public static void main(String[] args) {

    SortedStack sortedStack1 = new TreeStack(new AscendingComparator());
    stackPush(sortedStack1);
    printStack(sortedStack1);

    SortedStack sortedStack2 = new TreeStack();
    stackPush(sortedStack2);
    printStack(sortedStack2);
  }

  private static void stackPush(SortedStack sortedStack) {
    sortedStack.push("zzz");
    sortedStack.push("xyz");
    sortedStack.push("abc");
    sortedStack.push("xyz");
    sortedStack.push("def");
    sortedStack.push("abc");
    sortedStack.push("ghi");
    sortedStack.push("ghi");
    sortedStack.push("aaa");
    //TODO: cater for top element 'aaa'
    sortedStack.push("aaa");
    sortedStack.push("zzx");
    sortedStack.push("ghi");
    sortedStack.push("ghi");

  }

  private static void printStack(SortedStack sortedStack) {
    for (Object o : sortedStack) {
      System.out.println(o);
    }
    System.out.print("\n");
  }
}
