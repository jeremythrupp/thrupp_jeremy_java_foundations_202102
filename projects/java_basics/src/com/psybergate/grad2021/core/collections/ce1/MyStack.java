package com.psybergate.grad2021.core.collections.ce1;

public abstract class MyStack extends AbstractStack {


  public abstract void push(Object o);

  public abstract Object pop();

  public abstract Object get(int index);


}
