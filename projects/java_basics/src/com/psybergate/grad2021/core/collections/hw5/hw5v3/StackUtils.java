package com.psybergate.grad2021.core.collections.hw5.hw5v3;

public class StackUtils {
  public static void main(String[] args) {
    SortedStack sortedStack1 = new TreeStack(new AscendingComparator());
    //    SortedStack sortedStack1 = new TreeStack();

    sortedStack1.add("c");
    sortedStack1.add("a");
    sortedStack1.add("a");
    sortedStack1.add("z");
    sortedStack1.add("b");
    sortedStack1.add("a");
    sortedStack1.add("a");
    sortedStack1.add("z");

    while (sortedStack1.peek() != null) {
      System.out.println(sortedStack1.pop());
    }
  }
}
