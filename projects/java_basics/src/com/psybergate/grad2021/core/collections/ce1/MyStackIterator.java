package com.psybergate.grad2021.core.collections.ce1;

import java.util.Iterator;
import java.util.function.Consumer;

public class MyStackIterator implements Iterator {

  private int position = 0;

  private Object[] objects;

  private int size;

  public MyStackIterator(Object[] objects, int finalElementIndex) {
    this.objects = objects;
    size = finalElementIndex;
  }

  @Override
  public boolean hasNext() {
    return position < size+1;
  }

  @Override
  public Object next() {
    return objects[position++];
  }

  @Override
  public void remove() {

  }

  @Override
  public void forEachRemaining(Consumer action) {

  }
}
