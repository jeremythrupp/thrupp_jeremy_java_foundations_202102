package com.psybergate.grad2021.core.collections.hw5.hw5v1;

import java.util.Comparator;
import java.util.Iterator;

public class TreeStack extends SortedStack {

  Object[] objects = new Object[100];

  private int stackSize = 0;

  private int finalElementIndex = 0;

  private Comparator comparator;

  public TreeStack(Comparator comparator) {
    this.comparator = comparator;
  }

  public TreeStack() {
    comparator = null;
  }

  private void validateSize() {
    if (stackSize - finalElementIndex < 10) {
      increaseStackSize();
    } else if (stackSize - finalElementIndex > 110) {
      decreaseStackSize();
    }
  }

  public void increaseStackSize() {
    stackSize += 100;
    Object[] tempStack = objects;
    objects = new Object[stackSize];
    for (int i = 0; i < tempStack.length; i++) {
      objects[i] = tempStack[i];
    }
  }

  public void decreaseStackSize() {
    stackSize -= 100;
    Object[] tempStack = objects;
    objects = new Object[stackSize];
    for (int i = 0; i < objects.length; i++) {
      objects[i] = tempStack[i];
    }
  }

  public void push(Object o) {
    validateSize();
    if (!itemExists(o)) {
      add(o);
    }
  }

  public Object pop() {
    validateSize();
    Object retrievedItem = objects[finalElementIndex];
    objects[finalElementIndex] = null;
    finalElementIndex--;
    return retrievedItem;
  }

  public Object get(int index) {
    return objects[index];
  }

  public int size() {
    return objects.length;
  }

  public boolean isEmpty() {
    return finalElementIndex == 0;
  }

  public boolean contains(Object o) {
    for (Iterator iterator = iterator(); iterator.hasNext(); ) {
      Object obj = iterator.next();
      if (o.equals(obj)) {
        return true;
      }
    }
    return false;
  }

  public Iterator iterator() {
    return new MyStackIterator(objects, finalElementIndex);
  }

  public boolean add(Object o) {
    if (comparator != null) {
      return (addComparator(o));
    }
    return (addComparable(o));
  }

  private boolean addComparator(Object o) {
    Comparator comparator = this.comparator;
    addSortedElement(o, comparator);
    finalElementIndex++;
    return true;
  }

  private boolean addComparable(Object o) {
    Comparator comparator = new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
        if (o1 == null) {
          o1 = "";
        }
        return ((String) o1).compareTo(((String) o2));
      }
    };
    addSortedElement(o, comparator);
    finalElementIndex++;
    return true;
  }

  private void addSortedElement(Object o, Comparator comparator) {
    Object topElement = getTopElement();
    Object currentElement = o;
    int result = comparator.compare(topElement, currentElement);
    addCurrentElement(topElement, currentElement, result);
  }

  private Object getTopElement() {
    Object topElement;
    if (finalElementIndex == 0) {
      topElement = objects[finalElementIndex];
    } else {
      topElement = objects[finalElementIndex - 1];
    }
    return topElement;
  }

  private void addCurrentElement(Object topElement, Object currentElement, int result) {
    System.out.println("Top Element: " + topElement + ", Current Element: "  + currentElement + ", Result: " + result);
    if (result < 1) {
      objects[finalElementIndex] = currentElement;
    } else if (topElement != null) {
      objects[finalElementIndex - 1] = currentElement;
      objects[finalElementIndex] = topElement;
    }
  }

  private boolean itemExists(Object o) {
    for (Object object : objects) {
      String object1 = (String) object;
      String object2 = (String) o;
      if (object1 == null) {
        return false;
      }
      if (object1.equals(object2)) {
        return true;
      }
    }
    return false;
  }

}
