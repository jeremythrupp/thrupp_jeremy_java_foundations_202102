package com.psybergate.grad2021.core.collections.hw5.hw5v1;

public abstract class SortedStack extends AbstractStack{

  public abstract void push(Object o);

  public abstract Object pop();

  public abstract Object get(int index);

}
