package com.psybergate.grad2021.core.collections.hw2;

import java.util.Comparator;

public class AscendingComparator implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {

    return ((Integer) o1).compareTo((Integer) o2);
  }
}
