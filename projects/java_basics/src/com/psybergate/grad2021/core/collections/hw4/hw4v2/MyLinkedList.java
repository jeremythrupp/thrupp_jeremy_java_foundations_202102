package com.psybergate.grad2021.core.collections.hw4.hw4v2;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class MyLinkedList extends AbstractList {

  private List list;

  private Node lastNode;

  private Node head;

  public int length;

  public MyLinkedList(List list) {

  }

  public MyLinkedList() {
    head = new Node(null, lastNode);
    lastNode = head;
  }

  @Override
  public Object set(int index, Object element) {

    if (index == length) {
      throw new IndexOutOfBoundsException("Setting a list object when list is empty.");
    }

    Node node = getNode(index);

    return node.setObject(element);
  }

  private Node getNode(int index) {

    if (index == 0 && length == 0) {
      throw new IndexOutOfBoundsException("Accessing a list object when list is empty.");
    }

    if (index == 0 && length == 1) {
      System.out.println(length);
      return head.getNext();
    }

    Node node = head;

    for (int i = 0; i <= index; i++) {
      node = node.getNext();
    }

    return node;
  }

  @Override
  public Object get(int index) {

    return this.getNode(index).getObject();
  }

  @Override
  public boolean add(Object o) {
    Node temp = new Node(o, lastNode);
    lastNode = temp;
    length++;
    return true;
  }

  @Override
  public boolean remove(Object o) {
    Node temp = head;
    for (Object object : this) {
      Node node = (Node) object;
      if (node.getObject().equals((String) o)) {
        temp.setNext(node.getNext());
        node = null;
        length--;
        return true;
      }
      temp = temp.getNext();
    }
    return false;
  }

  @Override
  public Iterator iterator() {

    return new MyLinkedListIterator(head);
  }

  @Override
  public int size() {
    return length;
  }

  public Node getLastNode() {
    return lastNode;
  }

  @Override
  public boolean isEmpty() {
    return length == 0;
  }

  public Node getHead() {
    return head;
  }
}
