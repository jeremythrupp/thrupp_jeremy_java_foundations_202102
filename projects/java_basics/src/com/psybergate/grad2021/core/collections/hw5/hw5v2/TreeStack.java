package com.psybergate.grad2021.core.collections.hw5.hw5v2;

import java.util.Comparator;
import java.util.Iterator;

public class TreeStack extends SortedStack {

  Object[] objects = new Object[100];

  //TODO - Javadoc everything
  private int stackSize = 0;

  private int lastElementIndex = 0;

  private Comparator comparator;

  public TreeStack(Comparator comparator) {
    this.comparator = comparator;
  }

  public TreeStack() {
    comparator = null;
  }

  private void validateSize() {
    if (stackSize - lastElementIndex < 10) {
      increaseStackSize();
    } else if (stackSize - lastElementIndex > 110) {
      decreaseStackSize();
    }
  }

  //TODO MODIFIERS
  private void increaseStackSize() {
    stackSize += 100;
    Object[] tempStack = objects;
    objects = new Object[stackSize];
    for (int i = 0; i < tempStack.length; i++) {
      objects[i] = tempStack[i];
    }
  }

  public void decreaseStackSize() {
    stackSize -= 100;
    Object[] tempStack = objects;
    objects = new Object[stackSize];
    for (int i = 0; i < objects.length; i++) {
      objects[i] = tempStack[i];
    }
  }

  public void push(Object o) {
    validateSize();
    if (!itemExists(o)) {
      add(o);
    }
  }

  public Object pop() {
    validateSize();
    Object retrievedItem = objects[lastElementIndex];
    objects[lastElementIndex] = null;
    lastElementIndex--;
    return retrievedItem;
  }

  public Object get(int index) {
    return objects[index];
  }

  public int size() {
    return lastElementIndex;
  }

  public boolean isEmpty() {
    return lastElementIndex == -1;
  }

  public boolean contains(Object o) {
    for (Iterator iterator = iterator(); iterator.hasNext(); ) {
      Object obj = iterator.next();
      if (o.equals(obj)) {
        return true;
      }
    }
    return false;
  }

  public Iterator iterator() {
    return new MyStackIterator(objects, lastElementIndex);
  }

  @Override
  public boolean add(Object o) {
    if (lastElementIndex == 0) {
      objects[lastElementIndex] = o;
      lastElementIndex++;
      return true;
    }

    for (int i = 0; i < lastElementIndex; i++) {
      int result = comparator.compare(objects[i], o);
      int biggerIndex = i;
      if (result > 0) {
        shiftAndAdd(biggerIndex, o);
        break;
      }
    }

    return true;
  }

  private void shiftAndAdd(int biggerIndex, Object o) {
    Object temp = objects[biggerIndex];
    for (int i = lastElementIndex; i > biggerIndex; i--) {
      objects[i] = objects[i - 1];
    }
    objects[biggerIndex] = temp;
    lastElementIndex++;
  }

  private boolean itemExists(Object o) {
    for (Object object : objects) {
      if (object != null) {
        if (object.equals(o)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public String toString() {
    String result = "";
    if (lastElementIndex == 0) {
      return "" + objects[lastElementIndex];
    }
    for (int i = 0; i < lastElementIndex; i++) {
      result = result + "\n" + objects[i];
    }
    return result;
  }
}
