package com.psybergate.grad2021.core.collections.hw1;

import java.util.Comparator;

public class AscendingComparator implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {
    Customer customer1 = (Customer) o1;
    Customer customer2 = (Customer) o2;
    return customer1.compareTo(customer2);
  }
}
