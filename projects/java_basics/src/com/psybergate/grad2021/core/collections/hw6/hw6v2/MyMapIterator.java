package com.psybergate.grad2021.core.collections.hw6.hw6v2;

import java.util.Iterator;

public class MyMapIterator implements Iterator {

  private int position = 0;

  private Object[] objects;

  public MyMapIterator(Object[] objects) {
    this.objects = objects;
  }

  @Override
  public boolean hasNext() {
    return position < objects.length;
  }

  @Override
  public Object next() {
    return objects[position++];
  }
}
