package com.psybergate.grad2021.core.collections.hw4.hw4v1;

public class Utils {
  public static void main(String[] args) {
    MyLinkedList list = new MyLinkedList();
    list.add("abc");
    list.add("def");
    list.add("ghi");
    list.add("jkl");
    list.add("mno");
    System.out.println("list.size() = " + list.size());
    list.remove("def");
    System.out.println("list.size() = " + list.size());

    System.out.println("list.getFirst().getNext() = " + list.getFirst());
    System.out.println("list.getFirst().getNext() = " + list.getFirst().getNext());
    System.out.println("list.getFirst().getNext() = " + list.getFirst().getNext().getNext());
    System.out.println("list.getFirst().getNext() = " + list.getFirst().getNext().getNext().getNext());
    System.out.println("list.getFirst().getNext() = " + list.getFirst().getNext().getNext().getNext().getNext());

    for (Object o : list) {
      System.out.println(o);
    }

  }
}
