package com.psybergate.grad2021.core.collections.hw7;

import java.util.PriorityQueue;
import java.util.Queue;

public class Client {
  public static void main(String[] args) {
    Queue queue = new PriorityQueue();
    queue.add("1");
    queue.add("2");
    queue.add("3");
    queue.add("4");
    
    peekQueue(queue);
    printQueue(queue);

    pollQueue(queue);
    printQueue(queue);
  }

  private static void printQueue(Queue queue) {
    for (Object o : queue) {
      System.out.println(o);
    }
    System.out.print("\n");
  }

  private static void peekQueue(Queue queue) {
    System.out.println("First in Queue: " + queue.peek());
  }

  private static void pollQueue(Queue queue) {
    System.out.println("Removed: " + queue.poll());
  }
}
