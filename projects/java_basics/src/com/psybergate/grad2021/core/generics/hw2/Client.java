package com.psybergate.grad2021.core.generics.hw2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Client {
  public static void main(String[] args) {
    List<Integer> numbers = new ArrayList<>();
    generateNumbers(numbers, 50);

    List<String> names = new ArrayList<>();
    populateNames(names);

    NumberComparer.greaterThan1000(numbers);
//    NumberComparer.greaterThan1000(names); --> does not compile

  }

  private static void populateNames(List<String> names) {
    names.addAll(Arrays.asList("John", "Alice", "Sipho"));
  }

  private static void generateNumbers(List<Integer> numbers, int amount) {
    for (int i = 0; i < amount; i++) {
      numbers.add((int) (Math.random() * 100));
    }
  }
}
