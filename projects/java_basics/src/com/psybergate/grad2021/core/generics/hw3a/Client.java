package com.psybergate.grad2021.core.generics.hw3a;

import java.util.Collection;

public class Client {
  public static void main(String[] args) {
    Collection<Integer> numbers = DataUtils.generateNumbers();
    Collection<String> names = DataUtils.generateNames();
    Collection<Account> accounts = DataUtils.generateAccounts();

    int evenNumbers = getEvenNumbers(numbers);
    int oddNumbers = getOddNumbers(numbers);
    int primeNumbers = getPrimeNumbers(numbers);
    int namesStartingWithC = getNamesStartingWithCount(names, "c");
    int negativeBalances = getNegativeBalanceCount(accounts);

    System.out.println("Even Numbers: " + evenNumbers);
    System.out.println("Odd Numbers: " + oddNumbers);
    System.out.println("Prime Numbers: " + primeNumbers);
    System.out.println("Names Beginning with 'c':  " + namesStartingWithC);
    System.out.println("Negative Balances: " + negativeBalances);
  }

  private static int getEvenNumbers(Collection<Integer> numbers) {
    int count = 0;
    for (Integer number : numbers) {
      if (number % 2 == 0) {
        count++;
      }
    }
    return count;
  }

  private static int getOddNumbers(Collection<Integer> numbers) {
    int count = 0;
    for (Integer number : numbers) {
      if (number % 2 != 0) {
        count++;
      }
    }
    return count;
  }

  private static int getPrimeNumbers(Collection<Integer> numbers) {
    int count = 0;

    for (Integer number : numbers) {
      if (checkPrime(number)) {
        count++;
      }
    }
    return count;
  }

  private static int getNamesStartingWithCount(Collection<String> names, String letter) {
    int count = 0;
    for (String name : names) {
      String firstLetter = name.substring(0, 1);
      if (firstLetter.equalsIgnoreCase(letter)) {
        count++;
      }
    }
    return count;
  }

  private static int getNegativeBalanceCount(Collection<Account> accounts) {
    int count = 0;
    for (Account account : accounts) {
      if (account.getAccountBalance() < 0) {
        count++;
      }
    }
    return count;
  }

  private static boolean checkPrime(Integer number) {
    if ((number == 0) || (number == 1)) {
      return false;
    }

    for (int i = 2; i < number - 1; i++) {
      if (number % i == 0) {
        return false;
      }
    }

    return true;
  }
}
