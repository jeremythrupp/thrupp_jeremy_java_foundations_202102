package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.List;

public class ClientInJVM {
  public static void main(String[] args) {
    List names1 = new ArrayList<>();
    names1.add("Alice");
    names1.add("John");
    List result1 = reverse(names1);
    List result2 = reverseInJVM(names1);

    printListGenerics(result1);
    printListGenerics(result2);

    List names2 = new ArrayList();
    names2.add("Alice");
    names2.add("John");
    List result3 = reverse(names2);
    List result4 = reverseInJVM(names2);

    printListRawType(result3);
    printListRawType(result4);
  }

  public static List reverse(List names) {
    List<String> list = new ArrayList<>();
    list = names;
    int count = 0;
    for (String name : list) {
      char[] nameLeters = name.toCharArray();
      String reversedName = "";
      for (int i = nameLeters.length - 1; i >= 0; i--) {
        reversedName += nameLeters[i];
      }
      list.set(count, reversedName);
      count++;
    }

    return list;
  }

  public static void printListRawType(List list) {
    for (Object o : list) {
      System.out.println(o);
    }
    System.out.print("\n");
  }

  public static void printListGenerics(List<String> list) {
    for (String s : list) {
      System.out.println(s);
    }
    System.out.print("\n");
  }

  public static List reverseInJVM(List names) {
    List reversedNames = new ArrayList<>();
    reversedNames.add("Alice");
    reversedNames.add("John");

    int count = 0;
    for (Object name : reversedNames) {
      char[] nameLeters = String.valueOf(name).toCharArray();
      String reversedName = "";
      for (int i = nameLeters.length - 1; i >= 0; i--) {
        reversedName += nameLeters[i];
      }
      reversedNames.set(count, reversedName);
      count++;
    }

    return reversedNames;
  }

}
