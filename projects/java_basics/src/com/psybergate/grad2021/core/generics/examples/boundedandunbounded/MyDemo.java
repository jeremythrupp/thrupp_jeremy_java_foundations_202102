package com.psybergate.grad2021.core.generics.examples.boundedandunbounded;

import com.psybergate.grad2021.core.domain.Person;
import com.psybergate.grad2021.core.domain.Student;

import java.util.ArrayList;
import java.util.List;

public class MyDemo {
  public static void main(String[] args) {
    List<String> list1 = new ArrayList<>();
    List<Integer> list2 = new ArrayList<>();
    List list3 = new ArrayList();
//    myMethod1(list1); myMethod1(list2); myMethod1(list3);
//    myMethod2(list1); myMethod2(list2);  myMethod2(list3);
//    myMethod3(list1);   myMethod3(list2);  myMethod3(list3);
    myMethod4(list1);
    myMethod4(list2);
    myMethod4(list3);
  }

  public static void myMethod5(List<? extends Person> list) {
    //List & list.E = Unknown
//    list.add(new Student());
//    list.add(new Person());
    Person p1 = list.get(0); //works because E extends Person
//    Student s1 = list.get(0); //generics knows nothing about inheritance
  }

  public static void myMethod4(List<?> list) {
//    list.add("abc");
//    list.add(1);
//    list.add((Object) "abc");
    list.get(0);
    //List & list.E = unknown
  }

  public static void myMethod2(List<Object> list) {
    //List & list.E = Object
    list.add(3);
    list.set(1, "abc");
  }

  public static void myMethod1(List list) {
    //List & list.E =Object

  }

  public static void myMethod3(List<Integer> list) {
    //compiler trusts you with raw types so you can pass raw type list here
  }
}
