package com.psybergate.grad2021.core.generics.examples.box;

public class Box<T> {
  private T t;

  public Box() {
  }

  public void set(final T t) {
    this.t = t;
  }

  public T get() {
    return t;
  }

}
