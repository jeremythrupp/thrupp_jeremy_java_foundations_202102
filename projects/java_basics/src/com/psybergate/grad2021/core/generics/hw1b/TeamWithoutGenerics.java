package com.psybergate.grad2021.core.generics.hw1b;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>Team</code> class can contain a team of any subtype of <code>Person</code>
 */
public class TeamWithoutGenerics {

  private static final int MAX_TEAM_SIZE = 15;

  private List people = new ArrayList();

  public static int maxTeamSize() {
    return MAX_TEAM_SIZE;
  }

  public void addPerson(Object t) {
    people.add(t);
  }

  public void anyMethod(Object t) {
    // do nothing
  }

  public Object getPerson(int position) {
    return people.get(position);
  }

  public int getNumofPersons() {
    return people.size();
  }

  @SuppressWarnings("unchecked")
  // Perhaps also show this method 2 generic type parameters
  public Object[] asArray(Object[] e) {
    for (int i = 0; i < people.size(); i++) {
      e[i] = (Object) getPerson(i);
    }
    return e;
  }

  public void someRandom(List<Person> names) {
    names.add(new Student());
    System.out.println(names.get(1));
  }

}
