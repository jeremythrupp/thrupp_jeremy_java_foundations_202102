package com.psybergate.grad2021.core.generics.examples.toolbox;

public class ToolBox <T extends Tool>{
  private T t;

  public void set(final T t) {
    this.t = t;
  }

  public T get() {
    return t;
  }
}
