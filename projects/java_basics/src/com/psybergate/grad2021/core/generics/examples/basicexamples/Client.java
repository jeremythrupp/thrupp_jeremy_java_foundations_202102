package com.psybergate.grad2021.core.generics.examples.basicexamples;

import java.util.ArrayList;
import java.util.List;

public class Client {
  public static void main(String[] args) {
    listgeneric();
    listrawtype();
  }

  private static void listgeneric() {
    List<String> names = new ArrayList<>();
    names.add("Alice");
    names.add("Jane");
    names.add("John");
    for (String name : names) {
      System.out.println("name");
    }
  }

  private static void listrawtype() {
    List objects = new ArrayList();
    objects.add(new Car());
    objects.add(new Student());
    objects.add("abc");
    objects.add(3);
  }
}
