package com.psybergate.grad2021.core.generics.bridgemethod;


public class BridgeClass extends Calculator<Double> {
  @Override
  public double add(Double num1, Double num2) {
    return num1 + num2;
  }

  //Bridge Method
//  public double add(Object num1, Object num2) {
//    return ((Double) num1) + ((Double) num2);
//  }
}
