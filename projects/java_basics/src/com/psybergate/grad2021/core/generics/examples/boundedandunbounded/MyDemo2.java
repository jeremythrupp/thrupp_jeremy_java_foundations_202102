package com.psybergate.grad2021.core.generics.examples.boundedandunbounded;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyDemo2 {
  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    max3(list);
  }

  public static <T extends Object & Comparable<? super T>> T max3(Collection<T> coll) {
    //Always put the Class before the Interface
    //Object is type of T
    //Separate class from interface by using &
    //Comparable is interface of T
    //? --> (T must implement Comparable)

    return null;
  }

  public static <T extends Object & Comparable> T max4(Collection<T> coll) {
    //Always put the Class before the Interface
    //Object is type of T
    //Separate class from interface by using &
    //Comparable is interface of T
    //? --> (T must implement Comparable)

    return null;
  }

}
