package com.psybergate.grad2021.core.generics.hw3b;

public interface Counter<T> {
  public abstract boolean test(T object);
}
