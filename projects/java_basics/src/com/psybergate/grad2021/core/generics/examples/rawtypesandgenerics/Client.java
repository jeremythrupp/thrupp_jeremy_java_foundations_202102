package com.psybergate.grad2021.core.generics.examples.rawtypesandgenerics;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Client {
  public static void main(String[] args) {
    List l1 = new ArrayList();
    anyMethod1(l1);
    anyMethod2(l1);
    anyMethod3(l1);

    List<String> l2 = new ArrayList<>();
    anyMethod1(l2);
//    anyMethod2(l2);
//    anyMethod3(l2);

  }

  public static void anyMethod1(List list) {
    list.add(new File("filename"));
    list.add("John");
  }

  public static void anyMethod2(List<Object> list) {
    list.add(new File("filename"));
    list.add("John");
  }

  public static void anyMethod3(List<Integer> list) {
    list.add(5);
  }
}
