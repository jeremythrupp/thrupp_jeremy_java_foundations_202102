package com.psybergate.grad2021.core.generics.hw3b;

import java.util.ArrayList;
import java.util.List;

public class DataUtils {
  public static List<Integer> generateNumbers() {
    List<Integer> numbers = new ArrayList<>();
    numbers.add(72);
    numbers.add(50);
    numbers.add(64);
    numbers.add(7);
    numbers.add(9);
    numbers.add(11);
    numbers.add(13);

    return numbers;
  }

  public static List<String> generateNames() {
    List<String> names = new ArrayList<>();
    names.add("John");
    names.add("Cate");
    names.add("Clarissa");
    names.add("Alice");
    names.add("Sipho");
    return names;
  }

  public static List<Account> generateAccounts() {
    List<Account> accounts = new ArrayList<>();
    accounts.add(new Account(1, 100));
    accounts.add(new Account(2, -23));
    accounts.add(new Account(3, 44));
    accounts.add(new Account(4, 500));
    return accounts;
  }

}
