package com.psybergate.grad2021.core.generics.hw4a;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Example2 {
  public static void main(String[] args) {
    Integer[] numbers = {3,5,9,11};
    String[] names = {"Alice", "Matthew", "John", "Sipho"};

    List<Integer> list1 = fromArrayToList(numbers);
    List<String> list2 = fromArrayToList(names);
  }

  public static <T> List<T> fromArrayToList(T[] a) {
    return Arrays.stream(a).collect(Collectors.toList());
  }
}
