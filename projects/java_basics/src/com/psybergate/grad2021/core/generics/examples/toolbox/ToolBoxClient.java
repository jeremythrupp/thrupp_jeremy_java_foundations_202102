package com.psybergate.grad2021.core.generics.examples.toolbox;

public class ToolBoxClient {
  public static void main(String[] args) {
    example1();
    example2();
  }

  private static void example1() {
    ToolBox<Tool> toolBox = new ToolBox<>();
    toolBox.set(new Hammer());
    toolBox.set(new Chisel());

//    Hammer hammer1 = toolBox.get(); --> need to cast to Hammer
    Hammer hammer2 = (Hammer) toolBox.get();
  }

  private static void example2() {
    ToolBox<Spanner> toolBox1 = new ToolBox<>();
    ToolBox<Chisel> toolBox2 = new ToolBox<>();
    toolBox2.set(new Chisel());

    Tool tool = toolBox2.get();

    Chisel chisel = toolBox2.get();


  }
}
