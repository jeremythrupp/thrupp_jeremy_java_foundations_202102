package com.psybergate.grad2021.core.generics.hw4a;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Example1 {
  public static void main(String[] args) {
    rawType();
    generics();
  }

  private static void rawType() {
    List list = new LinkedList();
    list.add(new Integer(1));
//    Integer i = list.iterator().next();
    Integer i = (Integer) list.iterator().next();
  }

  private static void generics() {
    List<Integer> list = new ArrayList<>();
    list.add(new Integer(1));
    Integer i = list.iterator().next();
  }
}
