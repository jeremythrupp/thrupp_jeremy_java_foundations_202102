package com.psybergate.grad2021.core.generics.hw3b;

public class Account {

  private int accountNumber;

  private int accountBalance;

  public Account(int accountNumber, int accountBalance) {
    this.accountNumber = accountNumber;
    this.accountBalance = accountBalance;
  }

  public int getAccountBalance() {
    return accountBalance;
  }
}
