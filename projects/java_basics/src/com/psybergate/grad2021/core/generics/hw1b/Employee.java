package com.psybergate.grad2021.core.generics.hw1b;

/**
 * 
 * @since 01 Aug 2010
 */
public class Employee extends Person {

  private int yearEmployed;

  public Employee() {
  }

  public Employee(String name, int age) {
    super(name, age);
    yearEmployed = 2011;
  }

  public int getYearEmployed() {
    return yearEmployed;
  }

}
