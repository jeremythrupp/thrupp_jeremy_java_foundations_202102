package com.psybergate.grad2021.core.generics.hw3b;

public class PrimeCounter<T> implements Counter<T> {
  @Override
  public boolean test(T object) {
    Integer temp = (Integer) object;
    if ((temp == 0) || (temp == 1)) {
      return false;
    }

    for (int i = 2; i < temp - 1; i++) {
      if (temp % i == 0) {
        return false;
      }
    }

    return true;
  }
}
