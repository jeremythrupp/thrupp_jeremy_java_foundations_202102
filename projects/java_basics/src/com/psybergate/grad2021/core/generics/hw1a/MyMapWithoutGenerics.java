package com.psybergate.grad2021.core.generics.hw1a;

public class MyMapWithoutGenerics<K,V> {

  private K key;
  private V value;

  public MyMapWithoutGenerics(K key, V value) {
    this.key = key;
    this.value = value;
  }

  public K getKey() {
    return key;
  }

  public V getValue() {
    return value;
  }

  public void setKey(K key) {
    this.key = key;
  }

  public void setValue(V value) {
    this.value = value;
  }
}
