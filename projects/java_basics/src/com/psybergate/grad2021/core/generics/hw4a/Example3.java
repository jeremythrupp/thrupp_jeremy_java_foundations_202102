package com.psybergate.grad2021.core.generics.hw4a;

import java.util.List;

public class Example3 {

  public static void paintAllBuildings1(List<Building> buildings) {
    buildings.forEach(Building::paint);
  }

  public static void paintAllBuildings2(List<? extends Building> buildings) {
    buildings.forEach(Building::paint);
    //?
  }
}
