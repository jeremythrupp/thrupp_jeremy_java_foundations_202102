package com.psybergate.grad2021.core.generics.hw1b;

/**
 * 
 * @since 01 Aug 2010
 */
public class Student extends Person {

  private int yearRegistered;

  public Student() {
  }

  public Student(String name, int age) {
    super(name, age);
    yearRegistered = 2001;
  }
  
  @Override
  public String getName() {
    return super.getName();
  }

  public int getYearRegistered() {
    return yearRegistered;
  }

}
