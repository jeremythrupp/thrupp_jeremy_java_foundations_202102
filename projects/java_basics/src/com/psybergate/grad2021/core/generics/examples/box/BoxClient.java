package com.psybergate.grad2021.core.generics.examples.box;

public class BoxClient {
  public static void main(String[] args) {
    Box<String> box = new Box<>();
//    box.set(3); --> incorrect type
    box.set("Hello");
    System.out.println(box.get());
  }
}
