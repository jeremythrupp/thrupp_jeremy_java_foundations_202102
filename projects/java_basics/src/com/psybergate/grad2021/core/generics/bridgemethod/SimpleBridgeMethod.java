package com.psybergate.grad2021.core.generics.bridgemethod;

import java.util.Comparator;

public class SimpleBridgeMethod implements Comparator<Object> {

    //Taken from www.stackoverflow.com


    public int compare(Integer a, Integer b) {
      return a.compareTo(b);
    }

    //THIS is a "bridge method"
    public int compare(Object a, Object b) {
      return compare((Integer)a, (Integer)b);
    }
  }
