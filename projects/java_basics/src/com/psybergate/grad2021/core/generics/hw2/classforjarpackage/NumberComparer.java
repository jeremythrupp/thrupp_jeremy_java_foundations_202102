package com.psybergate.grad2021.core.generics.hw2.classforjarpackage;

import java.util.ArrayList;
import java.util.List;

public class NumberComparer {

  public static List<Integer> greaterThan1000(List<Integer> numbers) {
    List<Integer> numbers2 = new ArrayList<>();
    for (int i = 0; i < numbers.size(); i++) {
      if (numbers.get(i) >= 1000) {
        numbers2.add(numbers.get(i));
      }
    }
    return numbers2;
  }

}
