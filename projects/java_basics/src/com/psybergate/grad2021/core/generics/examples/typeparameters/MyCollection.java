package com.psybergate.grad2021.core.generics.demo01.typeparameters;

import java.util.ArrayList;
import java.util.Collection;

public class MyCollection<E> {

  public static void main(String[] args) {
    MyCollection<String> coll = new MyCollection<>(); // coll.E = String
    Collection<String> coll1 = new ArrayList<>();
    coll.max1(coll1);
    Collection<Faculty> coll2 = new ArrayList<>();
    coll.max2(coll2);
//    MyCollection.class.getge
  }

  public E max1(Collection<E> coll) {
    return null;
  }

  public <T extends Object & Comparable<? super T>> E max2(Collection<T> coll) {
    return null;
  }

}
