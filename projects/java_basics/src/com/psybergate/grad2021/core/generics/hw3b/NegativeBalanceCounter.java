package com.psybergate.grad2021.core.generics.hw3b;

public class NegativeBalanceCounter<T> implements Counter<T> {
  @Override
  public boolean test(T object) {
    Account account = (Account) object;
    int accountBalance = account.getAccountBalance();
    if (accountBalance < 0) {
      return true;
    }
    return false;
  }
}
