package com.psybergate.grad2021.core.generics.hw3b;

public class LetterEqualsCounter<T> implements Counter<T> {

  String letter;

  public LetterEqualsCounter(String letter) {
    this.letter = letter;
  }

  @Override
  public boolean test(T object) {
    String temp = (String) object;
    String firstLetter = temp.substring(0, 1);
    if (firstLetter.equalsIgnoreCase(letter)) {
      return true;
    }
    return false;
  }
}
