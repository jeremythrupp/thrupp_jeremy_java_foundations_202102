package com.psybergate.grad2021.core.generics.hw3b;

import java.util.Collection;

public class Client {
  public static void main(String[] args) {
    Collection<Integer> numbers = DataUtils.generateNumbers();
    Collection<String> names = DataUtils.generateNames();
    Collection<Account> accounts = DataUtils.generateAccounts();

    int evenNumbers = countIf(numbers, new EvenCounter());
    int oddNumbers = countIf(numbers, new OddCounter());
    int primeNumbers = countIf(numbers, new PrimeCounter());
    int namesStartingWithC = countIf(names, new LetterEqualsCounter("c"));
    int negativeBalances = countIf(accounts, new NegativeBalanceCounter());

    System.out.println("Even Numbers: " + evenNumbers);
    System.out.println("Odd Numbers: " + oddNumbers);
    System.out.println("Prime Numbers: " + primeNumbers);
    System.out.println("Names Beginning with 'c':  " + namesStartingWithC);
    System.out.println("Negative Balances: " + negativeBalances);

  }

  public static <T> int countIf(Collection<T> c, Counter<T> p) {
    int count = 0;

    for (T t : c) {
      if (p.test(t)) {
        count++;
      }
    }
    return count;
  }
}
