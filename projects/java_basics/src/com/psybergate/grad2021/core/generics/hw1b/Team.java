package com.psybergate.grad2021.core.generics.hw1b;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>Team</code> class can contain a team of any subtype of <code>Person</code>
 */
public class Team<T extends Person> {

  private static final int MAX_TEAM_SIZE = 15;

  private List<T> people = new ArrayList<T>();

  public static int maxTeamSize() {
    return MAX_TEAM_SIZE;
  }

  public void addPerson(T t) {
    people.add(t);
  }

  public void anyMethod(T t) {
    // do nothing
  }

  public T getPerson(int position) {
    return people.get(position);
  }

  public int getNumofPersons() {
    return people.size();
  }
  

  @SuppressWarnings("unchecked")
  // Perhaps also show this method 2 generic type parameters
  public <E extends Person> E[] asArray(E[] e) {
    for (int i = 0; i < people.size(); i++) {
      e[i] = (E) getPerson(i);
    }
    return e;
  }

  public void someRandom(List<Person> names) {
    names.add(new Student());
    System.out.println(names.get(1));
  }

//  public void someRandom(List names) {
//    names.add(new File("abc"));
//  }

  public static void main(String[] args)
      throws NoSuchMethodException, SecurityException {
    Team<Student> team = new Team<Student>();
//    team.someRandom(new ArrayList<Student>());
//    team.someRandom(new ArrayList<String>());
    
    team.addPerson(new Student("Chris", 40));
    team.addPerson(new Student("Colin", 35));
//    team.addPerson(new Person("Colin", 35));
    for (int i = 0; i < team.getNumofPersons(); i++) {
      System.out.println(i + " name: " + team.getPerson(i).getName());
      System.out.println(i + " yearregistered: " + team.getPerson(i).getYearRegistered());
    }
    @SuppressWarnings("rawtypes")
    Team t = new Team();

    Student[] students = team.asArray(new Student[2]);
    // String[] strings = team.asArray(new String[2]);
    for (Student student : students) {
      System.out.println("StudentArray: " + student.getName());
    }

    // Erasure
    Class<Team> teamClass = Team.class;
    Method method = teamClass.getMethod("getPerson", int.class);
    System.out.println("Return Type : " + method.getReturnType());

  }

}
