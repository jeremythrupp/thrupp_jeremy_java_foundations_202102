package com.psybergate.grad2021.core.generics.hw3b;

public class EvenCounter<T> implements Counter<T> {
  @Override
  public boolean test(T object) {
    Integer temp = (Integer) object;
    if (temp % 2 == 0) {
      return true;
    }
    return false;
  }
}
