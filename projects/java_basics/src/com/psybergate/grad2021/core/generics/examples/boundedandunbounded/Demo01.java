package com.psybergate.grad2021.core.generics.examples.boundedandunbounded;

import com.psybergate.grad2021.core.domain.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Demo01 {

  public static void main(String[] args) {
    unboundedList(new ArrayList<String>());
    unboundedList(new ArrayList<File>());
//    unboundedList1(new ArrayList<File>()); // not allowed
    unboundedList1(new ArrayList<Object>());
    boundedList(new ArrayList<Employee>());

    List<? extends Person> p1 = new ArrayList<Student>();
  }

  private static void unboundedList(final List<?> list) { // compilers stores
    // list is of type
    // List and list.E = unknown
    //    list.add("abc"); // not allowed - the compiler does not know E's type
    String s1 = (String) list.get(0);
  }

  private static void unboundedList1(final List<Object> list) { // compilers stores list is of type
    // List and list.E = Object
    list.add("abc"); // not allowed - the compiler does not know E's type
    String s1 = (String) list.get(0);
  }

  private static void boundedList(List<? extends Person> list) { // list.E is unknown but extends
    // Person
//    list.add("abc"); // compiler wont allow this
//    list.add(new Student()); // compiler wont allow this because again, E is unknown
//    Student s1 = list.get(0); // won't compile because E is not unknown
    Person p1 = list.get(0); // compiles because E extends Person
  }


}
