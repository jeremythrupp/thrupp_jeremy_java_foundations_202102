package com.psybergate.grad2021.core.usage;

public class Order {

  public static final String ORDER_DESCRIPTION_GENERIC = "This is a generic description.";

  private String name;

  public Order(String theName) {
    this.name = theName;
  }

  public static void main(String[] args) {

    System.out.println(ORDER_DESCRIPTION_GENERIC);
    System.out.println(Integer.toHexString(3244244));

  }

  public void printName() {
    name = "John";

    System.out.println(name);
    System.out.println("name = " + name);
    System.out.println("name = " + name);
    System.out.println("name = " + name);
    System.out.println(name);


  }

  public int getQuantity() {
    int result = getResult();

    System.out.println("Hello this is a line");
    Boolean b = new Boolean(true);
    System.out.println("hello how are you");

    System.out.println("These are random lines of code");


    return result;
  }

  private int getResult() {
    int result = 2 + 2;
    return result;
  }

  public String getName() {
    return "Jane";
  }
}
