package com.psybergate.grad2021.core.usage;

import java.io.IOException;

/**
 * See intellij presentation folder for pdf with all relevant shortcuts
 */
public class IntelliJUsage {

  public static void main(String[] args) throws IOException {
    System.out.println();

  }

  /**
   * For general shortcuts
   * @see: https://www.jetbrains.com/help/idea/mastering-keyboard-shortcuts.html
   */

  /**
   * Environment:
   * project structure: alt+ctrl+shift+s
   * Build path: part of project structure
   * Run anything: ctrl+ctrl
   * run: shift+f10
   * run menu: alt+shift+f10
   */

  /**
   * General shortcuts: <br>
   * note : navigate editor tab under settings
   *
   * new menu : alt+insert (to create new classes / packages / etc) from the project window
   * new menu: ctrl+alt+insert (this is better as it works everywhere)
   * show open views/windows: ctrl+tab
   * show recent files: ctrl+e
   * show recent locations: ctrl+shift+e
   * alt+1 : goes straight to project view (see numbers of ctrl+tab) e.g alt+6 = problems view, etc
   * hide a view: shift+esc
   * alt+enter: suggestions
   * rename: shift+f6
   * basic autocomplete: ctrl+space
   * smart autocomplete: ctrl+shift+space
   * suggestions/intention actions: alt+enter
   * settings/preferences (under file menu): ctrl+alt+s
   * surround with: alt+ctrl+t
   * delete line: ctrl+y
   * duplicate line: ctrl+d
   * duplicate method: either select whole method and push ctrl+d or inside method press: ctrl+- / ctrl+d /
   * delete method: see duplicate method (ctrl+- / ctrl+y)
   * compress method: ctrl+-
   * expand method: ctrl++
   *
   * search everywhere: shift+shift (very useful)
   * search/goto for class: ctrl+n
   * goto superclass/interface: ctrl+u
   * goto subclass: ctrl+alt+b (will show list of subclasses if more than one)
   * search actions: ctrl+shift+a
   * search for file: ctrl+shift+n
   * search in class: ctrl+f
   * go to declaration / drilldown into class: ctrl+b
   * go to implementation / drilldown into interface implentation: ctrl+alt+b
   * find com.psybergate.grad2021.core.usage of class/method in project: alt+f7
   * find com.psybergate.grad2021.core.usage of class/method in file: ctrl+f7
   * goto next file: alt+right arrow
   * goto previous file: alt+left arrow
   * goto previous tool window: f12
   * go to editor: esc
   * go to next/previous error: f2 / shift+f2
   *
   * save all: ctrl+s
   *
   * close window: ctrl+f4
   * maximize window: ctrl+shift+f12
   * maximize tool window: ctrl+shift+quote
   * open terminal window: alt+f12
   *
   * copy class: f5
   * move class: f6
   */

  /**
   * Editing:
   * add new line (without breaking current line): shift+enter
   * select item and expand / reduce: ctrl+w / ctrl+shift+w (very useful)
   */

  /**
   * Refactoring: <br> in general can use alt+enter
   * Refactor menu: ctrl+shift+alt+T
   * Extract method: ctrl+alt+m
   * Extract variable: ctrl+alt+v
   * inline variable/method : ctrl+alt+n
   * extract to object field/variable: ctrl+alt+f
   * extract to constant: ctrl+alt+c
   * extract parameter: ctrl+alt+p
   * @See: https://www.jetbrains.com/help/idea/tutorial-introduction-to-refactoring.html#Tutorial_Introduction_to_Refactoring-2-chapter
   */
  /**
   * Templates: see settings/editor/live templates  - examples of templates include: <br>
   *     main
   *     sout
   *     psfi
   *     psfs
   *     etc <br>
   * Note : can add your own live templates
   * show live templates in code window: ctrl+j
   */

  /**
   * Coding:
   * Can type "try" and autocomplete ; alt+enter after closing brace creates "catch" or "finally" part
   * similar for other code blocks like "if" etc
   * comment line or selected block: ctrl+/
   * override methods: ctrl+O
   * implement methods: ctrl+i
   * type get and enter to generate getter, etc
     * optimise imports: ctrl+alt+O
   * reformat code: ctrl+alt+l
   * reformat file: ctrl+shift+alt+l
   * setting code style: see settings/code style/java
   * move line: ctrl+shift+up / ctrl+shift+down (appears to only work within same method
   * move method: ctrl+shift+up / ctrl+shift+down (place cursor on method declaration line
   * wrapping with sout: name + "abc".sout + enter (or tab) will result in code System.out.println(name + "abc");
   * wrapping with try: name + "abc".try  + enter (or tab) will result in code wrapped in try block
   * note : the above 2 techniques are referred as "postfix completion" - check this out
   * String autocomplete: alt+/
   *
   */

  /**
   * code completion:
   * basic code completion: ctrl+space
   * smart code completion: ctrl+shift+space
   * complete current statement: ctrl+shift+enter
   * @See: https://blog.jetbrains.com/idea/2020/05/code-completion/
   */

  /**
   * @See Code Generation video: https://www.youtube.com/watch?v=btqCYUc3nFE
   * generate code: alt+insert (press this while the file editor has focus)
   */
}
