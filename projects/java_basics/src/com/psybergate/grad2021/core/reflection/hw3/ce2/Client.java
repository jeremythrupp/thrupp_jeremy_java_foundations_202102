package com.psybergate.grad2021.core.reflection.hw3.ce2;

import com.psybergate.grad2021.core.reflection.student.Student;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;

import static java.lang.invoke.MethodType.methodType;
import static com.psybergate.grad2021.core.utilities.PrintUtils.*;

public class Client {

  public static void main(String[] args) throws Throwable {

    Class clazz = Student.class;

    useDefaultConstructor(clazz);
    useSecondConstructor(clazz);
    useThirdConstructor(clazz);
  }

  /**
   * Uses reflection to construct an object and execute a method on that object.
   *
   * @param clazz the class object of the class.
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  private static void useThirdConstructor(Class clazz) throws Throwable {
    Constructor<Student> constructor =
        clazz.getConstructor(String.class, String.class, int.class, String.class, String.class);
    Student student = constructor
        .newInstance("abc123", "John", 19, "Male", "13 Short Street, Randburg. Johannesburg, South Africa. 2194.");
    MethodHandle getSuburb =
        MethodHandles.publicLookup().findVirtual(Student.class, "getSuburb", methodType(String.class));
    print(getSuburb.invoke(student));
  }

  /**
   * Uses reflection to construct an object and execute a method on that object.
   *
   * @param clazz the class object of the class.
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  private static void useSecondConstructor(Class clazz) throws Throwable {
    Constructor constructor = clazz.getConstructor(String.class, String.class, int.class, String.class);
    Student student = (Student) constructor.newInstance("abc123", "John", 19, "Male");
    MethodHandle getStudentNumber =
        MethodHandles.publicLookup().findVirtual(Student.class, "getStudentNumber", methodType(String.class));
    print(getStudentNumber.invoke(student));
  }

  /**
   * Uses reflection to construct an object and execute a method on that object.
   *
   * @param clazz the class object of the class.
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  private static void useDefaultConstructor(Class clazz) throws Throwable {
    Constructor constructor = clazz.getConstructor();
    Student student = (Student) constructor.newInstance();
    MethodHandle getName = MethodHandles.publicLookup().findVirtual(Student.class, "getName", methodType(String.class));
    print(getName.invoke(student));
  }

}
