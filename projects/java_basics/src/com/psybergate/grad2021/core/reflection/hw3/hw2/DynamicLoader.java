package com.psybergate.grad2021.core.reflection.hw3.hw2;

import com.psybergate.grad2021.core.reflection.student.Student;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;

import static java.lang.invoke.MethodType.genericMethodType;
import static java.lang.invoke.MethodType.methodType;

public class DynamicLoader {

  /**
   * Stores list names with their corresponding fully qualified class names.
   */
  private static final Properties PROPERTIES = new Properties();

  /**
   * Loads the app.properties file into PROPERTIES.
   *
   * @throws IOException
   */
  public static void loadStream() throws IOException {
    InputStream stream =
        Client.class.getClassLoader().getResourceAsStream("app.properties");

    PROPERTIES.load(stream);
  }

  /**
   * @param listName the name of the list that the user wants to implement.
   * @return the class object for the specific list.
   * @throws ClassNotFoundException
   */
  public static Class getClass(String listName) throws ClassNotFoundException {
    return Class.forName(getProperty(listName));
  }

  /**
   * @param key is the name of the specific List that the user wants to implement.
   * @return the fully qualified class name that matches the key.
   */
  static String getProperty(String key) {
    return (String) PROPERTIES.get(key);
  }

  /**
   * @param clazz is the specific List class that the user has chosen to populate.
   * @return the list that has been populated with the months January to December.
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws InstantiationException
   * @throws NoSuchMethodException
   */
  public static List<String> populateList(Class clazz) throws Throwable {
    MethodHandle method = MethodHandles.lookup().findConstructor(clazz, methodType(void.class));
    List<String> list = (List<String>) method.invoke();

    list.add("January");
    list.add("February");
    list.add("March");
    list.add("April");
    list.add("May");
    list.add("June");
    list.add("July");
    list.add("August");
    list.add("September");
    list.add("October");
    list.add("November");
    list.add("December");

    return list;
  }

}
