package com.psybergate.grad2021.core.reflection.hw3.hw2;

import com.psybergate.grad2021.core.utilities.PrintUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class Client {
  public static void main(String[] args) throws Throwable {

    DynamicLoader.loadStream();

    Class clazz = DynamicLoader.getClass("LinkedList");

    List<String> months = DynamicLoader.populateList(clazz);

    PrintUtils.printList(months);

  }


}
