package com.psybergate.grad2021.core.reflection.ce2;

import com.psybergate.grad2021.core.reflection.student.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static com.psybergate.grad2021.core.utilities.PrintUtils.print;

public class Client {
  public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
    Class clazz = Student.class;

    useDefaultConstructor(clazz);
    useSecondConstructor(clazz);
    useThirdConstructor(clazz);
  }

  /**
   * Uses reflection to construct an object and execute a method on that object.
   *
   * @param clazz the class object of the class.
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  private static void useDefaultConstructor(Class clazz) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
    Constructor constructor = clazz.getConstructor();
    Student student = (Student) constructor.newInstance();
    print(student.getName());
  }

  /**
   * Uses reflection to construct an object and execute a method on that object.
   *
   * @param clazz the class object of the class.
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  private static void useSecondConstructor(Class clazz) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
    Constructor constructor = clazz.getConstructor(String.class, String.class, int.class, String.class);
    Student student = (Student) constructor.newInstance("abc123", "John", 19, "Male");
    print(student.getStudentNumber());
  }

  /**
   * Uses reflection to construct an object and execute a method on that object.
   *
   * @param clazz the class object of the class.
   * @throws NoSuchMethodException
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  private static void useThirdConstructor(Class clazz) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
    Constructor<Student> constructor =
        clazz.getConstructor(String.class, String.class, int.class, String.class, String.class);
    Student student = constructor
        .newInstance("abc123", "John", 19, "Male", "13 Short Street, Randburg. Johannesburg, South Africa. 2194.");
    print(student.getSuburb());
  }
}
