package com.psybergate.grad2021.core.reflection.hw2;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;

public class DynamicLoader {

  /**
   * Stores list names with their corresponding fully qualified class names.
   */
  private static final Properties PROPERTIES = new Properties();

  /**
   * Loads the app.properties file into PROPERTIES.
   *
   * @throws IOException
   */
  public static void loadStream() throws IOException {
    InputStream stream =
        Client.class.getClassLoader().getResourceAsStream("app.properties");

    PROPERTIES.load(stream);
  }

  /**
   * @param listName the name of the list that the user wants to implement.
   * @return the class object for the specific list.
   * @throws ClassNotFoundException
   */
  public static Class getClass(String listName) throws ClassNotFoundException {
    return Class.forName(getProperty(listName));
  }

  /**
   * @param key is the name of the specific List that the user wants to implement.
   * @return the fully qualified class name that matches the key.
   */
  static String getProperty(String key) {
    return (String) PROPERTIES.get(key);
  }

  /**
   * @param clazz is the specific List class that the user has chosen to populate.
   * @return the list that has been populated with the months January to December.
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   * @throws InstantiationException
   * @throws NoSuchMethodException
   */
  public static List<String> populateList(Class clazz) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
    List<String> list = (List<String>) clazz.newInstance();

    Method add = clazz.getMethod("add", Object.class);
    add.invoke(list, "January");
    add.invoke(list, "February");
    add.invoke(list, "March");
    add.invoke(list, "April");
    add.invoke(list, "May");
    add.invoke(list, "June");
    add.invoke(list, "July");
    add.invoke(list, "August");
    add.invoke(list, "September");
    add.invoke(list, "October");
    add.invoke(list, "November");
    add.invoke(list, "December");

    return list;
  }

}
