package com.psybergate.grad2021.core.reflection.student;

import com.psybergate.grad2021.core.reflection.ce1.DomainClass;

import java.io.Serializable;

@DomainClass
public class Student implements Serializable {

  /**
   * The unique student business number.
   */
  private String studentNumber;

  /**
   * The students name.
   */
  private String name;

  /**
   * The students age.
   */
  private int age;

  /**
   * The students gender.
   */
  private String gender;

  /**
   * The address of the Student.
   * Default format: Street address, City. Postal Code.
   * -> "344 Short Street, Randburg. Johannesburg, South Africa. 2194"
   */
  private String address;

  public Student() {
  }

  public Student(String studentNumber, String name, int age, String gender) {
    this(studentNumber, name, age, gender, null);
  }

  public Student(String studentNumber, String name, int age, String gender, String address) {
    this.studentNumber = studentNumber;
    this.name = name;
    this.age = age;
    this.gender = gender;
    this.address = address;
  }

  /**
   * @return the suburb extracted from the address field.
   */
  public String getSuburb() {
    int comma = address.indexOf(',');
    int fullStop = address.indexOf('.');
    String suburb = address.substring(comma + 2, fullStop);
    return suburb;
  }

  public String getStudentNumber() {
    return studentNumber;
  }

  public String getName() {
    return name;
  }

  public int getAge() {
    return age;
  }
}
