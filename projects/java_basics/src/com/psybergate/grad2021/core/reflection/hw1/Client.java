package com.psybergate.grad2021.core.reflection.hw1;

import com.psybergate.grad2021.core.reflection.student.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static com.psybergate.grad2021.core.utilities.PrintUtils.print;

public class Client {
  public static void main(String[] args) {
    Class clazz = Student.class;

    printSuperClass(clazz);
    printClassModifiers(clazz);
    printFieldModifiers(clazz);
    printMethodModifiers(clazz);
  }

  /**
   * Prints the class modifiers of the class.
   *
   * @param clazz The class object of the class.
   */
  private static void printClassModifiers(Class clazz) {
    String classModifiers = "Class: " + clazz.getSimpleName() + ", " + Modifier.toString(clazz.getModifiers());
    print(classModifiers);
  }

  /**
   * Prints the method names of the class and their modifiers.
   *
   * @param clazz The class object of the class.
   */
  private static void printMethodModifiers(Class clazz) {
    for (Method method : clazz.getDeclaredMethods()) {
      String methodModifier = "Method: " + method.getName() + ", " + Modifier.toString(method.getModifiers());
      print(methodModifier);
    }
  }

  /**
   * Prints the field modifiers of the class.
   *
   * @param clazz The class object of the class.
   */
  private static void printFieldModifiers(Class clazz) {
    for (Field field : clazz.getDeclaredFields()) {
      String fieldModifier = "Field: " + field.getName() + ",  " + Modifier.toString(field.getModifiers());
      print(fieldModifier);
    }
  }

  /**
   * Prints the superclass name of the class.
   *
   * @param clazz The class object of the class.
   */
  private static void printSuperClass(Class clazz) {
    String superClassName = "Superclass: " + clazz.getSuperclass().getSimpleName();
    print(superClassName);
  }

}
