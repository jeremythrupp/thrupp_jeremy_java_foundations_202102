package com.psybergate.grad2021.core.reflection.ce1;

import com.psybergate.grad2021.core.reflection.student.Student;
import static com.psybergate.grad2021.core.utilities.PrintUtils.printLine;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;


public class Client {
  public static void main(String[] args) {
    Class clazz = Student.class;

    printClass(clazz);
    printPackage(clazz);
    printAnnotations(clazz);
    printInterfaces(clazz);
    printFields(clazz);
    printDeclaredFields(clazz);
    printParameter(clazz);
    printMethods(clazz);

  }

  /**
   * Prints the methods and their return types for a class object.
   *
   * @param clazz the class object of the class.
   */
  private static void printMethods(Class clazz) {
    for (Method method : clazz.getMethods()) {
      System.out.println("Method: " + method.getName() + "; Return Type: " + method.getReturnType().getSimpleName());
    }
    printLine();
  }

  /**
   * Prints the parameter types for the constructors of the class.
   *
   * @param clazz the class object of the class.
   */
  private static void printParameter(Class clazz) {
    int i = 1;
    for (Constructor constructor : clazz.getConstructors()) {
      System.out.println("Parameters for Constructor: " + i);
      for (Parameter parameter : constructor.getParameters()) {
        System.out.println(parameter.getType().getSimpleName());
      }
      printLine();
      i++;
    }
  }

  /**
   * Prints the declared fields of the class.
   *
   * @param clazz the class object of the class.
   */
  private static void printDeclaredFields(Class clazz) {
    for (Field declaredField : clazz.getDeclaredFields()) {
      System.out.println("Field name: " + declaredField.getName());
      printLine();
    }
  }

  /**
   * Prints the fields of the class.
   *
   * @param clazz the class object of the class.
   */
  private static void printFields(Class clazz) {
    for (Field publicField : clazz.getFields()) {
      System.out.println("Public field name: " + publicField.getName());
      printLine();
    }
  }

  /**
   * Prints the interfaces that the class implements.
   *
   * @param clazz the class object of the class.
   */
  private static void printInterfaces(Class clazz) {
    for (Class anInterface : clazz.getInterfaces()) {
      System.out.println("Interface name: " + anInterface.getSimpleName());
      printLine();
    }
  }

  /**
   * Prints the annotations of the class.
   *
   * @param clazz the class object of the class.
   */
  private static void printAnnotations(Class clazz) {
    Annotation[] labels = clazz.getAnnotations();
    for (Annotation label : labels) {
      System.out.println("Annotation name: " + label.annotationType().getSimpleName());
      printLine();
    }
  }

  /**
   * Prints the package name of the class.
   *
   * @param clazz the class object of the class.
   */
  private static void printPackage(Class clazz) {
    System.out.println("Package name: " + clazz.getPackage().getName());
    printLine();
  }

  /**
   * Prints the class name and the superclass name.
   *
   * @param clazz the class object of the class.
   */
  private static void printClass(Class clazz) {
    System.out.println("Class name: " + clazz.getSimpleName());
    System.out.println("Superclass name: " + clazz.getSuperclass().getSimpleName());
    printLine();
  }



}
