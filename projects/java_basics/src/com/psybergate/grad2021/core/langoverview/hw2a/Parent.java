package com.psybergate.grad2021.core.langoverview.hw2a;

public class Parent {

  static {
    System.out.println("Car Static Initializer is running");
//  throw new RuntimeException();
  }

  {
    System.out.println("Car Instance Initializer is running");
  }


  public Parent() {
    System.out.println("Car Constructor is running.");
  }


}
