package com.psybergate.grad2021.core.langoverview.hw6a;

import java.util.Date;

//Below will not compile.
//public static class Child extends Parent{}

public class Child extends Parent {

  private static int staticAge;

  private static String staticName;

  private int age;

  private String name;

  public Child(int age, String name) {
    this.age = age;
    this.name = name;
  }

  //Constructor cannot be static because this is redundant.
  //On class-loading the constructors run, therefore static is redundant.
//  public static Child(int age, String name) {
//    this.age = age;
//    this.name = name;
//  }

  public static void printTime() {
    System.out.println(new Date().getTime());
  }



  public void printLocation() {
    System.out.println("Child.printLocation");
  }
}
