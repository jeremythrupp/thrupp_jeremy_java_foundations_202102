package com.psybergate.grad2021.core.langoverview.hw5a;


import com.psybergate.grad2021.core.langoverview.hw5a.externalpackage.ExternalChild;

public class Utility {


  public static void main(String[] args) {

    new Child().whatIsAvailableToChild();

    new ExternalChild().whatIsAvailableToExternalChild();

//    Below will not compile because privateMethod() is private.
//    new Child().privateMethod();
//new Parent().privateMethod();
//new ExternalChild().privateMethod();

    new Child().packageMethod();
    new Parent().packageMethod();

//    Below will not compile because this method is not in the same package as the Utility
//    new ExternalChild().packageMethod();

    new Child().publicMethod();
    new Parent().publicMethod();
    new ExternalChild().publicMethod();

    new Parent().protectedMethod();
    new Child().protectedMethod();

//  Below will not compile. We cannot access protected methods in another package.
//  new ExternalChild().protectedMethod();


  }


}
