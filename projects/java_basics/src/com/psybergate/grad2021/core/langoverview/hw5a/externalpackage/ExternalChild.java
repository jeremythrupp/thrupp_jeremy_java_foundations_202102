package com.psybergate.grad2021.core.langoverview.hw5a.externalpackage;

import com.psybergate.grad2021.core.langoverview.hw5a.Parent;

public class ExternalChild extends Parent {
  public void whatIsAvailableToExternalChild() {
    System.out.println("What is available to the external child class:");
    System.out.println(PROTECTED_PARENT_TYPE);
    System.out.println(PUBLIC_PARENT_TYPE);
    //   Below code will not compile.
//    System.out.println(PACKAGE_PARENT_TYPE);
//    System.out.println(PRIVATE_PARENT_TYPE);
  }

  @Override
  protected void protectedMethod() {
    System.out.println("This is a protected method");
  }
}
