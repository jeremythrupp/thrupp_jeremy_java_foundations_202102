package com.psybergate.grad2021.core.langoverview.hw7a;


//Write code that shows the various usages of

//the final modifier:
//-in a class
//-in a method
//-in a constructor
//-in a public static final (variables and methods)
//-in a private final (variables and methods)
//-In a argument/parameter

public class Parent {
  public final void finalMethod() {
    System.out.println("This is a final method.");
  }


}


final class FinalClass {
  int age = 5;

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }
}

class AnotherInnerClass {
  FinalClass finalClass = new FinalClass();

//  Below will not compile because FinalClass is final.
//  finalClass = null;

  {
    finalClass.setAge(3);
    System.out.println(finalClass.getAge());
  }
}
