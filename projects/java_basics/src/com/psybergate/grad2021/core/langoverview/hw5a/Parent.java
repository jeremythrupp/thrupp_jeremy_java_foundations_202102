package com.psybergate.grad2021.core.langoverview.hw5a;

public class Parent {

  private final String PRIVATE_PARENT_TYPE = "Private Variable";
  public final String PUBLIC_PARENT_TYPE = "Public Variable";
  protected final String PROTECTED_PARENT_TYPE = "Protected Variable";
  final String PACKAGE_PARENT_TYPE = "Parent Variable";

  //Protected methods can be accessed in the same package of the class you are referencing.
  //Protected methods are also accessed by Children in another package:
  //-> see ExternalChild.java for more understanding
  protected void protectedMethod(){
    System.out.println("This is a protected method");
  }

  private void privateMethod(){
    System.out.println("This is a private method.");
  }

  void packageMethod(){
    System.out.println("This is a package method.");
  }

  public void publicMethod(){
    System.out.println("This is a public method.");
  }


}
