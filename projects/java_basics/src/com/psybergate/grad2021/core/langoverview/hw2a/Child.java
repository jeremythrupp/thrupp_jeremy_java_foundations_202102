package com.psybergate.grad2021.core.langoverview.hw2a;

public class Child extends Parent {
  static {
    System.out.println("Child Static Initializer is running");

    //Will not compile. Object variables only compile after Car's super method has executed.
    //name = "John";

    age = 8;
  }

  private String name;
  private static int age;

  {
    System.out.println("Child Instance Initializer is running");
  }


}
