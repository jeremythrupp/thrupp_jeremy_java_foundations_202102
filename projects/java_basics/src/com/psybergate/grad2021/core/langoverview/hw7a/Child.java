package com.psybergate.grad2021.core.langoverview.hw7a;

public class Child extends Parent {

  public static final int AMOUNT = 4;

  public static final int GRADE;

  static {
    GRADE = 2;
  }

  int age;

  String name;

  private final int TOTAL = 3;
  private final int COST;

  {
    COST = 3;
  }

//    The code below will not compile.
//    A constructor is already final because
//    a constructor cannot be overriden.
//  public final Child(int age, String name) {
//    this.age = age;
//    this.name = name;
//  }


//  The code below will not compile.
//  A child class cannot override a final method from the parent class.
//  public final void finalMethod(){
//  }

//  Below will not compile.
//  You cannot have a public static final method.
//  public static final printTime(){
//    System.out.println(new Date().getTime());
//  }


  // Below will not compile.
  // private methods cannot be final.
//private final printLocation(){
//  System.out.println("Child.printLocation");
//}

  public void finalParameter(final int DATE){
//    Below will not compile because DATE is final
//    DATE = 3;


  }


}
