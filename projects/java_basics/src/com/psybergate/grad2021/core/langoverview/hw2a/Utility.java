package com.psybergate.grad2021.core.langoverview.hw2a;

public class Utility {

  //Object Variable
  private Child child1;
  private final Child child2 = new Child();
  public Child child3;
  protected Child child4;
  public static final Child child5 = new Child();

  //Local Variable
  private int count1;
  private final int count2 = 2;
  public int count3;
  protected int count4;
  public static final int count5 = 5;

  public static void main(String[] args) {
    Parent parent1 = new Child();
  }
}
