package com.psybergate.grad2021.core.langoverview.hw5a;

public class Child extends Parent {

  public void whatIsAvailableToChild() {
    System.out.println("What is available to the child class:");
    System.out.println(PACKAGE_PARENT_TYPE);
    System.out.println(PROTECTED_PARENT_TYPE);
    System.out.println(PUBLIC_PARENT_TYPE);
    //  Below will not compile. PRIVATE_PARENT_TYPE is private.
//  System.out.println(PRIVATE_PARENT_TYPE);
  }

  @Override
  protected void protectedMethod() {
    System.out.println("This is a protected method");
  }

  private void privateMethod(){
    System.out.println("This is a private method.");
  }

  void packageMethod(){
    System.out.println("This is a package method.");
  }

  public void publicMethod(){
    System.out.println("This is a public method.");
  }
}
