package com.psybergate.grad2021.core.langoverview.hw1a;

//There can only be one public class in a .java file
//That class must have the same name as the file name
public class MultipleClasses {
  public static void main(String[] args) {
    //You can access the AdditionalClass in here
    new AdditionalClass();
  }
}

class AdditionalClass {
  public static void main(String[] args) {
    //You can access the first public class in here
    new MultipleClasses();
  }

}
