package com.psybergate.grad2021.core.langoverview.hw4a;

public class Operators {
  public static void main(String[] args) {
    if (4 % 2 == 0) {
      System.out.println("4 is divisible by 2.");
    } else {
      System.out.println("4 is not divisible by 2.");
    }


    System.out.print("\n");

    int Count = 0;
    System.out.println("Count is initialised to: " + Count);
    System.out.println("Count++ = " + Count++);
    System.out.println("Count is now: " + Count);
    System.out.println("++Count = " + ++Count);

    System.out.print("\n");

    int age1 = 3;
    int age2 = 3;
    Car car1 = new Car();
    Car car2 = new Car();

    if (age1 == age2) {
      System.out.println("age1 is == to age2");
    } else {
      System.out.println("age1 is not == to age2");
    }

    System.out.print("\n");

    if (car1 == car2) {
      System.out.println("car1 is == to car2");
    } else {
      System.out.println("car1 is not == car2");
    }

    System.out.println("\n");


    //READ_ME - The below operators can be tested by changing the value of the grade variable.

    int grade = 3;

    if (grade == 3 && new Car().isCarRunning()) {
      System.out.println("&&");
    }

    if (grade == 3 & new Car().isCarRunning()) {
      System.out.println("&");
    }

    if (grade == 3 || new Car().isCarRunning()) {
      System.out.println("||");
    }

    if (grade == 3 | new Car().isCarRunning()) {
      System.out.println("|");
    }


    int amount = 300;
    amount += 200;

    System.out.println("amount = " + amount);

    System.out.println("\n");


    int increase;
    increase = amount == 300 ? 10 : 20;

    System.out.println("increase = " + increase);

    System.out.print("\n");

    int value;
    //user assigns a number to value here
    value = 5;
    //then we run this switch statement
    switch (value) {
      case 0:
        System.out.println("value is 0");
        break;
      case 5:
        System.out.println("value is 5");
        break;
      default:
        System.out.println("value is neither 0 or 5");

    }


  }


}
