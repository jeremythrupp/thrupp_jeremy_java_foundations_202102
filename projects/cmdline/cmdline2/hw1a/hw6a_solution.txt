For further reading on the javap command, view the JDK specification or click here:
 https://docs.oracle.com/javase/8/docs/technotes/tools/windows/javap.html


javap
This command prints out the class file.
Remember that a class file is in bytecode.


javap -c 
Prints out the class file in more detail.
I.e. The instructions for each method in this class file.

javap -v (verbose)
Prints the class file in even more detail.
Here you can view the opcodes of the bytecode.
The opcodes can be found in the JDK specification.
