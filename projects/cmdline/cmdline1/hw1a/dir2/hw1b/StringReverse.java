package hw1b;

import hw1a.*;

public class StringReverse{
	
	public static void main(String[] args){
		
		
		
		String input = args[0];
		
		reverse(input);
	}
	
	public static void reverse(String input){
		System.out.println(MyStringUtils.reverse(input));
	}
}