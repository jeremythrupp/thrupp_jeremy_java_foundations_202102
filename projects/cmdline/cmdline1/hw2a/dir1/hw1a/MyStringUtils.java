package com.psybergate.grad2021.core.cmdline.cmdline1.hw2a.dir1.hw1a;

public class MyStringUtils{
	public static String reverse(String str ){
		char[] wordArray = str.toCharArray();
		char tempLetter = ' ';
		
		int arrayLength = wordArray.length - 1;
		for(int i = 0; i < wordArray.length-1; i++){
			tempLetter = wordArray[i];
			
			wordArray[i] = wordArray[arrayLength - i];
			
			wordArray[arrayLength - i] = tempLetter;
		}

		str = String.valueOf(wordArray);
		
		return str;

	}

}