package com.psybergate.grad2021.core.cmdline.cmdline1.hw2a.dir2.hw1b;

import com.psybergate.grad2021.core.cmdline.cmdline1.hw2a.dir1.hw1a.*;

public class StringReverse{
	
	public static void main(String[] args){
		
		
		
		String input = args[0];
		
		reverse(input);
	}
	
	public static void reverse(String input){
		System.out.println(MyStringUtils.reverse(input));
	}
}