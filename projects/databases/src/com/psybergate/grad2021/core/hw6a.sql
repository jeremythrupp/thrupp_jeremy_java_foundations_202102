select * from information_schema.tables;

SELECT count(table_name) FROM information_schema.tables;

SELECT column_name, data_type, column_default, is_nullable
FROM information_schema.columns WHERE table_name='customer';

SELECT * FROM information_schema.information_schema_catalog_name;