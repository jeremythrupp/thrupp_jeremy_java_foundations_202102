select customername, count(account_id) as numOfAccounts, sum(accountbalance) as totalBalance
from account
left join customer on account.customer_id = customer.customer_id
group by customername;

select customername, count(account_id) as numOfAccounts, sum(accountbalance) as totalBalance
from account
left join customer on account.customer_id = customer.customer_id
group by customername
having count(account_id) > 5 and sum(accountbalance) > 1000;