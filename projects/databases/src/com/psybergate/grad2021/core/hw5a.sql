explain select * from customer;

explain analyze select * from customer;

explain verbose select * from customer;

explain (costs false) select * from customer;

explain (buffers true) select * from customer;
