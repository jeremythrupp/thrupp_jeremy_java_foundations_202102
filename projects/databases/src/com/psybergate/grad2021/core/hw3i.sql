select customer.customername, account.accountnum, account.accountbalance as balance
from customer, account, accounttype
where account.accounttype_id = accounttype.accounttype_id and (accounttype.accounttypename = 'current' OR accounttype.accounttypename = 'credit') and customer.customer_id = account.customer_id;

select customer.customername, account.accountnum, account.accountbalance as balance
from account
left join accounttype on account.accounttype_id = accounttype.accounttype_id 
left join customer on account.customer_id = customer.customer_id
where (accounttype.accounttypename = 'current' OR accounttype.accounttypename = 'credit'); 

