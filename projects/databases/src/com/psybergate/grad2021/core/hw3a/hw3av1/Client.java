package com.psybergate.grad2021.core.hw3a.hw3av1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.psybergate.grad2021.core.hw3a.hw3av1.SqlUtils.*;

public class Client {
  public static void main(String[] args) {

    System.out.println(getSql());

    File sqlOutput = new File("src\\com\\psybergate\\grad2021\\core\\hw3a\\hw3b.sql");
    try {
      FileWriter writer = new FileWriter(sqlOutput);
      writer.write(getSql());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String getSql() {
    StringBuilder sqlQuery = new StringBuilder();

    sqlQuery.append(createTables());
    sqlQuery.append(insertTransactionType("withdraw"));
    sqlQuery.append(insertTransactionType("deposit"));
    sqlQuery.append(insertAccountType("current"));
    sqlQuery.append(insertAccountType("credit"));
    sqlQuery.append(insertCustomers(20));
    sqlQuery.append(insertAccounts(8));
    sqlQuery.append(insertTransaction(70));

    return sqlQuery.toString();
  }
}
