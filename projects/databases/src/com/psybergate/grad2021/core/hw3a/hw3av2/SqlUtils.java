package com.psybergate.grad2021.core.hw3a.hw3av2;

import static com.psybergate.grad2021.core.hw3a.hw3av2.BusinessNumberUtils.generateBusinessNumber;
import static com.psybergate.grad2021.core.hw3a.hw3av2.IdentifierUtils.*;
import static com.psybergate.grad2021.core.hw3a.hw3av2.NumberUtils.generateRandomBalance;
import static com.psybergate.grad2021.core.hw3a.hw3av2.NumberUtils.generateRandomNumber;

public class SqlUtils {

  /**
   *
   * @param amount represents the number of insert SQL queries to be generated for the transaction table.
   * @return the generated insert SQL queries for the transaction table.
   */
  public static String insertTransactions(int amount) {
    StringBuilder sqlQuery = new StringBuilder();

    for (Integer accountID : getAccountIDs()) {
      for (int i = 0; i < amount; i++) {
        String sql =
            "INSERT INTO Transaction (transaction_ID, transactionNum, transactionDate, transactionType_ID, account_ID)"
                + "VALUES (" + generateID("Transaction") + ",' " + generateBusinessNumber("Transaction")
                + "', '1992-03-04' ," + getID("TransactionType") + ", " + accountID + ");";
        sqlQuery.append(sql);
      }
    }

    return sqlQuery.toString();

  }


  /**
   *
   * @param amount represents the number of insert SQL queries to be generated for the Account table.
   * @return the generated insert SQL queries for the Account table.
   */
  public static String insertAccounts(int customerID, int amount) {
    String sql = "";

    for (int i = 0; i < amount; i++) {
      sql += "INSERT INTO Account(account_ID, accountNum, customer_ID, accountDate, accountBalance, accountType_ID)"
          + " VALUES (" + generateAccountID(customerID) + ", '"
          + generateBusinessNumber("Account") + "', " + customerID
          + ",'2020-04-15' ," + generateRandomBalance() + ", " + getID("AccountType") + ");"
      ;
    }
    return sql;
  }


  /**
   *
   * @param amount represents the number of insert SQL queries to be generated for the Customer table.
   * @return the generated insert SQL queries for the Customer table.
   */
  public static String insertCustomers(int amount) {
    String sql = "";
    for (int i = 0; i < amount; i++) {
      sql += "INSERT INTO Customer(" +
          "customer_ID, customerNum, customerName, customerSurname, customerDateOfBirth, customerCity)"
          + " VALUES (" + generateID("Customer") + ", '"
          + generateBusinessNumber("Customer") + "', 'cust" + generateRandomNumber() + "', 'Smith', '1992-03-15', 'Johannesburg');";
    }
    return sql;
  }

  /**
   *
   * @param transactionType is the type of transaction that will be inserted into the TransactionType table.
   * @return the insert SQL query for the TransactionType table for a specific transaction type.
   */
  public static String insertTransactionType(String transactionType) {
    String sql = "INSERT INTO TransactionType (transactionType_ID, transactionTypeNum, transactionTypeName)"
        + "VALUES (" + generateID("TransactionType") + ", '"
        + generateBusinessNumber("TransactionType") + "', '"
        + transactionType + "');";

    return sql;
  }

  /**
   * @param accountType is the type of account that will be inserted into the AccountType table.
   * @return the insert SQL query for the AccountType table for a specific account type.
   */
  public static String insertAccountType(String accountType) {
    String sql = "INSERT INTO AccountType(accountType_ID, accountTypeNum, accountTypeName)"
        + "VALUES (" + generateID("AccountType") + ", '"
        + generateBusinessNumber("AccountType") + "', '"
        + accountType + "');";

    return sql;
  }

  /**
   *
   * @return the create SQL query for the tables within a database.
   */
  public static String createTables() {
    String sql =
        "create table if not exists customer (customer_id int not null, customernum varchar unique not null, customername varchar not null, customersurname varchar not null, customerdateofbirth date not null,customercity varchar not null ,primary key (customer_id) );"
            + "create table if not exists account (account_id int not null, customer_id int not null,  accounttype_id int not null, accountnum varchar unique not null, accountdate date not null, accountbalance double precision not null, primary key (account_id));"
            + "create table if not exists transaction (transaction_id int not null, transactionnum varchar not null, transactiontype_id int not null, account_id int not null, transactiondate date not null);"
            + "create table if not exists transactiontype (transactiontype_id int not null, transactiontypenum varchar not null, transactiontypename varchar not null, primary key (transactiontype_id));"
            + "create table if not exists accounttype (accounttype_id int not null, accounttypenum varchar not null, accounttypename varchar not null, primary key (accounttype_id));"
            + "alter table account add foreign key (accounttype_id) references accounttype(accounttype_id);"
            + "alter table transaction add foreign key (account_id) references account (account_id);";

    return sql;
  }
}