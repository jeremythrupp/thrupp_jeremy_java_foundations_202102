package com.psybergate.grad2021.core.hw3a.hw3av1;

import static com.psybergate.grad2021.core.hw3a.hw3av1.BusinessNumberUtils.generateBusinessNumber;
import static com.psybergate.grad2021.core.hw3a.hw3av1.IdentifierUtils.generateID;
import static com.psybergate.grad2021.core.hw3a.hw3av1.IdentifierUtils.getID;
import static com.psybergate.grad2021.core.hw3a.hw3av1.NumberUtils.*;

public class SqlUtils {

  public static String insertAccounts(int amount) {
    String sql = "";
    for (int i = 0; i < amount; i++) {
      sql += "INSERT INTO Account(account_ID, accountNum, customer_ID, accountDate, accountBalance, accountType_ID)"
          + " VALUES (" + generateID("Account") + ", '"
          + generateBusinessNumber("Account") + "', " + getID("Customer")
          + ",'2020-04-15' ," + generateRandomNumber(10000) + ", " + getID("AccountType") + ");"
      ;
    }
    return sql;
  }





  public static String insertTransaction(int amount) {
    String sql = "";
    for (int i = 0; i <= amount; i++) {
      sql += "INSERT INTO Transaction (transaction_ID, transactionNum, transactionDate, transactionType_ID, account_ID)"
          + "VALUES (" + generateID("Transaction") + ",' " + generateBusinessNumber("Transaction")
          + "', '1992-03-04' ," + getID("Transaction") + ", " + getID("Account") + ");";

    }
    return sql;
  }

  public static String insertCustomers(int amount) {
    String sql = "";
    for (int i = 0; i < amount; i++) {
      sql += "INSERT INTO Customer(" +
          "customer_ID, customerNum, customerName, customerSurname, customerDateOfBirth)"
          + " VALUES (" + generateID("Customer") + ", '"
          + generateBusinessNumber("Customer") + "', 'John', 'Smith', '1992-03-15');";
    }
    return sql;
  }

  //TODO - generateID - rather use serial
  public static String insertTransactionType(String transactionType) {
    String sql = "INSERT INTO TransactionType (transactionType_ID, transactionTypeNum, transactionTypeName)"
        + "VALUES (" + generateID("TransactionType") + ", '"
        + generateBusinessNumber("TransactionType") + "', '"
        + transactionType + "');";

    return sql;
  }

  /**
   *
   * @param accountType is the type of account. Eg. "current"
   * @return
   */
  public static String insertAccountType(String accountType) {
    String sql = "INSERT INTO AccountType(accountType_ID, accountTypeNum, accountTypeName)"
        + "VALUES (" + generateID("AccountType") + ", '"
        + generateBusinessNumber("AccountType") + "', '"
        + accountType + "');";

    return sql;
  }

  public static String createTables() {
    String sql =
        "create table if not exists customer (customer_id int not null, customernum varchar unique not null, customername varchar not null, customersurname varchar not null, customerdateofbirth date not null, primary key (customer_id) );"
            + "create table if not exists account (account_id int not null, customer_id int not null,  accounttype_id int not null, accountnum varchar unique not null, accountdate date not null, accountbalance double precision not null, primary key (account_id));"
            + "create table if not exists transaction (transaction_id int not null, transactionnum varchar not null, transactiontype_id int not null, account_id int not null, transactiondate date not null);"
            + "create table if not exists transactiontype (transactiontype_id int not null, transactiontypenum varchar not null, transactiontypename varchar not null, primary key (transactiontype_id));"
            + "create table if not exists accounttype (accounttype_id int not null, accounttypenum varchar not null, accounttypename varchar not null, primary key (accounttype_id));"
            + "alter table account add foreign key (accounttype_id) references accounttype(accounttype_id);"
            + "alter table transaction add foreign key (account_id) references account (account_id);";

    return sql;
  }
}