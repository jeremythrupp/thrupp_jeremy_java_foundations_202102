package com.psybergate.grad2021.core.hw3a.hw3av2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.psybergate.grad2021.core.hw3a.hw3av2.NumberUtils.generateRandomNumber;
import static com.psybergate.grad2021.core.hw3a.hw3av2.SqlUtils.*;

public class Client {
  public static void main(String[] args) {

    File sqlOutput = new File("src\\com\\psybergate\\grad2021\\core\\hw3a\\hw3b.sql");
    try {
      FileWriter writer = new FileWriter(sqlOutput);
      writer.write(getSql());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   *
   * @return the entire SQL query for creating tables and inserting data into those tables.
   */
  private static String getSql() {
    StringBuilder sqlQuery = new StringBuilder();

    sqlQuery.append(createTables());
    sqlQuery.append(insertTransactionType("withdraw"));
    sqlQuery.append(insertTransactionType("deposit"));
    sqlQuery.append(insertAccountType("current"));
    sqlQuery.append(insertAccountType("credit"));
    sqlQuery.append(insertCustomers(20));

    for (Integer customerID : IdentifierUtils.getCustomerIDs()) {
      //some customers must have no accounts as seen below:
      if (customerID % 2 == 0) {
        sqlQuery.append(insertAccounts(customerID, generateRandomNumber(8)));
      }
    }

    sqlQuery.append(insertTransactions(generateRandomNumber(100)));

    return sqlQuery.toString();

  }
}
