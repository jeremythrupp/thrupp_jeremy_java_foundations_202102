package com.psybergate.grad2021.core.hw3a.hw3av1;

import java.util.HashSet;
import java.util.Set;

public class BusinessNumberUtils {

  //TODO - javadoc fields
  private static Set<String> customerNums = new HashSet<>();

  private static Set<String> accountNums = new HashSet<>();

  private static Set<String> accountTypeNums = new HashSet<>();

  private static Set<String> transactionNums = new HashSet<>();

  private static Set<String> transactionTypeNums = new HashSet<>();

  public static String generateBusinessNumber(String table) {
    String businessNumber = "";
    switch (table) {
      case "Customer":
        businessNumber = generateBusinessNumber(customerNums);
        break;
      case "Account":
        businessNumber = generateBusinessNumber(accountNums);
        break;
      case "AccountType":
        businessNumber = generateBusinessNumber(accountTypeNums);
        break;
      case "Transaction":
        businessNumber = generateBusinessNumber(transactionNums);
        break;
      case "TransactionType":
        businessNumber = generateBusinessNumber(transactionTypeNums);
        break;
    }

    return businessNumber;
  }

  private static String generateBusinessNumber(Set<String> numsCollections) {
    boolean exists = false;
    String num = "";
    while (!exists) {
      num = "num" + NumberUtils.generateRandomNumber();
      if (!numsCollections.contains(num)) {
        numsCollections.add(num);
        exists = true;
      }
    }

    return num;
  }

}
