package com.psybergate.grad2021.core.hw3a.hw3av1;

public class NumberUtils {

  public static int generateRandomNumber() {
    return 1 + ((int) (Math.random() * 1000));
  }

  /**
   * @param maximum
   * @return returns a random number between 0 and maximum
   */
  public static int generateRandomNumber(int maximum) {
    return (int) (Math.random() * maximum);
  }
}
