package com.psybergate.grad2021.core.hw3a.hw3av2;

public class TableUtils {
  /**
   * TABLE_ACCOUNT represents the title of the 'Account' table.
   */
  public static final String TABLE_ACCOUNT = "Account";

  /**
   * TABLE_TRANSACTION represents the title of the 'Transaction' table.
   */
  public static final String TABLE_TRANSACTION = "Transaction";

  /**
   * TABLE_TRANSACTION_TYPE represents the title of the 'TransactionType' table.
   */
  public static final String TABLE_TRANSACTION_TYPE = "TransactionType";

  /**
   * TABLE_CUSTOMER represents the title of the 'Customer' table.
   */
  public static final String TABLE_CUSTOMER = "Customer";

  /**
   * TABLE_ACCOUNT_TYPE represents the title of the 'AccountType' table.
   */
  public static final String TABLE_ACCOUNT_TYPE = "AccountType";

}
