package com.psybergate.grad2021.core.hw3a.hw3av2;

public class NumberUtils {

  public static int generateRandomNumber() {
    return 1 + ((int) (Math.random() * 1000));
  }

  /**
   *
   * @return a random number that has the chance of being negative.
   */
  public static int generateRandomBalance() {
    if (generateRandomNumber(2) > 0) {
      return (-1) * generateRandomNumber(20000);
    }
    return generateRandomNumber(20000);
  }

  /**
   * @param maximum
   * @return returns a random number between 0 and maximum
   */
  public static int   generateRandomNumber(int maximum) {
    return (int) (Math.random() * maximum);
  }

}
