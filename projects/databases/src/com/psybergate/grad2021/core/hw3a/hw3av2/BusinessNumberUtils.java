package com.psybergate.grad2021.core.hw3a.hw3av2;

import java.util.HashSet;
import java.util.Set;

/**
 * Class BusinessNumberUtils is used to generate unique business numbers.
 * These business numbers are not used by the Database Management System.
 * Instead, the Database Management System uses the Unique Identifiers generated in class IdentifierUtils.
 */
public class BusinessNumberUtils {

  /**
   * customerNums is the collection of unique Customer business numbers that have been generated.
   */
  private static Set<String> customerNums = new HashSet<>();

  /**
   * accountNums is the collection of unique Account business numbers that have been generated.
   */
  private static Set<String> accountNums = new HashSet<>();

  /**
   * accountTypeNums is the collection of unique AccountType business numbers that have been generated.
   */
  private static Set<String> accountTypeNums = new HashSet<>();

  /**
   * transactionNums is the collection of unique Transaction business numbers that have been generated.
   */
  private static Set<String> transactionNums = new HashSet<>();

  /**
   * transactionTypeNums is the collection of unique TransactionType business numbers that have been generated.
   */
  private static Set<String> transactionTypeNums = new HashSet<>();

  /**
   * @param tableName is the table for which a unique business number will be generated.
   * @return the uniquely generated business number for the specific table name.
   */
  public static String generateBusinessNumber(String tableName) {
    String businessNumber = "";
    switch (tableName) {
      case TableUtils.TABLE_CUSTOMER:
        businessNumber = generateBusinessNumber(customerNums);
        break;
      case TableUtils.TABLE_ACCOUNT:
        businessNumber = generateBusinessNumber(accountNums);
        break;
      case TableUtils.TABLE_ACCOUNT_TYPE:
        businessNumber = generateBusinessNumber(accountTypeNums);
        break;
      case TableUtils.TABLE_TRANSACTION:
        businessNumber = generateBusinessNumber(transactionNums);
        break;
      case TableUtils.TABLE_TRANSACTION_TYPE:
        businessNumber = generateBusinessNumber(transactionTypeNums);
        break;
    }

    return businessNumber;
  }

  /**
   * @param numsCollections is the set of collections containing the unique business numbers for a specific table.
   * @return the uniquely generated business number for a specific table.
   */
  private static String generateBusinessNumber(Set<String> numsCollections) {
    boolean exists = false;
    String num = "";
    while (!exists) {
      num = "num" + NumberUtils.generateRandomNumber();
      if (!numsCollections.contains(num)) {
        numsCollections.add(num);
        exists = true;
      }
    }

    return num;
  }

}
