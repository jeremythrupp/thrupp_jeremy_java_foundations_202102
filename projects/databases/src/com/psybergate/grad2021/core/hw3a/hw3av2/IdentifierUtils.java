package com.psybergate.grad2021.core.hw3a.hw3av2;

import java.util.*;

import static com.psybergate.grad2021.core.hw3a.hw3av2.NumberUtils.generateRandomNumber;

/**
 * Class IdentifierUtils is used to generate unique identifiers.
 * These unique identifier are used by the Database Management System for various functions including performing
 * queries.
 */
public class IdentifierUtils {

  /**
   * customerAccountIDs are the unique account identifiers that belong to a specific customer.
   */
  private static Map<Integer, Set<Integer>> customerAccountIDs = new HashMap<>();

  /**
   * customerIDsWithAccounts are the unique Customer identifiers of which have Accounts belonging to them.
   */
  private static Set<Integer> customerIDsWithAccounts = new HashSet<>();

  /**
   * customerIDs are the unique Customer identifiers that have been generated.
   */
  private static Set<Integer> customerIDs = new HashSet<>();

  /**
   * accountIDs are the unique Account identifiers that have been generated.
   */
  private static Set<Integer> accountIDs = new HashSet<>();

  /**
   * accountTypeIDs are the unique AccountType identifiers that have been generated.
   */
  private static Set<Integer> accountTypeIDs = new HashSet<>();

  /**
   * transactionIDs are the unique Transaction identifiers that have been generated.
   */
  private static Set<Integer> transactionIDs = new HashSet<>();

  /**
   * transactionTypeIDs are the unique TransactionType identifiers that have been generated.
   */
  private static Set<Integer> transactionTypeIDs = new HashSet<>();

  /**
   * @param customerID represents the Customer for which an Account Identifier will be generated for.
   * @return the generated Account Identifier for the Customer.
   */
  public static int generateAccountID(int customerID) {

    if (!checkCustomerIDExists(customerID)) {
      customerIDsWithAccounts.add(customerID);
      return generateAccountIDNewCustomer(customerID);
    }

    int accountID = 0;
    boolean added = false;

    while (!added) {
      accountID = generateRandomNumber();
      Set<Integer> accIDs = customerAccountIDs.get(customerID);
      if (!accIDs.contains(accountID)) {
        accIDs.add(accountID);
        customerAccountIDs.replace(customerID, accIDs);
        added = true;
      }
    }
    return accountID;
  }

  /**
   * This method will check if accountIDs contains the customerID.
   * If customerID does not exist in accountIDs, the customerID with a Set containing an accountID
   * will be added to accountIDs.
   *
   * @param customerID is the key in accountIDs.
   */
  private static boolean checkCustomerIDExists(int customerID) {
    if (!customerAccountIDs.containsKey(customerID)) {
      return false;
    }
    return true;
  }

  /**
   * @param customerID is the Customer Identifier for which an Account Identifier will be generated.
   * @return the generated Account Identifier.
   */
  private static int generateAccountIDNewCustomer(int customerID) {
    Set<Integer> accIDs = new HashSet<>();
    boolean added = false;
    int accountID = 0;
    while (!added) {
      accountID = generateRandomNumber();
      if (!accountIDs.contains(accountID)) {
        accIDs.add(accountID);
        customerAccountIDs.put(customerID, accIDs);
        accountIDs.add(accountID);
        added = true;
      }
    }
    return accountID;
  }

  /**
   * @param table is the table name for which a unique identifier will be generated.
   * @return the unique identifier for the specific table.
   */
  public static int generateID(String table) {

    Integer id = 0;
    switch (table) {
      case TableUtils.TABLE_CUSTOMER:
        id = generateIdentifier(customerIDs);
        break;
      case TableUtils.TABLE_ACCOUNT_TYPE:
        id = generateIdentifier(accountTypeIDs);
        break;
      case TableUtils.TABLE_TRANSACTION:
        id = generateIdentifier(transactionIDs);
        break;
      case TableUtils.TABLE_TRANSACTION_TYPE:
        id = generateIdentifier(transactionTypeIDs);
        break;
    }

    return id;

  }

  /**
   * @param idCollections is the collection containing the unique identifiers for a specific table.
   * @return the uniquely generated identifier for a specific table.
   */
  private static int generateIdentifier(Set<Integer> idCollections) {
    boolean exists = false;
    int id = 0;
    while (!exists) {
      id = NumberUtils.generateRandomNumber();
      if (!idCollections.contains(id)) {
        idCollections.add(id);
        exists = true;
      }
    }
    return id;
  }

  /**
   * @param tableName the name of the table for which an identifier must be retrieved.
   * @return the unique identifier for the specific table.
   */
  public static Integer getID(String tableName) {
    List<Integer> ids = new ArrayList<>();

    if (tableName.equals(TableUtils.TABLE_TRANSACTION)) {
      ids.addAll(transactionIDs);
    } else if (tableName.equals(TableUtils.TABLE_TRANSACTION_TYPE)) {
      ids.addAll(transactionTypeIDs);
    } else if (tableName.equals(TableUtils.TABLE_CUSTOMER)) {
      ids.addAll(customerIDs);
    } else if (tableName.equals(TableUtils.TABLE_ACCOUNT_TYPE)) {
      ids.addAll(accountTypeIDs);
    }

    return pickRandomID(ids);
  }

  /**
   * @param tableName the name of the table to retrieve the ID for.
   * @return will either return an ID for the corresponding table, or null.
   */
  public static Integer getIDCanBeNull(String tableName) {
    if (generateRandomNumber(1000) > 500) {
      return null;
    }
    return getID(tableName);
  }

  /**
   * @param ids is the list of ids of a specific collection.
   * @return a randomly chosen id from the collection of ids that is passed in.
   */
  public static int pickRandomID(List<Integer> ids) {
    return ids.get(generateRandomNumber(ids.size()));
  }

  /**
   * @return the set of unique Customer identifiers.
   */
  public static Set<Integer> getCustomerIDs() {
    return customerIDs;
  }

  /**
   * @return the Account identifiers.
   */
  public static Set<Integer> getAccountIDs() {
    return accountIDs;
  }
}

