package com.psybergate.grad2021.core.hw3a.hw3av1;

import java.util.HashSet;
import java.util.Set;
import static com.psybergate.grad2021.core.hw3a.hw3av1.NumberUtils.*;

public class IdentifierUtils {

  //TODO - javadoc fields

  private static Set<Integer> customerIDs = new HashSet<>();

  private static Set<Integer> accountIDs = new HashSet<>();

  private static Set<Integer> accountTypeIDs = new HashSet<>();

  private static Set<Integer> transactionIDs = new HashSet<>();

  private static Set<Integer> transactionTypeIDs = new HashSet<>();

  public static int generateID(String table) {
    int id = 0;
    switch (table) {
      case "Customer":
        id = generateIdentifier(customerIDs);
        break;
      case "Account":
        id = generateIdentifier(accountIDs);
        break;
      case "AccountType":
        id = generateIdentifier(accountTypeIDs);
        break;
      case "Transaction":
        id = generateIdentifier(transactionIDs);
        break;
      case "TransactionType":
        id = generateIdentifier(transactionTypeIDs);
        break;
    }

    return id;

  }

  private static int generateIdentifier(Set<Integer> idCollections) {
    boolean exists = false;
    int id = 0;
    while (!exists) {
      id = NumberUtils.generateRandomNumber();
      if (!idCollections.contains(id)) {
        idCollections.add(id);
        exists = true;
      }
    }
    return id;
  }

  public static Integer getID(String tableName) {

    //Look into converting the Set collection to an array of the same size.
    //This code is too 'clunky'.
    Integer[] ids = new Integer[1000];
    if (tableName.equals("Account")) {
      accountIDs.toArray(ids);
    } else if (tableName.equals("Transaction")) {
      transactionIDs.toArray(ids);
    } else if (tableName.equals("TransactionType")) {
      transactionTypeIDs.toArray(ids);
      if(generateRandomNumber(1000) > 500){
        //choose the second transaction type instead of the first
        return ids[1];
      }
    } else if (tableName.equals("Customer")) {
      customerIDs.toArray(ids);
    } else if (tableName.equals("AccountType")){
      accountTypeIDs.toArray(ids);
      if(generateRandomNumber(1000) > 500){
        //choose the first account type instead of the first
        return ids[1];
      }
    }

    return ids[0];
  }

}
