select customer.customername, customer.customercity as city, account.accountnum, account.accountbalance as balance, count(transaction.transaction_id) as numoftransactions 
from account
left join customer on account.customer_id = customer.customer_id
left join transaction on account.account_id = transaction.account_id
group by customer.customer_id, account.account_id
having (customer.customercity) = 'Johannesburg' OR count(transaction.transaction_id) > 20;
