select customer.customername as customerName, count(account.account_id) as numOfAccounts, sum(account.accountbalance) as totalBalance
from account
left join customer on account.customer_id = customer.customer_id
group by customer.customer_id;

select customer.customername as customerName, count(account.account_id) as numOfAccounts, sum(account.accountbalance) as totalBalance
from account
left join customer on account.customer_id = customer.customer_id
group by customer.customer_id
having count(account.account_id) > 5 and sum(account.accountbalance) > 1000;