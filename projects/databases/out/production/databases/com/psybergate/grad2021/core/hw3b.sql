ALTER TABLE account ADD COLUMN needsToBeReviewed boolean NULL;
update account set needstobereviewed = false;
alter table account alter column needsToBeReviewed set not null;

update account set needstobereviewed = true where accountdate < (CURRENT_DATE - INTERVAL '90 days');