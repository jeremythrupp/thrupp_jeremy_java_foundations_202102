select customer.customername as customerName, max(account.accountbalance) as maxbalance, min(account.accountbalance) as minbalance
from account 
left join customer on account.customer_id = customer.customer_id
group by customer.customer_id;

select customer.customername as customerName, max(account.accountbalance) as maxbalance, min(account.accountbalance) as minbalance
from account 
left join customer on account.customer_id = customer.customer_id
group by customer.customer_id
having min(account.accountbalance) > 1000 and max(account.accountbalance) > 800;