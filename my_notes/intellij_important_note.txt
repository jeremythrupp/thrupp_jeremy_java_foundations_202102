Change this setting to prevent intellij from rebuilding the entire project:

->Settings
->Build, Execution, Deployment
->Compiler
	-> Tick the box: "Build project automatically"