package com.psybergate.jeefnds.enterprise.hwent0.domain;

import com.psybergate.jeefnds.enterprise.hwent0.annotations.DomainProperty;

public class Customer {

  /**
   * customerNum is a unique identifier for each Customer object
   */
  @DomainProperty(name = "customerNum", primaryKey = true)
  private long customerNum;

  /**
   * name is the first name of the customer
   */
  @DomainProperty(name = "name")
  private String name;


  /**
   * surname is the last name of the customer
   */
  @DomainProperty(name = "surname")
  private String surname;

  /**
   * dateOfBirth is the birthdate of the customer
   */
  @DomainProperty(name = "dateOfBirth")
  private Integer dateOfBirth;

  public Customer(long customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public long getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  @Override
  public String toString() {
    String output = "Customer Number: " + customerNum + "\n"
        + "Name: " + name + "\n"
        + "Surname: " + surname + "\n"
        + "Date of Birth: " + dateOfBirth + "\n";

    return output;
  }
}
