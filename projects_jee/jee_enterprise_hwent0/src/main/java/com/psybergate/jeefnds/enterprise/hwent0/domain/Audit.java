package com.psybergate.jeefnds.enterprise.hwent0.domain;

import com.psybergate.jeefnds.enterprise.hwent0.annotations.DomainProperty;

public class Audit {

  /**
   * customerNum is a unique identifier for each Audit object
   */
  @DomainProperty(name = "auditNum", primaryKey = true)
  private long auditNum;

  @DomainProperty(name = "date")
  private long date;

  @DomainProperty(name = "action")
  private String action;

  @DomainProperty(name = "warnings")
  private String warnings;

  public Audit(long auditNum, long date, String action, String warnings) {
    this.auditNum = auditNum;
    this.date = date;
    this.action = action;
    this.warnings = warnings;
  }

  public long getAuditNum() {
    return auditNum;
  }

  public long getDate() {
    return date;
  }

  public String getAction() {
    return action;
  }

  public String getWarnings() {
    return warnings;
  }

  @Override
  public String toString() {
    return "auditNum=" + auditNum +
        ", income=" + date +
        ", expense=" + action +
        ", sales=" + warnings +
        "/n";
  }
}
