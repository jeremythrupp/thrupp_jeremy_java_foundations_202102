package com.psybergate.jeefnds.enterprise.hwent0.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a field is intended to be included in the database columns.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainProperty {
  boolean primaryKey() default false;
  boolean nullable() default false;

  String name();
}
