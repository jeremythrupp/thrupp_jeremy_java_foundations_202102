package com.psybergate.jeefnds.enterprise.hwent0.service;

import com.psybergate.jeefnds.enterprise.hwent0.domain.Audit;
import com.psybergate.jeefnds.enterprise.hwent0.annotations.DomainProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AuditService {

  public static String getInsertAuditSQL(Audit audit) {
    List<String> fieldNames = new ArrayList<>();

    String sqlQuery = "insert into audit " + " (";

    Field[] fields = audit.getClass().getDeclaredFields();

    for (int i = 0; i <= fields.length - 1; i++) {
      Field field = fields[i];
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        String fieldName = ((DomainProperty) annotation).name();
        sqlQuery = sqlQuery + fieldName;
        fieldNames.add(field.getName());
      }
      if (i <= fields.length - 2) {
        sqlQuery = addComma(sqlQuery);
      }
    }
    sqlQuery = addParenthesis(sqlQuery);

    return appendSQL(sqlQuery, audit, fieldNames) + ";";
  }

  private static String appendSQL(String sqlQueryBeginning, Audit audit, List<String> fieldNames) {
    String sql = sqlQueryBeginning + " VALUES (";
    for (Iterator iterator = fieldNames.iterator(); iterator.hasNext(); ) {
      String field = (String) iterator.next();
      sql += getValue(audit, field);
      if (iterator.hasNext()) {
        sql = addComma(sql);
      }
    }
    sql = addParenthesis(sql);
    return sql;
  }

  private static String addParenthesis(String sql) {
    sql += ")";
    return sql;
  }

  private static String addComma(String sql) {
    sql += ", ";
    return sql;
  }

  private static String getValue(Audit audit, String field) {
    String value = "";
    switch (field) {
      case "auditNum":
        value = String.valueOf(audit.getAuditNum());
        break;
      case "date":
        value = String.valueOf(audit.getDate());
        break;
      case "action":
        value = "'" + audit.getAction() + "'";
        break;
      case "warnings":
        value = "'" + audit.getWarnings() + "'";
        break;
    }
    return value;
  }

}
