package com.psybergate.jeefnds.enterprise.hwent0.client;

import com.psybergate.jeefnds.enterprise.hwent0.database.DatabaseManager;
import com.psybergate.jeefnds.enterprise.hwent0.domain.Audit;
import com.psybergate.jeefnds.enterprise.hwent0.domain.Customer;
import com.psybergate.jeefnds.enterprise.hwent0.service.AuditService;
import com.psybergate.jeefnds.enterprise.hwent0.service.CustomerService;

public class Client {
  public static void main(String[] args) {
    DatabaseManager.dropTablesSQL();

    instantiateTables();
  }

  private static void instantiateTables() {
    Customer customer = new Customer(1, "John", "Doe", 19850311);
    String createCustomerTable = DatabaseManager.createTableSQL("customer", Customer.class);
    String insertCustomer = CustomerService.getInsertCustomerSQL(customer);

    Audit audit = new Audit(1, 20011210, "Inserted Customer into Customer table.", "None.");
    String createAuditTable = DatabaseManager.createTableSQL("audit", Audit.class);
    String insertAudit = AuditService.getInsertAuditSQL(audit);

    String sqlQuery = createCustomerTable + "\n" + insertCustomer + "\n" + createAuditTable + "\n" + insertAudit;

    DatabaseManager.passQueryToDatabase(sqlQuery);
  }
}
