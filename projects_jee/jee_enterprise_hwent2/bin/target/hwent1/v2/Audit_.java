package hwent1.v2;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Audit.class)
public abstract class Audit_ {

	public static volatile SingularAttribute<Audit, String> warnings;
	public static volatile SingularAttribute<Audit, String> action;
	public static volatile SingularAttribute<Audit, Long> id;
	public static volatile SingularAttribute<Audit, LocalDate> auditDate;

	public static final String WARNINGS = "warnings";
	public static final String ACTION = "action";
	public static final String ID = "id";
	public static final String AUDIT_DATE = "auditDate";

}

