package hwent1.v1;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Customer.class)
public abstract class Customer_ {

	public static volatile SingularAttribute<Customer, String> surname;
	public static volatile SingularAttribute<Customer, String> name;
	public static volatile SingularAttribute<Customer, LocalDate> dateOfBirth;
	public static volatile SingularAttribute<Customer, Long> id;

	public static final String SURNAME = "surname";
	public static final String NAME = "name";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String ID = "id";

}

