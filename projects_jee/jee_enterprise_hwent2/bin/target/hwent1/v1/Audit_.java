package hwent1.v1;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Audit.class)
public abstract class Audit_ {

	public static volatile SingularAttribute<Audit, LocalDate> date;
	public static volatile SingularAttribute<Audit, String> warnings;
	public static volatile SingularAttribute<Audit, String> action;
	public static volatile SingularAttribute<Audit, Long> id;

	public static final String DATE = "date";
	public static final String WARNINGS = "warnings";
	public static final String ACTION = "action";
	public static final String ID = "id";

}

