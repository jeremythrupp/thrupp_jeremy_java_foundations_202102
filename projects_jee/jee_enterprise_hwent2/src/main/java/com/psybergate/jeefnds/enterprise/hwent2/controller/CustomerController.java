package com.psybergate.jeefnds.enterprise.hwent2.controller;

import com.psybergate.jeefnds.enterprise.hwent2.domain.Audit;
import com.psybergate.jeefnds.enterprise.hwent2.domain.Customer;
import com.psybergate.jeefnds.enterprise.hwent2.service.CustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CustomerController {
  public void addCustomer(HttpServletRequest request, HttpServletResponse response) {
    try {
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("addCustomer.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    LocalDate dateOfBirth = LocalDate.parse(request.getParameter("dateOfBirth"));

    Customer customer = new Customer();

    customer.setName(name);
    customer.setSurname(surname);
    customer.setDateOfBirth(dateOfBirth);

    Audit audit = new Audit();
    audit.setAction("Added Customer.");
    audit.setAuditDate(LocalDate.now());
    audit.setWarnings("None.");

    CustomerService customerService = new CustomerService();
    customerService.saveCustomer(customer, audit);
  }
}
