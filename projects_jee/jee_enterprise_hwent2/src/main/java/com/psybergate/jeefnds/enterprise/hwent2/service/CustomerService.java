package com.psybergate.jeefnds.enterprise.hwent2.service;

import com.psybergate.jeefnds.enterprise.hwent2.domain.Audit;
import com.psybergate.jeefnds.enterprise.hwent2.domain.Customer;
import com.psybergate.jeefnds.enterprise.hwent2.resource.AuditResource;
import com.psybergate.jeefnds.enterprise.hwent2.resource.CustomerResource;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CustomerService {

  public static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("CustomerAudit_JPA");

  private EntityManager entityManager;

  private static EntityManager getEntityManager() {
    return emf.createEntityManager();
  }

  public boolean saveCustomer(Customer customer, Audit audit) {
    entityManager = getEntityManager();
    entityManager.getTransaction().begin();

    CustomerResource customerResource = new CustomerResource();
    AuditResource auditResource = new AuditResource();
    customerResource.saveCustomer(customer, entityManager);
    auditResource.saveAudit(audit, entityManager);

    entityManager.getTransaction().commit();
    entityManager.close();
    emf.close();
    return true;
  }
}
