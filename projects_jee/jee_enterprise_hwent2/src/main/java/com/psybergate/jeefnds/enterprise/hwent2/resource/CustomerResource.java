package com.psybergate.jeefnds.enterprise.hwent2.resource;

import com.psybergate.jeefnds.enterprise.hwent2.domain.Customer;

import javax.persistence.EntityManager;

public class CustomerResource {
  public void saveCustomer(Customer customer, EntityManager entityManager) {
    entityManager.persist(customer);
  }
}
