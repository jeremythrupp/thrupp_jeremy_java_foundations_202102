package com.psybergate.jeefnds.enterprise.hwent2.resource;

import com.psybergate.jeefnds.enterprise.hwent2.domain.Audit;

import javax.persistence.EntityManager;

public class AuditResource {
  public void saveAudit(Audit audit, EntityManager entityManager) {
    entityManager.persist(audit);
  }
}
