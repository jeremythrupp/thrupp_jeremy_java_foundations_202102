package com.psybergate.jeefnds.enterprise.hwent1.v2;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class Client {

  public static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("CustomerAudit_JPA");

  private static EntityManager entityManager;


  public static void main(String[] args) {
    entityManager = getEntityManager();
    entityManager.getTransaction().begin();

    Customer customer = getCustomer();
    Audit audit = getAudit();

    save(customer);
    save(audit);

    entityManager.getTransaction().commit();
    entityManager.close();
    emf.close();
  }

  public static void save(Object object) {
    entityManager.persist(object);
  }

  private static Audit getAudit() {
    Audit audit = new Audit();
    audit.setAuditDate(LocalDate.now());
    audit.setAction("Added customers.");
    audit.setWarnings("IO warning.");
    return audit;
  }

  private static Customer getCustomer() {
    Customer customer = new Customer();
    customer.setName("Johnny");
    customer.setSurname("Hello");
    customer.setDateOfBirth(LocalDate.now());
    return customer;
  }

  private static EntityManager getEntityManager() {
    return emf.createEntityManager();
  }

}
