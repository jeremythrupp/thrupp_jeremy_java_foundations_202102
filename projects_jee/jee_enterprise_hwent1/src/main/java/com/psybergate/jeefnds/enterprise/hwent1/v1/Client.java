package com.psybergate.jeefnds.enterprise.hwent1.v1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class Client {

  private static SessionFactory factory;

  public static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("");

  static {

    StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/psybergate/jeefnds/enterprise/hwent1/v1/hibernate.cfg.xml").build();

    Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

    factory = meta.getSessionFactoryBuilder().build();
  }

  public static void main(String[] args) {
    Session session = factory.openSession();
    Transaction transaction = null;
    try {
      transaction = session.beginTransaction();

      storeCustomer(session);
//      storeAudit(session);

      transaction.commit();
    } catch (Exception e) {
      transaction.rollback();
    } finally {
      session.close();
      factory.close();
    }
  }

//  private static void storeAudit(Session session) {
//    Audit audit = new Audit();
//    audit.setDate(LocalDate.now());
//    audit.setAction("Added Customer object to Database.");
//    audit.setWarnings("None.");
//
//    session.save(audit);
//  }

  private static void storeCustomer(Session session) {
    Customer customer = new Customer();
    customer.setName("Peter");
    customer.setSurname("Jones");
    customer.setDateOfBirth(LocalDate.now());

    session.save(customer);
  }

}
