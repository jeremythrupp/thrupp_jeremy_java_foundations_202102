package com.psybergate.jeefnds.enterprise.hwent3.v2.client;

import com.psybergate.jeefnds.enterprise.hwent3.v2.domain.Audit;
import com.psybergate.jeefnds.enterprise.hwent3.v2.domain.Customer;

import javax.ejb.Local;
import java.time.LocalDate;

@Local
public interface CustomerService {

  public boolean saveCustomer(String name, String surname, LocalDate dateOfBirth);

  public boolean persist(Customer customer, Audit audit);

}
