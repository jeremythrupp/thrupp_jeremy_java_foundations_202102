package com.psybergate.jeefnds.enterprise.hwent3.v2.resource;

import com.psybergate.jeefnds.enterprise.hwent3.v2.domain.Audit;

import javax.ejb.Local;

@Local
public interface AuditResource {
  public void saveAudit(Audit audit);
}
