package com.psybergate.jeefnds.enterprise.hwent3.v2.client;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

/**
 * The Controller needs to be in the CDI container because in this class the
 * CustomerService is injected. Therefore, use the @Named annotation.
 */
@Named //include in CDI container
@ApplicationScoped //lifetime of the session
public class CustomerController {

  @Inject
  private CustomerService customerService;

  public CustomerController() {

  }

  /**
   *
   * Note: We typically don't do validation in Controllers.
   * @param request
   * @param response
   */
  public void addCustomer(HttpServletRequest request, HttpServletResponse response) {
    try {
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("addCustomer.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    LocalDate date = LocalDate.parse(request.getParameter("dateOfBirth"));
    customerService.saveCustomer(name, surname, date);
  }
}
