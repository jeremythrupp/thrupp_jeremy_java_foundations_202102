package com.psybergate.jeefnds.enterprise.hwent3.v2.resource;

import com.psybergate.jeefnds.enterprise.hwent3.v2.domain.Audit;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequestScoped //try @Dependent
public class AuditResourceImpl implements AuditResource {

  @PersistenceContext(unitName = "CustomerAudit_JPA")
  private EntityManager entityManager;

  public void saveAudit(Audit audit) {
    entityManager.persist(audit);
  }
}
