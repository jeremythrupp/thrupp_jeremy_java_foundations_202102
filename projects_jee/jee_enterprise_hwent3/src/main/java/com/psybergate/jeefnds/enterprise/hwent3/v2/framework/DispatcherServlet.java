package com.psybergate.jeefnds.enterprise.hwent3.v2.framework;

import com.psybergate.jeefnds.enterprise.hwent3.v2.client.CustomerController;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = "/addcustomer")
public class DispatcherServlet extends HttpServlet {

  @Inject
  private CustomerController customerController;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    customerController.addCustomer(request, response);
  }
}
