package com.psybergate.jeefnds.enterprise.hwent3.v2.resource;

import com.psybergate.jeefnds.enterprise.hwent3.v2.domain.Customer;

import javax.ejb.Local;

@Local
public interface CustomerResource{
  public void saveCustomer(Customer customer);
}
