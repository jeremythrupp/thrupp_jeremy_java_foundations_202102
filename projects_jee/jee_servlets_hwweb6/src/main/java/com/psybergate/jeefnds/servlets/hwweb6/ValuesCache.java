package com.psybergate.jeefnds.servlets.hwweb6;

import java.util.ArrayList;
import java.util.List;

public class ValuesCache {
  private static List<String> values = new ArrayList<>();

  public static void addValues(List<String> names) {
    values = names;
  }

  public static List<String> getValues() {
    return values;
  }
}

