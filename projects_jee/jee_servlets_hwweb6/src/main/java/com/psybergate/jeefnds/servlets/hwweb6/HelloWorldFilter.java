package com.psybergate.jeefnds.servlets.hwweb6;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@WebFilter("/hello")
public class HelloWorldFilter implements Filter {

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    System.out.println("filter id = " + System.identityHashCode(this));
    List<String> names = Arrays.asList(servletRequest.getParameterValues("name"));
    if (!names.contains("John")) {
      filterChain.doFilter(servletRequest, servletResponse);
    } else {
      PrintWriter out = servletResponse.getWriter();
      out.println("Invalid name 'John'. Please try again.");
    }

  }

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

}
