package com.psybergate.jeefnds.enterprise.hwent4.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(name = "auditdate")
  private LocalDate auditDate;

  @Column(name = "action")
  private String action;

  @Column(name = "warnings")
  private String warnings;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public LocalDate getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(LocalDate auditDate) {
    this.auditDate = auditDate;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String action) {
    this.action = action;
  }

  public String getWarnings() {
    return warnings;
  }

  public void setWarnings(String warnings) {
    this.warnings = warnings;
  }
}
