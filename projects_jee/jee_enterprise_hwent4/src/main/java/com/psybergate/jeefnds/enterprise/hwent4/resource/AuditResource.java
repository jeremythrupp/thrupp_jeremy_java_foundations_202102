package com.psybergate.jeefnds.enterprise.hwent4.resource;

import com.psybergate.jeefnds.enterprise.hwent4.domain.Audit;

import javax.ejb.Local;

@Local
public interface AuditResource {
  public void saveAudit(Audit audit);
}
