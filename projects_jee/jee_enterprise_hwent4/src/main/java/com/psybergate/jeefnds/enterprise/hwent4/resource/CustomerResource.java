package com.psybergate.jeefnds.enterprise.hwent4.resource;

import com.psybergate.jeefnds.enterprise.hwent4.domain.Customer;

import javax.ejb.Local;

@Local
public interface CustomerResource{
  public void saveCustomer(Customer customer);
}
