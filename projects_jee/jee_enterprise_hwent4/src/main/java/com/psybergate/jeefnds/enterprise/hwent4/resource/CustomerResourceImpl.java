package com.psybergate.jeefnds.enterprise.hwent4.resource;

import com.psybergate.jeefnds.enterprise.hwent4.domain.Customer;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequestScoped
public class CustomerResourceImpl implements CustomerResource {

  @PersistenceContext(unitName = "CustomerAudit_JPA")
  private EntityManager entityManager;

  public void saveCustomer(Customer customer) {
    entityManager.persist(customer);
    System.out.println("entityManager.getClass() = " + entityManager.getClass());
  }
}
