package com.psybergate.jeefnds.enterprise.hwent4.client;

import com.psybergate.jeefnds.enterprise.hwent4.domain.Audit;
import com.psybergate.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.jeefnds.enterprise.hwent4.resource.AuditResourceImpl;
import com.psybergate.jeefnds.enterprise.hwent4.resource.CustomerResourceImpl;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;

/**
 * A stateless session bean by default contains transactional properties.
 * The transactional bean which will be wrapped by a transactional proxy.
 */
@Stateless
@Transactional
public class CustomerServiceImpl implements CustomerService {

  private final AuditResourceImpl auditResourceImpl;

  private final CustomerResourceImpl customerResourceImpl;

  @Inject
  public CustomerServiceImpl(CustomerResourceImpl customerResourceImpl, AuditResourceImpl auditResourceImpl) {
    this.customerResourceImpl = customerResourceImpl;
    this.auditResourceImpl = auditResourceImpl;
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  @Override
  public boolean saveCustomer(String name, String surname, LocalDate dateOfBirth) {

    Customer customer = new Customer();
    customer.setName(name);
    customer.setSurname(surname);
    customer.setDateOfBirth(dateOfBirth);
    Audit audit = new Audit();
    audit.setAction("Added Customer.");
    audit.setAuditDate(LocalDate.now());
    audit.setWarnings("None.");

    return persist(customer, audit);
  }

  @TransactionAttribute(TransactionAttributeType.REQUIRED)
  @Override
  public boolean persist(Customer customer, Audit audit) {

    customerResourceImpl.saveCustomer(customer);
    auditResourceImpl.saveAudit(audit);

    return true;

  }
}
