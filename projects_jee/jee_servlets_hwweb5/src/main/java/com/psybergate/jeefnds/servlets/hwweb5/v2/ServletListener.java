package com.psybergate.jeefnds.servlets.hwweb5.v2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(value = "/hello")
public class ServletListener extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
    List<String> names = names = ValuesCache.getValues();

    String name = pickRandomName(names);
    printName(resp, name);
  }

  private void printName(HttpServletResponse resp, String name) {
    try {
      resp.setContentType("text/html");
      PrintWriter writer = resp.getWriter();
      writer.println("<h1> Hello:" + name + " </h1>");

    } catch (Exception e) {
      throw new RuntimeException(e.getMessage() + "\n" + e.getLocalizedMessage());
    }
  }

  private String pickRandomName(List<String> names) {
    String temp = "";
    boolean nameAssigned = false;
    while (!nameAssigned) {
      for (String name : names) {
        double randomNumber = Math.random() * 1000;
        if (randomNumber > 500) {
          temp = name;
          nameAssigned = true;
        }
      }
    }
    return temp;
  }
}
