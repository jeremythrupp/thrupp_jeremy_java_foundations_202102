package com.psybergate.jeefnds.servlets.hwweb5.v1;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import java.util.Arrays;
import java.util.List;

//@WebListener
public class ServletListener implements ServletRequestListener {

  @Override
  public void requestInitialized(ServletRequestEvent sre) {
    List<String> names = Arrays.asList(sre.getServletRequest().getParameterValues("name"));
    ValuesCache.addValues(names);
  }

}
