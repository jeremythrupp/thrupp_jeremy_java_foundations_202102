package com.psybergate.jeefnds.servlets.hwweb3.service;

import com.psybergate.jeefnds.servlets.hwweb3.Customer;
import com.psybergate.jeefnds.servlets.hwweb3.annotations.DomainProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

public class CustomerService {

  public static String getInsertSQL(Customer customer) {
    List<String> fieldNames = new ArrayList<>();

    String sqlQuery = "INSERT INTO CUSTOMER (";

    Field[] fields = customer.getClass().getDeclaredFields();

    for (int i = 0; i <= fields.length - 1; i++) {
      Field field = fields[i];
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        String fieldName = ((DomainProperty) annotation).name();
        sqlQuery = sqlQuery + fieldName;
        fieldNames.add(field.getName());
      }
      if (i <= fields.length - 2) {
        sqlQuery = addComma(sqlQuery);
      }
    }
    sqlQuery = addParenthesis(sqlQuery);

    return appendSQL(sqlQuery, customer, fieldNames) + ";";
  }

  private static String appendSQL(String sqlQueryBeginning, Customer customer, List<String> fieldNames) {
    String sql = sqlQueryBeginning + " VALUES (";
    for (Iterator iterator = fieldNames.iterator(); iterator.hasNext(); ) {
      String field = (String) iterator.next();
      sql += getValue(customer, field);
      if (iterator.hasNext()) {
        sql = addComma(sql);
      }
    }
    sql = addParenthesis(sql);
    return sql;
  }

  private static String addParenthesis(String sql) {
    sql += ")";
    return sql;
  }

  private static String addComma(String sql) {
    sql += ", ";
    return sql;
  }

  private static String getValue(Customer customer, String field) {
    String value = "";
    switch (field) {
      case "customerNum":
        value = String.valueOf(customer.getCustomerNum());
        break;
      case "name":
        value = "'" + customer.getName() + "'";
        break;
      case "surname":
        value = "'" + customer.getSurname() + "'";
        break;
      case "dateOfBirth":
        value = String.valueOf(customer.getDateOfBirth());
        break;
    }
    return value;
  }

}
