package com.psybergate.jeefnds.servlets.hwweb3.controllers;

import com.psybergate.jeefnds.servlets.hwweb3.Customer;
import com.psybergate.jeefnds.servlets.hwweb3.service.CustomerService;
import com.psybergate.jeefnds.servlets.hwweb3.database.DatabaseManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomerController {
  public void addCustomer(HttpServletRequest request, HttpServletResponse response){
    try {
      RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/resources/addCustomer.jsp");
      requestDispatcher.forward(request, response);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }
    long customerNum = Long.valueOf(request.getParameter("customerNum"));
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    Integer dateOfBirth = Integer.valueOf(request.getParameter("dateOfBirth"));
    Customer customer = new Customer(customerNum, name, surname, dateOfBirth);

    String createSQL = DatabaseManager.getCreateSQL();
    String insertSQL = CustomerService.getInsertSQL(customer);

    DatabaseManager.passQueryToDatabase(createSQL);
    DatabaseManager.passQueryToDatabase(insertSQL);

  }
}
