package com.psybergate.jeefnds.servlets.hwweb3.servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

@WebServlet(value = "/addcustomer")
public class DispatcherServlet extends HttpServlet {
  private Properties properties = new Properties();

  @Override
  public void init() {
    try {
      InputStream stream =
          DispatcherServlet.class.getClassLoader().getResourceAsStream("app.properties");
      properties.load(stream);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    try {
      String servletPath = request.getServletPath().substring(1);
      String classAndMethod = properties.getProperty(servletPath);
      int index = classAndMethod.indexOf('#');
      String className = classAndMethod.substring(0, index);
      String methodName = classAndMethod.substring(index + 1);
      Object controller = Class.forName(className).newInstance();
      Method method = controller.getClass().getMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
      method.invoke(controller, request, response);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

  }
}
