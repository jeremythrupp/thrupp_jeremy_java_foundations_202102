package com.psybergate.jeefnds.servlets.hwweb2;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet(urlPatterns = {"/*"})
public class HttpInfoServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("v1.7");

    printHeaders(request, response);

    printProtocol(request, response);

    printHTTPMethod(request, response);

    printRequestURI(request, response);

    printPathInfo(request, response);

  }

  private void printPathInfo(HttpServletRequest request, HttpServletResponse response) {
    try {
      String pathInfo = request.getPathInfo();
      PrintWriter writer = response.getWriter();
      writer.println("<h1> Path Info: </h1>");
      writer.println("<p>" + pathInfo + "</p>");
      writer.println("<h1> ========================================================= </h1>");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void printRequestURI(HttpServletRequest request, HttpServletResponse response) {
    try {
      String requestURI = request.getRequestURI();
      PrintWriter writer = response.getWriter();
      writer.println("<h1> Request URI: </h1>");
      writer.println("<p>" + requestURI + "</p>");
      writer.println("<h1> ========================================================= </h1>");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void printHTTPMethod(HttpServletRequest request, HttpServletResponse response) {
    try {
      String httpMethod = request.getMethod();
      PrintWriter writer = response.getWriter();
      writer.println("<h1> HTTP Method: </h1>");
      writer.println("<p>" + httpMethod + "</p>");
      writer.println("<h1> ========================================================= </h1>");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void printProtocol(HttpServletRequest request, HttpServletResponse response) {
    try {
      PrintWriter writer = response.getWriter();
      writer.println("<h1> Protocol: </h1>");
      String protocol = request.getProtocol();
      writer.println("<p>" + protocol + "</p>");
      writer.println("<h1> ========================================================= </h1>");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void printHeaders(HttpServletRequest request, HttpServletResponse response) {
    try {
      Enumeration<String> enums = (Enumeration<String>) request.getHeaderNames();
      int i = 1;
      PrintWriter writer = response.getWriter();
      writer.println("<h1> Headers: </h1>");
      while (enums.hasMoreElements()) {
        String headerName = enums.nextElement();
        String headerValue = request.getHeader(headerName);
        response.setContentType("text/html");
        writer.println("<p> Header " + i + ".) " + headerName + ": " + headerValue + "</p>");
        i++;
      }
      writer.println("<h1> ========================================================= </h1>");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
